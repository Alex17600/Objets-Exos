--
-- PostgreSQL database dump
--

-- Dumped from database version 15.2
-- Dumped by pg_dump version 15.2

-- Started on 2023-04-25 09:22:03

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 5 (class 2615 OID 2200)
-- Name: public; Type: SCHEMA; Schema: -; Owner: pg_database_owner
--

CREATE SCHEMA public;


ALTER SCHEMA public OWNER TO pg_database_owner;

--
-- TOC entry 3430 (class 0 OID 0)
-- Dependencies: 5
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: pg_database_owner
--

COMMENT ON SCHEMA public IS 'standard public schema';


--
-- TOC entry 276 (class 1255 OID 17472)
-- Name: creer_chef(character varying, character varying, character varying, character varying, integer); Type: PROCEDURE; Schema: public; Owner: postgres
--

CREATE PROCEDURE public.creer_chef(IN p_nom character varying, IN p_prenom character varying, IN p_mot_de_passe character varying, IN p_email character varying, IN p_id_restaurant integer)
    LANGUAGE plpgsql
    AS $_$   
BEGIN
  IF length(p_mot_de_passe) < 6 THEN
    RAISE EXCEPTION 'Le mot de passe doit contenir au moins 6 caractères';
  END IF;

  IF NOT p_email ~ '^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$' THEN
    RAISE EXCEPTION 'Adresse email invalide';
  END IF;
 
  IF EXISTS (SELECT 1 FROM chef WHERE email = p_email) THEN
    RAISE EXCEPTION 'L''adresse email existe déjà';
  END IF;

  INSERT INTO chef (nom, prenom, mot_de_passe, email, id_restaurant)
  VALUES (p_nom, p_prenom, encode(digest(p_mot_de_passe, 'sha1'),'hex'), p_email, p_id_restaurant);
END;
$_$;


ALTER PROCEDURE public.creer_chef(IN p_nom character varying, IN p_prenom character varying, IN p_mot_de_passe character varying, IN p_email character varying, IN p_id_restaurant integer) OWNER TO postgres;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 222 (class 1259 OID 17414)
-- Name: chef; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.chef (
    id_chef integer NOT NULL,
    nom character varying(50) NOT NULL,
    prenom character varying(50) NOT NULL,
    mot_de_passe character varying(255) NOT NULL,
    email character varying(50) NOT NULL,
    id_restaurant integer NOT NULL
);


ALTER TABLE public.chef OWNER TO postgres;

--
-- TOC entry 221 (class 1259 OID 17413)
-- Name: chef_id_chef_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.chef_id_chef_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.chef_id_chef_seq OWNER TO postgres;

--
-- TOC entry 3431 (class 0 OID 0)
-- Dependencies: 221
-- Name: chef_id_chef_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.chef_id_chef_seq OWNED BY public.chef.id_chef;


--
-- TOC entry 226 (class 1259 OID 17457)
-- Name: commentaire; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.commentaire (
    id_recette integer NOT NULL,
    id_chef integer NOT NULL,
    date_poste timestamp without time zone,
    contenu character varying(255),
    note numeric(15,2),
    id_commentaire integer NOT NULL
);


ALTER TABLE public.commentaire OWNER TO postgres;

--
-- TOC entry 227 (class 1259 OID 17523)
-- Name: commentaire_id_commentaire_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.commentaire_id_commentaire_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.commentaire_id_commentaire_seq OWNER TO postgres;

--
-- TOC entry 3432 (class 0 OID 0)
-- Dependencies: 227
-- Name: commentaire_id_commentaire_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.commentaire_id_commentaire_seq OWNED BY public.commentaire.id_commentaire;


--
-- TOC entry 225 (class 1259 OID 17442)
-- Name: composition; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.composition (
    id_recette integer,
    id_ingredient integer NOT NULL,
    "quantité" integer,
    id_composition integer NOT NULL
);


ALTER TABLE public.composition OWNER TO postgres;

--
-- TOC entry 228 (class 1259 OID 17533)
-- Name: composition_id_composition_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.composition_id_composition_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.composition_id_composition_seq OWNER TO postgres;

--
-- TOC entry 3433 (class 0 OID 0)
-- Dependencies: 228
-- Name: composition_id_composition_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.composition_id_composition_seq OWNED BY public.composition.id_composition;


--
-- TOC entry 216 (class 1259 OID 17393)
-- Name: ingredient; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.ingredient (
    id_ingredient integer NOT NULL,
    nom character varying(50) NOT NULL,
    type_aliment character varying(50),
    unit_mesure character varying(50)
);


ALTER TABLE public.ingredient OWNER TO postgres;

--
-- TOC entry 215 (class 1259 OID 17392)
-- Name: ingredient_id_ingredient_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.ingredient_id_ingredient_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ingredient_id_ingredient_seq OWNER TO postgres;

--
-- TOC entry 3434 (class 0 OID 0)
-- Dependencies: 215
-- Name: ingredient_id_ingredient_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.ingredient_id_ingredient_seq OWNED BY public.ingredient.id_ingredient;


--
-- TOC entry 224 (class 1259 OID 17426)
-- Name: recette; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.recette (
    id_recette integer NOT NULL,
    nom character varying(50) NOT NULL,
    temps_prepa integer NOT NULL,
    nbr_couvert integer,
    prepa text,
    vege boolean NOT NULL,
    id_type_recette integer NOT NULL,
    id_chef integer NOT NULL,
    image text,
    moyenne_note real[]
);


ALTER TABLE public.recette OWNER TO postgres;

--
-- TOC entry 3435 (class 0 OID 0)
-- Dependencies: 224
-- Name: COLUMN recette.temps_prepa; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.recette.temps_prepa IS 'Temps de prépa en minutes';


--
-- TOC entry 223 (class 1259 OID 17425)
-- Name: recette_id_recette_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.recette_id_recette_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.recette_id_recette_seq OWNER TO postgres;

--
-- TOC entry 3436 (class 0 OID 0)
-- Dependencies: 223
-- Name: recette_id_recette_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.recette_id_recette_seq OWNED BY public.recette.id_recette;


--
-- TOC entry 218 (class 1259 OID 17400)
-- Name: restaurant; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.restaurant (
    id_restaurant integer NOT NULL,
    nom character varying(50) NOT NULL,
    adresse character varying(50),
    ville character varying(50),
    num_tel character varying(50),
    email character varying(50)
);


ALTER TABLE public.restaurant OWNER TO postgres;

--
-- TOC entry 217 (class 1259 OID 17399)
-- Name: restaurant_id_restaurant_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.restaurant_id_restaurant_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.restaurant_id_restaurant_seq OWNER TO postgres;

--
-- TOC entry 3437 (class 0 OID 0)
-- Dependencies: 217
-- Name: restaurant_id_restaurant_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.restaurant_id_restaurant_seq OWNED BY public.restaurant.id_restaurant;


--
-- TOC entry 220 (class 1259 OID 17407)
-- Name: type_recette; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.type_recette (
    id_type_recette integer NOT NULL,
    nom character varying(50) NOT NULL
);


ALTER TABLE public.type_recette OWNER TO postgres;

--
-- TOC entry 219 (class 1259 OID 17406)
-- Name: type_recette_id_type_recette_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.type_recette_id_type_recette_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.type_recette_id_type_recette_seq OWNER TO postgres;

--
-- TOC entry 3438 (class 0 OID 0)
-- Dependencies: 219
-- Name: type_recette_id_type_recette_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.type_recette_id_type_recette_seq OWNED BY public.type_recette.id_type_recette;


--
-- TOC entry 3244 (class 2604 OID 17417)
-- Name: chef id_chef; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.chef ALTER COLUMN id_chef SET DEFAULT nextval('public.chef_id_chef_seq'::regclass);


--
-- TOC entry 3247 (class 2604 OID 17524)
-- Name: commentaire id_commentaire; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.commentaire ALTER COLUMN id_commentaire SET DEFAULT nextval('public.commentaire_id_commentaire_seq'::regclass);


--
-- TOC entry 3246 (class 2604 OID 17534)
-- Name: composition id_composition; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.composition ALTER COLUMN id_composition SET DEFAULT nextval('public.composition_id_composition_seq'::regclass);


--
-- TOC entry 3241 (class 2604 OID 17396)
-- Name: ingredient id_ingredient; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ingredient ALTER COLUMN id_ingredient SET DEFAULT nextval('public.ingredient_id_ingredient_seq'::regclass);


--
-- TOC entry 3245 (class 2604 OID 17429)
-- Name: recette id_recette; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.recette ALTER COLUMN id_recette SET DEFAULT nextval('public.recette_id_recette_seq'::regclass);


--
-- TOC entry 3242 (class 2604 OID 17403)
-- Name: restaurant id_restaurant; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.restaurant ALTER COLUMN id_restaurant SET DEFAULT nextval('public.restaurant_id_restaurant_seq'::regclass);


--
-- TOC entry 3243 (class 2604 OID 17410)
-- Name: type_recette id_type_recette; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.type_recette ALTER COLUMN id_type_recette SET DEFAULT nextval('public.type_recette_id_type_recette_seq'::regclass);


--
-- TOC entry 3418 (class 0 OID 17414)
-- Dependencies: 222
-- Data for Name: chef; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.chef VALUES (4, 'Hupi', 'Robert', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'roberthue@gmail.com', 4);
INSERT INTO public.chef VALUES (5, 'buzin', 'Pedro', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'pedrobuzin@hot.com', 2);


--
-- TOC entry 3422 (class 0 OID 17457)
-- Dependencies: 226
-- Data for Name: commentaire; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.commentaire VALUES (1, 4, '2023-04-19 11:57:23', 'C''etait super bon', 8.00, 1);
INSERT INTO public.commentaire VALUES (4, 4, '2023-04-20 14:13:12', 'whoua', 9.00, 4);
INSERT INTO public.commentaire VALUES (4, 4, '2023-04-20 16:13:24', 'c''est trop bon les lasagnes', 9.00, 5);
INSERT INTO public.commentaire VALUES (9, 4, '2023-04-20 16:22:40', 'un bon gratin facile à f aire', 6.00, 6);
INSERT INTO public.commentaire VALUES (1, 4, '2023-04-24 13:30:42', 'Hmmmm quel delice', 8.00, 27);
INSERT INTO public.commentaire VALUES (1, 4, '2023-04-24 13:34:15', 'ok test', 5.00, 28);
INSERT INTO public.commentaire VALUES (4, 4, '2023-04-24 13:38:16', 'bien bien', 9.00, 29);
INSERT INTO public.commentaire VALUES (10, 4, '2023-04-24 14:46:02', 'pas mauvais', 7.00, 31);
INSERT INTO public.commentaire VALUES (10, 4, '2023-04-24 14:47:38', 'pô bon ', 2.00, 32);
INSERT INTO public.commentaire VALUES (9, 4, '2023-04-24 14:48:17', 'mouais bof', 4.00, 33);


--
-- TOC entry 3421 (class 0 OID 17442)
-- Dependencies: 225
-- Data for Name: composition; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.composition VALUES (1, 5, 200, 1);
INSERT INTO public.composition VALUES (1, 7, 150, 2);
INSERT INTO public.composition VALUES (1, 6, 200, 3);
INSERT INTO public.composition VALUES (1, 4, 250, 4);
INSERT INTO public.composition VALUES (9, 1, 1, 5);
INSERT INTO public.composition VALUES (4, 17, 25, 12);
INSERT INTO public.composition VALUES (10, 6, 100, 9);
INSERT INTO public.composition VALUES (10, 27, 200, 10);
INSERT INTO public.composition VALUES (10, 2, 2, 31);
INSERT INTO public.composition VALUES (9, 6, 250, 6);
INSERT INTO public.composition VALUES (9, 3, 10, 7);
INSERT INTO public.composition VALUES (10, 32, 500, 11);
INSERT INTO public.composition VALUES (16, 6, 100, 35);


--
-- TOC entry 3412 (class 0 OID 17393)
-- Dependencies: 216
-- Data for Name: ingredient; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.ingredient VALUES (1, 'Poulet', 'viande', 'kg');
INSERT INTO public.ingredient VALUES (2, 'Tomate', 'légume', 'kg');
INSERT INTO public.ingredient VALUES (3, 'Champignon', 'légume', 'gramme');
INSERT INTO public.ingredient VALUES (4, 'Riz', 'féculent', 'gramme');
INSERT INTO public.ingredient VALUES (5, 'Asperges blanches', 'légume', 'gramme');
INSERT INTO public.ingredient VALUES (6, 'Crème Fraiche', 'produit laitier', 'gramme');
INSERT INTO public.ingredient VALUES (7, 'Roquefort', 'fromage', 'gramme');
INSERT INTO public.ingredient VALUES (8, 'Gruyere rapé', 'fromage', 'gramme');
INSERT INTO public.ingredient VALUES (10, 'Oignon', 'Légume', 'Unité');
INSERT INTO public.ingredient VALUES (11, 'Poivron', 'Légume', 'Unité');
INSERT INTO public.ingredient VALUES (12, 'Carotte', 'Légume', 'Unité');
INSERT INTO public.ingredient VALUES (13, 'Poulet', 'Viande', 'Gramme');
INSERT INTO public.ingredient VALUES (14, 'Boeuf', 'Viande', 'Gramme');
INSERT INTO public.ingredient VALUES (15, 'Lait', 'Produit Laitier', 'Millilitre');
INSERT INTO public.ingredient VALUES (16, 'Farine', 'Céréale', 'Gramme');
INSERT INTO public.ingredient VALUES (17, 'Sucre', 'Sucre', 'Gramme');
INSERT INTO public.ingredient VALUES (18, 'Pomme de terre', 'Légume', 'Unité');
INSERT INTO public.ingredient VALUES (19, 'Courgette', 'Légume', 'Unité');
INSERT INTO public.ingredient VALUES (20, 'Aubergine', 'Légume', 'Unité');
INSERT INTO public.ingredient VALUES (21, 'Chou-fleur', 'Légume', 'Unité');
INSERT INTO public.ingredient VALUES (22, 'Saumon', 'Poisson', 'Gramme');
INSERT INTO public.ingredient VALUES (23, 'Thon', 'Poisson', 'Gramme');
INSERT INTO public.ingredient VALUES (24, 'Mozzarella', 'Produit Laitier', 'Gramme');
INSERT INTO public.ingredient VALUES (25, 'Parmesan', 'Produit Laitier', 'Gramme');
INSERT INTO public.ingredient VALUES (26, 'Riz', 'Céréale', 'Gramme');
INSERT INTO public.ingredient VALUES (27, 'Quinoa', 'Céréale', 'Gramme');
INSERT INTO public.ingredient VALUES (28, 'Pâtes', 'Céréale', 'Gramme');
INSERT INTO public.ingredient VALUES (29, 'Huile d''olive', 'Huile', 'Millilitre');
INSERT INTO public.ingredient VALUES (30, 'Vinaigre balsamique', 'Vinaigre', 'Millilitre');
INSERT INTO public.ingredient VALUES (31, 'Miel', 'Sucre', 'Gramme');
INSERT INTO public.ingredient VALUES (32, 'Sirop d''érable', 'Sucre', 'Millilitre');
INSERT INTO public.ingredient VALUES (33, 'Basilic', 'Herbe', 'Gramme');
INSERT INTO public.ingredient VALUES (34, 'Persil', 'Herbe', 'Gramme');
INSERT INTO public.ingredient VALUES (35, 'Thym', 'Herbe', 'Gramme');
INSERT INTO public.ingredient VALUES (36, 'Cumin', 'Epice', 'Gramme');


--
-- TOC entry 3420 (class 0 OID 17426)
-- Dependencies: 224
-- Data for Name: recette; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.recette VALUES (4, 'Lasagne', 85, 8, '1.Eplucher les tomates', false, 3, 4, 'C:\Users\17010-27-06\Desktop\Projets JAVA\Objets-Exos\forknife\src\main\resources\com\forknife\lasagne.jpg', NULL);
INSERT INTO public.recette VALUES (1, 'Gratin d''asperges au Roquefort', 25, 10, '1. Faire cuire les asperges 20 minutes dans de l’eau bouillante. 
2. Bien égoutter les asperges et les disposer dans un plat à gratin.', true, 2, 4, 'C:\Users\17010-27-06\Desktop\Projets JAVA\Objets-Exos\forknife\src\main\resources\com\forknife\gratin-asperge.jpg', NULL);
INSERT INTO public.recette VALUES (9, 'Gratin de poulet aux Oeufs', 135, 8, '1-Enduire le poulet de crême fraîche
2-Mettre les champignons
3-faire cuire le tout', false, 2, 4, 'C:\Users\17010-27-06\Desktop\Projets JAVA\Objets-Exos\forknife\src\main\resources\com\forknife\gratin_poulet.jpg', NULL);
INSERT INTO public.recette VALUES (10, 'Choux à la crême', 85, 10, '1. Prenez le tout et mélanger', false, 1, 4, NULL, NULL);
INSERT INTO public.recette VALUES (16, 'test', 251, 12, 'yukh,', true, 1, 4, NULL, NULL);


--
-- TOC entry 3414 (class 0 OID 17400)
-- Dependencies: 218
-- Data for Name: restaurant; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.restaurant VALUES (2, 'Le Finisseur', '34 bis avenue du Geay', 'Bordeaux', '0123456789', NULL);
INSERT INTO public.restaurant VALUES (3, 'Ritournelle', '758 avenue du général', 'Paris', '0195874563', 'ritournelle@msn.com');
INSERT INTO public.restaurant VALUES (4, 'Fourchette&Co', '2 rue du grand jour', 'Paris', '0276543210', 'fco@gmail.com');
INSERT INTO public.restaurant VALUES (1, 'Le Gourmand', '19 avenue du rond point', 'Agen', '0458269847', NULL);


--
-- TOC entry 3416 (class 0 OID 17407)
-- Dependencies: 220
-- Data for Name: type_recette; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.type_recette VALUES (1, 'Entrée');
INSERT INTO public.type_recette VALUES (2, 'Plat');
INSERT INTO public.type_recette VALUES (3, 'Dessert');


--
-- TOC entry 3439 (class 0 OID 0)
-- Dependencies: 221
-- Name: chef_id_chef_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.chef_id_chef_seq', 6, true);


--
-- TOC entry 3440 (class 0 OID 0)
-- Dependencies: 227
-- Name: commentaire_id_commentaire_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.commentaire_id_commentaire_seq', 33, true);


--
-- TOC entry 3441 (class 0 OID 0)
-- Dependencies: 228
-- Name: composition_id_composition_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.composition_id_composition_seq', 35, true);


--
-- TOC entry 3442 (class 0 OID 0)
-- Dependencies: 215
-- Name: ingredient_id_ingredient_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.ingredient_id_ingredient_seq', 36, true);


--
-- TOC entry 3443 (class 0 OID 0)
-- Dependencies: 223
-- Name: recette_id_recette_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.recette_id_recette_seq', 16, true);


--
-- TOC entry 3444 (class 0 OID 0)
-- Dependencies: 217
-- Name: restaurant_id_restaurant_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.restaurant_id_restaurant_seq', 4, true);


--
-- TOC entry 3445 (class 0 OID 0)
-- Dependencies: 219
-- Name: type_recette_id_type_recette_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.type_recette_id_type_recette_seq', 3, true);


--
-- TOC entry 3255 (class 2606 OID 17419)
-- Name: chef chef_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.chef
    ADD CONSTRAINT chef_pkey PRIMARY KEY (id_chef);


--
-- TOC entry 3261 (class 2606 OID 17530)
-- Name: commentaire commentaire_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.commentaire
    ADD CONSTRAINT commentaire_pk PRIMARY KEY (id_commentaire);


--
-- TOC entry 3259 (class 2606 OID 17540)
-- Name: composition composition_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.composition
    ADD CONSTRAINT composition_pk PRIMARY KEY (id_composition);


--
-- TOC entry 3249 (class 2606 OID 17398)
-- Name: ingredient ingredient_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ingredient
    ADD CONSTRAINT ingredient_pkey PRIMARY KEY (id_ingredient);


--
-- TOC entry 3257 (class 2606 OID 17431)
-- Name: recette recette_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.recette
    ADD CONSTRAINT recette_pkey PRIMARY KEY (id_recette);


--
-- TOC entry 3251 (class 2606 OID 17405)
-- Name: restaurant restaurant_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.restaurant
    ADD CONSTRAINT restaurant_pkey PRIMARY KEY (id_restaurant);


--
-- TOC entry 3253 (class 2606 OID 17412)
-- Name: type_recette type_recette_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.type_recette
    ADD CONSTRAINT type_recette_pkey PRIMARY KEY (id_type_recette);


--
-- TOC entry 3262 (class 2606 OID 17420)
-- Name: chef chef_id_restaurant_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.chef
    ADD CONSTRAINT chef_id_restaurant_fkey FOREIGN KEY (id_restaurant) REFERENCES public.restaurant(id_restaurant);


--
-- TOC entry 3267 (class 2606 OID 17467)
-- Name: commentaire commentaire_id_chef_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.commentaire
    ADD CONSTRAINT commentaire_id_chef_fkey FOREIGN KEY (id_chef) REFERENCES public.chef(id_chef);


--
-- TOC entry 3268 (class 2606 OID 17462)
-- Name: commentaire commentaire_id_recette_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.commentaire
    ADD CONSTRAINT commentaire_id_recette_fkey FOREIGN KEY (id_recette) REFERENCES public.recette(id_recette);


--
-- TOC entry 3265 (class 2606 OID 17452)
-- Name: composition composition_id_ingredient_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.composition
    ADD CONSTRAINT composition_id_ingredient_fkey FOREIGN KEY (id_ingredient) REFERENCES public.ingredient(id_ingredient);


--
-- TOC entry 3266 (class 2606 OID 17447)
-- Name: composition composition_id_recette_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.composition
    ADD CONSTRAINT composition_id_recette_fkey FOREIGN KEY (id_recette) REFERENCES public.recette(id_recette);


--
-- TOC entry 3263 (class 2606 OID 17437)
-- Name: recette recette_id_chef_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.recette
    ADD CONSTRAINT recette_id_chef_fkey FOREIGN KEY (id_chef) REFERENCES public.chef(id_chef);


--
-- TOC entry 3264 (class 2606 OID 17432)
-- Name: recette recette_id_type_recette_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.recette
    ADD CONSTRAINT recette_id_type_recette_fkey FOREIGN KEY (id_type_recette) REFERENCES public.type_recette(id_type_recette);


-- Completed on 2023-04-25 09:22:03

--
-- PostgreSQL database dump complete
--

