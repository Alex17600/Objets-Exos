module com.forknife {
    requires transitive javafx.controls;
    requires javafx.fxml;
    requires javafx.base;
    requires transitive javafx.graphics;
    requires transitive java.sql;
    requires org.apache.commons.codec;
    requires transitive org.controlsfx.controls;

    opens com.forknife to javafx.fxml;
    opens com.forknife.DAO to javafx.fxml;
    opens com.forknife.Modele to javafx.fxml;
    opens com.forknife.Controlleur to javafx.fxml, org.controlsfx.controls;

    exports com.forknife.DAO;
    exports com.forknife.Modele;
    exports com.forknife.Controlleur;
    exports com.forknife;
}
