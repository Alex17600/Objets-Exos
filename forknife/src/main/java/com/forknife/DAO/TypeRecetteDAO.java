package com.forknife.DAO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.forknife.Modele.TypeRecette;

public class TypeRecetteDAO implements DAO<TypeRecette> {

    @Override
    public TypeRecette findById(int id) {
        TypeRecette type = new TypeRecette();

        try {
            PreparedStatement stm = connection.prepareStatement (
            "select * from type_recette where id_type_recette = ?;");
            stm.setInt(1, id);
            ResultSet rs = stm.executeQuery();
            if (rs.next()) {
                type.setIdTypeRecette(rs.getInt("id_type_recette"));
                type.setNom(rs.getString("nom"));
            }
            rs.close();
            stm.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return type;
    }

    @Override
    public List<TypeRecette> findAll() {
        List<TypeRecette> types = new ArrayList<>();

        try {
            PreparedStatement stm = connection.prepareStatement(
                    "SELECT * FROM type_recette;");
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                TypeRecette type = new TypeRecette();
                type.setIdTypeRecette(rs.getInt("id_type_recette"));
                type.setNom(rs.getString("nom"));

                types.add(type);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return types;
    }

    @Override
    public int save(TypeRecette t) {
        throw new UnsupportedOperationException("Unimplemented method 'save'");
    }
   
}
