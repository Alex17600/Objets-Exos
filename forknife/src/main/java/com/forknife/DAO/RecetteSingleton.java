package com.forknife.DAO;

import com.forknife.Modele.Recette;

public class RecetteSingleton {
    public Recette recette;
    private static RecetteSingleton instance;
    
    private RecetteSingleton (){
    }
    
    private RecetteSingleton(Recette recette) {
        // The following code emulates slow initialization.
        try {
            Thread.sleep(1000);
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
        this.recette = recette;
    }

    public static RecetteSingleton getInstance() {
        if (instance == null) {
            instance = new RecetteSingleton();
        }
        return instance;
    }

    public Recette getRecette() {
        return recette;
    }

    public void setRecette(Recette recette) {
        this.recette = recette;
    }

}
