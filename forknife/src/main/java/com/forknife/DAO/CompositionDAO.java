package com.forknife.DAO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import com.forknife.Modele.Composition;

public class CompositionDAO implements DAO<Composition> {

    public Composition findById(int id) {
        Composition composition = new Composition();
        IngredientDAO ingredientDAO = new IngredientDAO();
        RecetteDAO recetteDAO = new RecetteDAO();

        try {
            PreparedStatement stm = connection.prepareStatement(
                    "select * from composition where id_composition =?;");
            stm.setInt(1, id);
            ResultSet rs = stm.executeQuery();
            if (rs.next()) {
                composition.setIdComposition(rs.getInt("id_composition"));
                composition.setQuantite(rs.getInt("quantité"));
                composition.setIngredient(ingredientDAO.findById(rs.getInt("id_ingredient")));
                composition.setRecette(recetteDAO.findById(rs.getInt("id_recette")));
            }
            rs.close();
            stm.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return composition;
    }

    @Override
    public List<Composition> findAll() {
        List<Composition> compositions = new ArrayList<>();
        IngredientDAO ingredientDAO = new IngredientDAO();
        RecetteDAO recetteDAO = new RecetteDAO();

        try {
            PreparedStatement stm = connection.prepareStatement(
                    "SELECT * FROM composition;");
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Composition composition = new Composition();
                composition.setIdComposition(rs.getInt("id_composition"));
                composition.setQuantite(rs.getInt("quantité"));
                composition.setIngredient(ingredientDAO.findById(rs.getInt("id_ingredient")));
                composition.setRecette(recetteDAO.findById(rs.getInt("id_recette")));

                compositions.add(composition);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return compositions;
    }

    @Override
    public int save(Composition composition) {
        int id = 0;
        try {
            PreparedStatement stm = connection.prepareStatement(
                    "INSERT INTO composition (quantité, id_ingredient, id_recette) VALUES (?, ?, ?) returning id_composition");
            stm.setInt(1, composition.getQuantite());
            stm.setInt(2, composition.getIngredient().getIdIngredient());
            stm.setInt(3, composition.getRecette().getIdRecette());
   
            ResultSet rs = stm.executeQuery();
            if (rs.next()){
                id = rs.getInt(1);
            }
            stm.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return id;
    }

    public List<Composition> findByRecetteId(int idRecette) {
        List<Composition> compositions = new ArrayList<>();
        IngredientDAO ingredientDAO = new IngredientDAO();
        
        try {
            PreparedStatement stm = connection.prepareStatement(
                    "SELECT * FROM composition WHERE id_recette = ?");
            stm.setInt(1, idRecette);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Composition composition = new Composition();
                composition.setIdComposition(rs.getInt("id_composition"));
                composition.setQuantite(rs.getInt("quantité"));
                composition.setIngredient(ingredientDAO.findById(rs.getInt("id_ingredient")));

                compositions.add(composition);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return compositions;
    }

    public boolean update(Composition composition) {
        try {
            PreparedStatement stm = connection.prepareStatement(
                    "UPDATE composition SET id_recette = ?, id_ingredient = ?, quantité = ? WHERE id_composition = ?;");
            stm.setInt(1, composition.getRecette().getIdRecette());
            stm.setInt(2, composition.getIngredient().getIdIngredient());
            stm.setInt(3, composition.getQuantite());
            stm.setInt(4, composition.getIdComposition());

            int rowsUpdated = stm.executeUpdate();
            stm.close();
            return rowsUpdated > 0;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public void deleteComposition(int id) {
        try {

            PreparedStatement stm0 = connection.prepareStatement(
                    "delete from composition where id_composition = ?");

            stm0.setInt(1, id);
            stm0.executeUpdate();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
