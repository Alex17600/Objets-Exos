package com.forknife.DAO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.forknife.Modele.Restaurant;

public class RestaurantDAO implements DAO<Restaurant> {

    @Override
    public Restaurant findById(int id) {
        Restaurant restaurant = new Restaurant();

        try {
            PreparedStatement stm = connection.prepareStatement(
                    "SELECT * FROM restaurant WHERE id_restaurant = ?;");
            stm.setInt(1, id);
            ResultSet result = stm.executeQuery();
            if (result.next()) {
                restaurant.setId_restaurant(result.getInt("id_restaurant"));
                restaurant.setNom(result.getString("nom"));
                restaurant.setAdresse(result.getString("adresse"));
                restaurant.setVille(result.getString("ville"));
                restaurant.setNumTel(result.getString("num_tel"));
                restaurant.setEmail(result.getString("email"));
            }

            result.close();
            stm.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return restaurant;
    }

    @Override
    public List<Restaurant> findAll() {
        List<Restaurant> restaurants = new ArrayList<>();
        try {
            PreparedStatement stm = connection.prepareStatement(
                    "SELECT * FROM restaurant;");
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Restaurant restaurant = new Restaurant();
                restaurant.setId_restaurant(rs.getInt("id_restaurant"));
                restaurant.setNom(rs.getString("nom"));
                restaurant.setAdresse(rs.getString("adresse"));
                restaurant.setVille(rs.getString("ville"));
                restaurant.setNumTel(rs.getString("num_tel"));
                restaurant.setEmail(rs.getString("email"));

                restaurants.add(restaurant);
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        return restaurants;
    }

    @Override
    public int save(Restaurant t) {
        throw new UnsupportedOperationException("Unimplemented method 'save'");
    }
    
}
