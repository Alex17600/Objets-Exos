package com.forknife.DAO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.codec.digest.DigestUtils;

import com.forknife.Modele.Chef;
import com.forknife.Modele.Restaurant;

public class ChefDAO implements DAO<Chef> {

    @Override
    public Chef findById(int id) {
        Chef chef = new Chef();

        try {
            PreparedStatement stm = connection.prepareStatement(
                    "SELECT * FROM chef WHERE id_chef = ?;");
            stm.setInt(1, id);
            ResultSet result = stm.executeQuery();
            if (result.next()) {
                chef.setIdChef(result.getInt("id_chef"));
                chef.setNom(result.getString("nom"));
                chef.setPrenom(result.getString("prenom"));
                chef.setEmail(result.getString("email"));
                chef.setMotdepasse(result.getString("mot_de_passe"));
            }

            result.close();
            stm.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return chef;
    }

    @Override
    public List<Chef> findAll() {
        List<Chef> chefs = new ArrayList<>();
        RestaurantDAO rsDAO = new RestaurantDAO();
        List<Restaurant> restaurants = rsDAO.findAll();
        try {
            PreparedStatement stm = connection.prepareStatement(
                    "SELECT * FROM chef ");
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Chef chef = new Chef();
                chef.setIdChef(rs.getInt("id_chef"));
                chef.setNom(rs.getString("nom"));
                chef.setPrenom(rs.getString("prenom"));
                chef.setEmail(rs.getString("email"));
                chef.setMotdepasse(rs.getString("mot_de_passe"));
                int restaurantId = rs.getInt("id_restaurant");
                for (Restaurant restaurant : restaurants) {
                    if (restaurant.getId_restaurant() == restaurantId) {
                        chef.setRestaurant(restaurant);
                        break;
                    }
                }
                chefs.add(chef);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return chefs;
    }

    public Chef findByIdentifiantAndPassword(String identifiant, String motdepasse) {
        Chef chef = null;
        try {
            
            String mdpHache = DigestUtils.sha1Hex(motdepasse);
            PreparedStatement stm = connection.prepareStatement(
                    "SELECT * FROM chef WHERE email = ? AND mot_de_passe = ?;");
            stm.setString(1, identifiant);
            stm.setString(2, mdpHache);
            ResultSet result = stm.executeQuery();
            if (result.next()) {
                chef = new Chef();
                chef.setIdChef(result.getInt("id_chef"));
                chef.setNom(result.getString("nom"));
                chef.setPrenom(result.getString("prenom"));
                chef.setEmail(result.getString("email"));
                chef.setMotdepasse(result.getString("mot_de_passe"));
                int restaurantId = result.getInt("id_restaurant");
                RestaurantDAO restaurantDAO = new RestaurantDAO();
                Restaurant restaurant = restaurantDAO.findById(restaurantId);
                chef.setRestaurant(restaurant);
            }

            result.close();
            stm.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return chef;
    }

    @Override
    public int save(Chef t) {
        throw new UnsupportedOperationException("Unimplemented method 'save'");
    }

}
