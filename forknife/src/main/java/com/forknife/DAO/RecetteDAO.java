package com.forknife.DAO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import com.forknife.Modele.Commentaire;
import com.forknife.Modele.Recette;

public class RecetteDAO implements DAO<Recette> {
    TypeRecetteDAO typeRecetteDAO = new TypeRecetteDAO();
    ChefDAO chefDao = new ChefDAO();

    @Override
    public Recette findById(int id) {
        Recette recette = new Recette();

        try {
            PreparedStatement stm = connection.prepareStatement(
                    "SELECT * FROM recette WHERE id_recette = ?;");
            stm.setInt(1, id);
            ResultSet result = stm.executeQuery();
            if (result.next()) {
                recette.setIdRecette(result.getInt("id_recette"));
                recette.setNom(result.getString("nom"));
                recette.setTempsPrepa(result.getInt("temps_prepa"));
                recette.setNbrCouvert(result.getInt("nbr_couvert"));
                recette.setPrepa(result.getString("prepa"));
                recette.setVegetarien(result.getBoolean("vege"));
                recette.setType(typeRecetteDAO.findById(result.getInt("id_type_recette")));
                recette.setChef(chefDao.findById(result.getInt("id_chef")));
                String imageName = result.getString("image");
                recette.setImage(imageName);
            }

            result.close();
            stm.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return recette;
    }

    @Override
    public List<Recette> findAll() {
        List<Recette> recettes = new ArrayList<>();
        TypeRecetteDAO typeRecetteDAO = new TypeRecetteDAO();
        ChefDAO chefDao = new ChefDAO();
        CommentaireDAO commentaireDAO = new CommentaireDAO();
        try {
            PreparedStatement stm = connection.prepareStatement(
                    "SELECT * FROM recette;");
            ResultSet result = stm.executeQuery();
            while (result.next()) {
                Recette recette = new Recette();
                recette.setIdRecette(result.getInt("id_recette"));
                recette.setNom(result.getString("nom"));
                recette.setTempsPrepa(result.getInt("temps_prepa"));
                recette.setNbrCouvert(result.getInt("nbr_couvert"));
                recette.setPrepa(result.getString("prepa"));
                recette.setVegetarien(result.getBoolean("vege"));
                recette.setType(typeRecetteDAO.findById(result.getInt("id_type_recette")));
                recette.setChef(chefDao.findById(result.getInt("id_chef")));
                recette.setImage(result.getString("image"));
                

                List<Commentaire> commentaires = commentaireDAO.findByRecette(recette);
                recette.setListCom((ArrayList<Commentaire>) commentaires);

                recettes.add(recette);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return recettes;
    }

    @Override
    public int save(Recette recette) {
        int id = 0;
        try {
            PreparedStatement stm = connection.prepareStatement(
                    "INSERT INTO recette (nom, temps_prepa, nbr_couvert, prepa, vege, id_type_recette, id_chef, image) VALUES (?, ?, ?, ?, ?, ?, ?, ?) returning id_recette;");
            stm.setString(1, recette.getNom());
            stm.setInt(2, recette.getTempsPrepa());
            stm.setInt(3, recette.getNbrCouvert());
            stm.setString(4, recette.getPrepa());
            stm.setBoolean(5, recette.getVegetarien());
            stm.setInt(6, recette.getType().getIdTypeRecette());
            stm.setInt(7, recette.getChef().getIdChef());
            stm.setString(8, recette.getImage());

            ResultSet resulSet = stm.executeQuery();
            if (resulSet.next()) {
                id = resulSet.getInt(1);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return id;
    }

    public boolean update(Recette recette) {
        try {
            PreparedStatement stm = connection.prepareStatement(
                    "UPDATE recette SET nom = ?, temps_prepa = ?, nbr_couvert = ?, prepa = ?, vege = ?, id_type_recette = ?, id_chef = ?, image = ? WHERE id_recette = ?;");
            stm.setString(1, recette.getNom());
            stm.setInt(2, recette.getTempsPrepa());
            stm.setInt(3, recette.getNbrCouvert());
            stm.setString(4, recette.getPrepa());
            stm.setBoolean(5, recette.getVegetarien());
            stm.setInt(6, recette.getType().getIdTypeRecette());
            stm.setInt(7, recette.getChef().getIdChef());
            stm.setString(8, recette.getImage());
            stm.setInt(9, recette.getIdRecette());

            int rowsUpdated = stm.executeUpdate();
            stm.close();
            return rowsUpdated > 0;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public void deleteRecette(int id) {
        try {

            PreparedStatement stm0 = connection.prepareStatement(
                    "delete from commentaire where id_recette = ?");
            PreparedStatement stm1 = connection.prepareStatement(
                    "delete from composition where id_recette = ?");
            PreparedStatement stm2 = connection.prepareStatement(
                    "delete from recette where id_recette = ?");

            stm0.setInt(1, id);
            stm0.addBatch();

            stm1.setInt(1, id);
            stm1.addBatch();

            stm2.setInt(1, id);
            stm2.addBatch();

            // execute the batch
            stm0.executeBatch();
            stm1.executeBatch();
            stm2.executeBatch();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
