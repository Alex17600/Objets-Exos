package com.forknife.DAO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public final class ConnectionPostgreSQL {

    private static Connection connection;
    private static String mdp = "123456";
    private ConnectionPostgreSQL() {
    }

    public static Connection getInstance() {
        if (connection == null) {
            try {
                connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/forknife", "forknife",
                    mdp);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return connection;
    }
}

