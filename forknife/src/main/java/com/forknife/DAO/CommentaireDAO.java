package com.forknife.DAO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.forknife.Modele.Commentaire;
import com.forknife.Modele.Recette;

public class CommentaireDAO implements DAO<Commentaire> {

    @Override
    public Commentaire findById(int id) {
        Commentaire commentaire = new Commentaire();
        RecetteDAO recetteDAO = new RecetteDAO();
        ChefDAO chefDAO = new ChefDAO();
        try {
            PreparedStatement stm = connection.prepareStatement(
                    "SELECT * FROM recette WHERE id_commentaire = ?;");
            stm.setInt(1, id);
            ResultSet result = stm.executeQuery();
            if (result.next()) {
                commentaire.setRecette(recetteDAO.findById(result.getInt("id_recette")));
                commentaire.setChef(chefDAO.findById(result.getInt("id_chef")));
                commentaire.setDatePoste(result.getTimestamp("date_poste"));
                commentaire.setContenu(result.getString("contenu"));
                commentaire.setNote(result.getInt("note"));
            }

            result.close();
            stm.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return commentaire;
    }

    @Override
    public List<Commentaire> findAll() {
        List<Commentaire> commentaires = new ArrayList<>();
        RecetteDAO recetteDAO = new RecetteDAO();
        ChefDAO chefDAO = new ChefDAO();
        try {
            PreparedStatement stm = connection.prepareStatement(
                    "SELECT * FROM ingredient;");
            ResultSet result = stm.executeQuery();
            while (result.next()) {
                Commentaire commentaire = new Commentaire();
                commentaire.setRecette(recetteDAO.findById(result.getInt("id_recette")));
                commentaire.setChef(chefDAO.findById(result.getInt("id_chef")));
                commentaire.setDatePoste(result.getTimestamp("date_poste"));
                commentaire.setContenu(result.getString("contenu"));
                commentaire.setNote(result.getInt("note"));
                
                commentaires.add(commentaire);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return commentaires;
    }

    @Override
    public int save(Commentaire commentaire) {
        int id = 0;
        try {
            PreparedStatement stm = connection.prepareStatement(
                    "INSERT INTO commentaire (id_recette, id_chef, date_poste, contenu, note) VALUES (?, ?, ?, ?, ?) returning id_commentaire");
            stm.setInt(1, commentaire.getRecette().getIdRecette());
            stm.setInt(2, commentaire.getChef().getIdChef());
            stm.setTimestamp(3, commentaire.getDatePoste());
            stm.setString(4, commentaire.getContenu());
            stm.setInt(5, commentaire.getNote());
   
            ResultSet rs = stm.executeQuery();
            if (rs.next()){
                id = rs.getInt(1);
            }
            stm.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return id;
    }

    public List<Commentaire> findByRecette(Recette recette) {
        List<Commentaire> commentaires = new ArrayList<>();
        ChefDAO chefDAO = new ChefDAO();
        try {
            PreparedStatement stm = connection.prepareStatement(
                    "SELECT * FROM commentaire where id_recette = ?;");
                    stm.setInt(1, recette.getIdRecette());
            ResultSet result = stm.executeQuery();
            while (result.next()) {
                Commentaire commentaire = new Commentaire();
                commentaire.setIdComment(result.getInt("id_commentaire"));
                commentaire.setRecette(recette);
                commentaire.setChef(chefDAO.findById(result.getInt("id_chef")));
                commentaire.setDatePoste(result.getTimestamp("date_poste"));
                commentaire.setContenu(result.getString("contenu"));
                commentaire.setNote(result.getInt("note"));
    
                commentaires.add(commentaire);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return commentaires;
    }

    public List<Commentaire> findByRecetteid(int recetteId) {
        List<Commentaire> commentaires = new ArrayList<>();
        RecetteDAO recetteDAO = new RecetteDAO();
        ChefDAO chefDAO = new ChefDAO();
        try {
            PreparedStatement stm = connection.prepareStatement(
                    "SELECT * FROM commentaire where id_recette = ?;");
                    stm.setInt(1, recetteId);
            ResultSet result = stm.executeQuery();
            while (result.next()) {
                Commentaire commentaire = new Commentaire();
                commentaire.setIdComment(result.getInt("id_commentaire"));
                commentaire.setRecette(recetteDAO.findById(result.getInt("id_recette")));
                commentaire.setChef(chefDAO.findById(result.getInt("id_chef")));
                commentaire.setDatePoste(result.getTimestamp("date_poste"));
                commentaire.setContenu(result.getString("contenu"));
                commentaire.setNote(result.getInt("note"));
    
                commentaires.add(commentaire);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return commentaires;
    }

    public void deleteCommentaire(int id) {
        try {
            PreparedStatement stm = connection.prepareStatement(
                    "delete from commentaire where id_commentaire = ?");

            stm.setInt(1, id);
            stm.executeUpdate();
            stm.close();
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
