package com.forknife.DAO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import com.forknife.Modele.Ingredient;

public class IngredientDAO implements DAO<Ingredient> {

    @Override
    public Ingredient findById(int id) {
        Ingredient ingredient = new Ingredient();

        try {
            PreparedStatement stm = connection.prepareStatement(
                    "SELECT * FROM ingredient WHERE id_ingredient = ?;");
            stm.setInt(1, id);
            ResultSet result = stm.executeQuery();
            if (result.next()) {
                ingredient.setIdIngredient(result.getInt("id_ingredient"));
                ingredient.setNom(result.getString("nom"));
                ingredient.setTypeAliment(result.getString("type_aliment"));
                ingredient.setUnitMesure(result.getString("unit_mesure"));
            }

            result.close();
            stm.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return ingredient;
    }

    public Ingredient findByName(String name) {
       Ingredient ingredient = new Ingredient();

       try {
        PreparedStatement stm = connection.prepareStatement( "select * from ingredient where nom like ?;");
        stm.setString(1, name);
        ResultSet rs = stm.executeQuery();
        if (rs.next()) {
            ingredient.setIdIngredient(rs.getInt("id_ingredient"));
            ingredient.setNom(rs.getString("nom"));
            ingredient.setTypeAliment(rs.getString("type_aliment"));
            ingredient.setUnitMesure(rs.getString("unit_mesure"));
        }
        rs.close();
        stm.close();
       } catch (Exception e) {
        e.printStackTrace();
       }
       return ingredient;
    } 

    @Override
    public List<Ingredient> findAll() {
        List<Ingredient> ingredients = new ArrayList<>();

        try {
            PreparedStatement stm = connection.prepareStatement(
                    "SELECT * FROM ingredient;");
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Ingredient ingredient = new Ingredient();
                ingredient.setIdIngredient(rs.getInt("id_ingredient"));
                ingredient.setNom(rs.getString("nom"));
                ingredient.setTypeAliment(rs.getString("type_aliment"));
                ingredient.setUnitMesure(rs.getString("unit_mesure"));
                
                ingredients.add(ingredient);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ingredients;
    }


    @Override
    public int save(Ingredient t) {
        throw new UnsupportedOperationException("Unimplemented method 'save'");
    }
    
}
