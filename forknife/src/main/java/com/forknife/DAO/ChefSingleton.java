package com.forknife.DAO;

import com.forknife.Modele.Chef;

public class ChefSingleton {
    public Chef chef;
    private static ChefSingleton instance;

    private ChefSingleton () {
    }
    
    private ChefSingleton(Chef chef) {
        // The following code emulates slow initialization.
        try {
            Thread.sleep(1000);
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
        this.chef = chef;
    }

    public static ChefSingleton getInstance(Chef chef) {
        if (instance == null) {
            instance = new ChefSingleton(chef);
        }
        return instance;
    }

    public Chef getChef() {
        return chef;
    }

    public void setChef(Chef chef) {
        this.chef = chef;
    }
}
