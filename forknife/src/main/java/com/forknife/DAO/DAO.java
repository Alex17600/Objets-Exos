package com.forknife.DAO;

import java.sql.Connection;
import java.util.List;

public interface DAO<T> {
    public Connection connection = ConnectionPostgreSQL.getInstance();

  /**
     * Permet de récupérer un objet via son ID
     * @param id
     * @return
     */
    public abstract T findById(int id);
    
    /**
     * Permet de récupérer tous les objets dans une liste
     * @return
     */
    public abstract List<T> findAll();
    
    /**
     * Permet de créer une entrée dans la base de données
     * @param t
     */
    public abstract int save(T t);  
}
