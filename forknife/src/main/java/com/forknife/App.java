package com.forknife;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * JavaFX App
 */
public class App extends Application {
    public static final String cheminImage = "C:\\Users\\17010-27-06\\.forknife\\imageBDD\\";
    private static Scene scene;
    private static AnchorPane root;

    @Override
    public void start(Stage stage) throws IOException {
        root = FXMLLoader.load(getClass().getResource("main.fxml"));
        scene = new Scene(root, 1280, 1024);
        stage.setMaximized(true);
        stage.setScene(scene);
        stage.show();
        setCenterView("login.fxml");
    }

    public static void setCenterView(String vue) {
        try {
            Pane view = FXMLLoader.load(App.class.getResource(vue));
            BorderPane borderPane = (BorderPane)root.getChildren().get(0);
            borderPane.setRight(null);
            borderPane.setCenter(view);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void setRightView(String vue) {
        try {
            Pane view = FXMLLoader.load(App.class.getResource(vue));
            BorderPane borderPane = (BorderPane)root.getChildren().get(0);
            borderPane.setRight(view);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void setCenterView(Parent view) {
        BorderPane borderPane = (BorderPane)root.getChildren().get(0);
        borderPane.setRight(null);
        borderPane.setCenter(view);
    }

    public static void main(String[] args) {
        launch();
    }

}