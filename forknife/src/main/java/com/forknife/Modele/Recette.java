package com.forknife.Modele;

import java.util.ArrayList;

public class Recette {

  private int idRecette;
  private String nom;
  private int tempsPrepa;
  private int nbrCouvert;
  private String prepa;
  private boolean vegetarien;
  private TypeRecette type;
  private Chef chef;
  private String image;
  private ArrayList<Commentaire> listCom;

  public Recette(int idRecette, String nom, int tempsPrepa, int nbrCouvert, String prepa, boolean vegetarien,
      TypeRecette type, Chef chef, String image) {
    this.idRecette = idRecette;
    this.nom = nom;
    this.tempsPrepa = tempsPrepa;
    this.nbrCouvert = nbrCouvert;
    this.prepa = prepa;
    this.vegetarien = vegetarien;
    this.type = type;
    this.chef = chef;
    this.image = image;
  }

  public Recette() {
  }

  public Recette(String nom, int tempsPrepa, int nbrCouvert, String prepa, boolean vegetarien, TypeRecette type,
      Chef chef) {
    this.nom = nom;
    this.tempsPrepa = tempsPrepa;
    this.nbrCouvert = nbrCouvert;
    this.prepa = prepa;
    this.vegetarien = vegetarien;
    this.type = type;
    this.chef = chef;
  }

  public int getIdRecette() {
    return idRecette;
  }

  public void setIdRecette(int idRecette) {
    this.idRecette = idRecette;
  }

  public String getNom() {
    return nom;
  }

  public void setNom(String nom) {
    this.nom = nom;
  }

  public int getTempsPrepa() {
    return tempsPrepa;
  }

  public void setTempsPrepa(int tempsPrepa) {
    this.tempsPrepa = tempsPrepa;
  }

  public String getPrepa() {
    return prepa;
  }

  public void setPrepa(String prepa) {
    this.prepa = prepa;
  }

  public boolean getVegetarien() {
    return vegetarien;
  }

  public void setVegetarien(boolean vegetarien) {
    this.vegetarien = vegetarien;
  }

  public int getNbrCouvert() {
    return nbrCouvert;
  }

  public void setNbrCouvert(int nbrCouvert) {
    this.nbrCouvert = nbrCouvert;
  }

  public TypeRecette getType() {
    return type;
  }

  public void setType(TypeRecette type) {
    this.type = type;
  }

  public Chef getChef() {
    return chef;
  }

  public void setChef(Chef chef) {
    this.chef = chef;
  }

  public String getImage() {
    return image;
  }

  public void setImage(String image) {
    this.image = image;
  }

  public ArrayList<Commentaire> getListCom() {
    return listCom;
  }

  public void setListCom(ArrayList<Commentaire> listCom) {
    this.listCom = listCom;
  }

  @Override
  public String toString() {
    return nom;
  }

}
