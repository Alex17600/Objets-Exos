package com.forknife.Modele;

import java.util.ArrayList;

public class Chef {

  private int idChef;
  private String nom;
  private String prenom;
  private String email;
  private String motdepasse;
  private Restaurant restaurant;
  private ArrayList<Recette> arrayListOfRecette;
  private Recette recette;

  public Chef(int idChef, String nom, String prenom, String email, String motdepasse, Restaurant restaurant,
      ArrayList<Recette> arrayListOfRecette, Recette recette) {
    this.idChef = idChef;
    this.nom = nom;
    this.prenom = prenom;
    this.email = email;
    this.motdepasse = motdepasse;
    this.restaurant = restaurant;
    this.arrayListOfRecette = arrayListOfRecette;
    this.recette = recette;
  }

  public Chef() {
  }

  public int getIdChef() {
    return idChef;
  }

  public void setIdChef(int idChef) {
    this.idChef = idChef;
  }

  public String getNom() {
    return nom;
  }

  public void setNom(String nom) {
    this.nom = nom;
  }

  public String getPrenom() {
    return prenom;
  }

  public void setPrenom(String prenom) {
    this.prenom = prenom;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getMotdepasse() {
    return motdepasse;
  }

  public void setMotdepasse(String motdepasse) {
    this.motdepasse = motdepasse;
  }

  public Restaurant getRestaurant() {
    return restaurant;
  }

  public void setRestaurant(Restaurant restaurant) {
    this.restaurant = restaurant;
  }

  public ArrayList<Recette> getArrayListOfRecette() {
    return arrayListOfRecette;
  }

  public void setArrayListOfRecette(ArrayList<Recette> arrayListOfRecette) {
    this.arrayListOfRecette = arrayListOfRecette;
  }

  public Recette getRecette() {
    return recette;
  }

  public void setRecette(Recette recette) {
    this.recette = recette;
  }

  @Override
  public String toString() {
    return "Chef [idChef=" + idChef + ", nom=" + nom + ", prenom=" + prenom + ", email=" + email + ", motdepasse="
        + motdepasse + ", restaurant=" + restaurant;
  }
}
