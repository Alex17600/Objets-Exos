package com.forknife.Modele;

public class Ingredient {

  private int idIngredient;
  private String nom;
  private String typeAliment;
  private String unitMesure;
  private int quantite;

  public Ingredient(int idIngredient, String nom, String typeAliment, String unitMesure) {
    this.idIngredient = idIngredient;
    this.nom = nom;
    this.typeAliment = typeAliment;
    this.unitMesure = unitMesure;
  }

  public Ingredient() {
  }

  public String getNom() {
    return nom;
  }

  public void setNom(String nom) {
    this.nom = nom;
  }

  public String getTypeAliment() {
    return typeAliment;
  }

  public void setTypeAliment(String typeAliment) {
    this.typeAliment = typeAliment;
  }

  public String getUnitMesure() {
    return unitMesure;
  }

  public void setUnitMesure(String unitMesure) {
    this.unitMesure = unitMesure;
  }

  public int getIdIngredient() {
    return idIngredient;
  }

  public void setIdIngredient(int idIngredient) {
    this.idIngredient = idIngredient;
  }

  public int getQuantite() {
    return quantite;
  }

  public void setQuantite(int quantite) {
    this.quantite = quantite;
  }

  @Override
  public String toString() {
    return nom;
  }
}
