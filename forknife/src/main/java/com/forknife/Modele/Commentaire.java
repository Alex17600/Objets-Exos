package com.forknife.Modele;

import java.sql.Timestamp;

public class Commentaire {

  private int idComment;

  private Recette recette;

  private Chef chef;

  private Timestamp datePoste;

  private String contenu;

  private int note;

  public Commentaire(Recette recette, Chef chef, Timestamp datePoste, String contenu, int note) {
    this.recette = recette;
    this.chef = chef;
    this.datePoste = datePoste;
    this.contenu = contenu;
    this.note = note;
  }

  public Commentaire() {
  }

  public int getIdComment() {
    return idComment;
  }

  public void setIdComment(int idComment) {
    this.idComment = idComment;
  }

  public Recette getRecette() {
    return recette;
  }

  public void setRecette(Recette idRecette) {
    this.recette = idRecette;
  }

  public Chef getChef() {
    return chef;
  }

  public void setChef(Chef idChef) {
    this.chef = idChef;
  }

  public Timestamp getDatePoste() {
    return datePoste;
  }

  public void setDatePoste(Timestamp datePoste) {
    this.datePoste = datePoste;
  }

  public String getContenu() {
    return contenu;
  }

  public void setContenu(String contenu) {
    this.contenu = contenu;
  }

  public int getNote() {
    return note;
  }

  public void setNote(int note) {
    this.note = note;
  }

  @Override
  public String toString() {
    return "Commentaire [idRecette=" + recette + ", idChef=" + chef + ", datePoste=" + datePoste + ", contenu="
        + contenu + ", note=" + note + "]";
  }

}
