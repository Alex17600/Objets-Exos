package com.forknife.Modele;

public class Composition {

  private int idComposition;
  private Recette recette;
  private Ingredient ingredient;
  private int quantite;

  public Composition(int idComposition, Recette recette, Ingredient ingredient, int quantite) {
    this.idComposition = idComposition;
    this.recette = recette;
    this.ingredient = ingredient;
    this.quantite = quantite;
  }

  public Composition() {
  }

  public Recette getRecette() {
    return recette;
  }

  public void setRecette(Recette recette) {
    this.recette = recette;
  }

  public Ingredient getIngredient() {
    return ingredient;
  }

  public void setIngredient(Ingredient ingredient) {
    this.ingredient = ingredient;
  }

  public int getQuantite() {
    return quantite;
  }

  public void setQuantite(int quantite) {
    this.quantite = quantite;
  }

  public int getIdComposition() {
    return idComposition;
  }

  public void setIdComposition(int idComposition) {
    this.idComposition = idComposition;
  }

  @Override
  public String toString() {
    return "Composition=" + idComposition + ", idRecette=" + recette + ", idIngredient=" + ingredient
        + ", quantite=" + quantite;
  }

}
