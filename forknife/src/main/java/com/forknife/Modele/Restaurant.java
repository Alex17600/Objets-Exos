package com.forknife.Modele;

import java.util.ArrayList;

public class Restaurant {

  private int id_restaurant;
  private String nom;
  private String adresse;
  private String ville;
  private String numTel;
  private String email;
  private ArrayList<Chef> arrayListOfChef;

  public Restaurant(int id_restaurant, String nom, String adresse, String ville, String numTel, String email,
      ArrayList<Chef> arrayListOfChef) {
    this.id_restaurant = id_restaurant;
    this.nom = nom;
    this.adresse = adresse;
    this.ville = ville;
    this.numTel = numTel;
    this.email = email;
    this.arrayListOfChef = arrayListOfChef;
  }

  public Restaurant() {
  }

  public String getNom() {
    return nom;
  }

  public void setNom(String nom) {
    this.nom = nom;
  }

  public String getAdresse() {
    return adresse;
  }

  public void setAdresse(String adresse) {
    this.adresse = adresse;
  }

  public String getVille() {
    return ville;
  }

  public void setVille(String ville) {
    this.ville = ville;
  }

  public String getNumTel() {
    return numTel;
  }

  public void setNumTel(String numTel) {
    this.numTel = numTel;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public ArrayList<Chef> getArrayListOfChef() {
    return arrayListOfChef;
  }

  public void setArrayListOfChef(ArrayList<Chef> arrayListOfChef) {
    this.arrayListOfChef = arrayListOfChef;
  }

  public int getId_restaurant() {
    return id_restaurant;
  }

  public void setId_restaurant(int id_restaurant) {
    this.id_restaurant = id_restaurant;
  }

  @Override
  public String toString() {
    return "id_restaurant=" + id_restaurant + ", nom=" + nom + ", adresse=" + adresse + ", ville=" + ville
        + ", numTel=" + numTel + ", email=" + email + "]";
  }

}
