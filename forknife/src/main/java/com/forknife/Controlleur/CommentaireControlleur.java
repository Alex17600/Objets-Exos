package com.forknife.Controlleur;

import java.util.List;
import com.forknife.DAO.CommentaireDAO;
import com.forknife.Modele.Commentaire;
import com.forknife.Modele.Recette;
import com.forknife.DAO.RecetteSingleton;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class CommentaireControlleur {
    @FXML
    private VBox showComments;
    private RecetteSingleton recetteSelected = RecetteSingleton.getInstance();

    @FXML
    public void initialize() {
        Recette recette = recetteSelected.getRecette();

        CommentaireDAO commentaireDAO = new CommentaireDAO();
        List<Commentaire> commentaires = commentaireDAO.findByRecetteid(recette.getIdRecette());

        for (Commentaire commentaire : commentaires) {

            VBox commentaireBox = new VBox();
            commentaireBox.getStyleClass().add("commentaire-box");

            Label dateLabel = new Label("Date de poste : " + commentaire.getDatePoste());
            dateLabel.getStyleClass().add("date-label");
            Label auteurLabel = new Label("Auteur : " + commentaire.getChef().getNom());
            auteurLabel.getStyleClass().add("auteur-label");
            Label contenuLabel = new Label("Contenu : " + commentaire.getContenu());
            contenuLabel.getStyleClass().add("contenu-label");
            Label noteLabel = new Label("Note : " + commentaire.getNote() + "/10");
            noteLabel.getStyleClass().add("note-label");

            commentaireBox.getChildren().addAll(dateLabel, auteurLabel, contenuLabel, noteLabel);

            // Ajouter un bouton "Supprimer" pour chaque commentaire
            Button supprimerButton = new Button("Supprimer");
            supprimerButton.setPrefWidth(80); // définir la largeur du bouton
            supprimerButton.setOnAction(e -> supprimerCommentaire(commentaire, commentaireBox, commentaires));

            // Créer un HBox pour le bouton "Supprimer" et le mettre à côté des autres
            // éléments du commentaire
            HBox hbox = new HBox();
            hbox.getChildren().addAll(commentaireBox, supprimerButton);
            hbox.setSpacing(10); // définir l'espacement entre les éléments
            showComments.getChildren().add(hbox);
        }
    }

    private void supprimerCommentaire(Commentaire commentaire, VBox commentaireBox, List<Commentaire> commentaires) {
        CommentaireDAO commentaireDAO = new CommentaireDAO();
        commentaireDAO.deleteCommentaire(commentaire.getIdComment());
        commentaires.remove(commentaire);
        // Retirer le HBox correspondant au commentaire de la vue
        Node parent = commentaireBox.getParent();
        if (parent instanceof HBox) {
            showComments.getChildren().remove(parent);
        }
    }

}
