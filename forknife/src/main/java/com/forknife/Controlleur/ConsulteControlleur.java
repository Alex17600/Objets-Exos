package com.forknife.Controlleur;

import java.util.ArrayList;
import java.util.List;
import com.forknife.App;
import com.forknife.DAO.ChefSingleton;
import com.forknife.DAO.CompositionDAO;
import com.forknife.DAO.RecetteDAO;
import com.forknife.DAO.RecetteSingleton;
import com.forknife.Modele.Chef;
import com.forknife.Modele.Composition;
import com.forknife.Modele.Ingredient;
import com.forknife.Modele.Recette;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;

public class ConsulteControlleur {
    @FXML
    private TextField noteTexteField;
    @FXML
    private Label reponseVege;
    @FXML
    private TextField nbrCouvertTextfield;
    @FXML
    private TextArea infosRecette;
    @FXML
    private TextArea ingredientLabel;
    @FXML
    private TextArea prepaLabel;
    @FXML
    private Button updateButton;
    @FXML
    private Button supprimeButton;
    // Singletons
    private Chef chef;
    private RecetteSingleton recetteSelected = RecetteSingleton.getInstance();
    private ChefSingleton chefSingleton = ChefSingleton.getInstance(chef);

    public void setRecette(Recette recette) {
        recetteSelected.setRecette(recette);
        // rendre les texteAreas non editable au départ
        infosRecette.setEditable(false);
        ingredientLabel.setEditable(false);
        prepaLabel.setEditable(false);
        System.out.println(recetteSelected.getRecette().getType().getIdTypeRecette());
        CompositionDAO compositionDAO = new CompositionDAO();
        List<Composition> compositions = compositionDAO.findByRecetteId(recette.getIdRecette());
        String preparation = recette.getPrepa();

        // plat végé?
        if (recette.getVegetarien() == true) {
            reponseVege.setText("Oui");
        } else {
            reponseVege.setText("Non");
        }

        String nomRecette = recette.getNom();
        int tempsPrepa = recette.getTempsPrepa();
        int nbrCouvert = recette.getNbrCouvert();
        Chef chef = recette.getChef();
        String nomChef = chef.getNom();
        String prenomChef = chef.getPrenom();

        // Check pour savoir si tu est l'auteur de la recette que tu consulte
        Chef chefSelected = chefSingleton.getChef();
        if (chefSelected.getIdChef() == chef.getIdChef()) {
            supprimeButton.setVisible(true);
            updateButton.setVisible(true);
            infosRecette.setEditable(true);
            ingredientLabel.setEditable(true);
            prepaLabel.setEditable(true);
            updateButton.setOnAction(event -> {
                updateRecetteIfYouAreTheAutor();
            });
        } else {
            updateButton.setVisible(false);
            supprimeButton.setVisible(false);
            infosRecette.setEditable(false);
            ingredientLabel.setEditable(false);
            prepaLabel.setEditable(false);
        }

        // remplissage nombre de couvert par defaut
        nbrCouvertTextfield.setText(String.valueOf(nbrCouvert));

        // bloc 1
        String label1 = "Nom de la recette: " + nomRecette + "\n" + "Temps de préparation: " + tempsPrepa + "\n"
                + "Nombre de couverts: " + nbrCouvert + "\n" + "Createur: " + nomChef + " " + prenomChef;
        infosRecette.setText(label1);

        StringBuilder ingredientStr = new StringBuilder();
        // liste des ingrédients associés à la recette
        List<Ingredient> ingredients = new ArrayList<>();
        for (Composition composition : compositions) {
            ingredients.add(composition.getIngredient());
            ingredientStr.append(composition.getIngredient().getNom() + " " + composition.getQuantite() + " "
                    + composition.getIngredient().getUnitMesure()).append("\n");
        }

        ingredientLabel.setText(ingredientStr.toString());

        prepaLabel.setText(preparation);

        nbrCouvertTextfield.textProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null && !newValue.isEmpty()) {
                ingredientLabel.clear();
                for (Composition composition : compositions) {
                    int couvertsSelected = Integer.parseInt(newValue);
                    int quantite = composition.getQuantite() * couvertsSelected / nbrCouvert;
                    String nomIngredient = composition.getIngredient().getNom();
                    String quantiteStr = String.valueOf(quantite);
                    String unitMesure = composition.getIngredient().getUnitMesure();
                    if (couvertsSelected > 1) {
                        unitMesure = unitMesure + "s";
                    }
                    String ligneIngredient = nomIngredient + " " + quantiteStr + " " + unitMesure;
                    ingredientLabel.appendText(ligneIngredient + "\n");
                }
            }
        });
    }

    @FXML
    public void updateRecetteIfYouAreTheAutor() {
        App.setCenterView("ajoutRecette.fxml");
    }

    @FXML
    public void deleteRecetteByAuthor() {
        RecetteDAO recetteDAO = new RecetteDAO();
        recetteDAO.deleteRecette(recetteSelected.getRecette().getIdRecette());

        // Effacer les textareas
        infosRecette.setText("");
        ingredientLabel.setText("");
        prepaLabel.setText("");

        // Afficher un message de confirmation
        Alert alert = new Alert(AlertType.INFORMATION);
        alert.setTitle("Confirmation");
        alert.setHeaderText(null);
        alert.setContentText("Recette supprimée avec succès!");
        alert.showAndWait();
    }

    @FXML
    public void creerCommentaire() {
        App.setRightView("zoneCreerComment.fxml");
    }

    @FXML
    public void voirCommentaire() {
        App.setRightView("voirCommentaire.fxml");
    }

    @FXML
    public void retourConsulteTopVue() {
        App.setCenterView("consulte.fxml");
    }


}
