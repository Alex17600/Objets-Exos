package com.forknife.Controlleur;

import com.forknife.App;
import com.forknife.DAO.ChefDAO;
import com.forknife.Modele.Chef;
import com.forknife.DAO.ChefSingleton;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

public class LoginControlleur {
    @FXML
    private TextField identifiantTextfield;
    @FXML
    private PasswordField passwordTextefield;
    private ChefDAO chefDao;


    public LoginControlleur() {
        chefDao = new ChefDAO();
    }

    public void initialize() {
        identifiantTextfield.setText("roberthue@gmail.com");
        passwordTextefield.setText("123456");
    }

    public void connexion() {
        String identifiant = identifiantTextfield.getText();
        String password = passwordTextefield.getText();

        Chef chef = chefDao.findByIdentifiantAndPassword(identifiant, password);

        ChefSingleton.getInstance(chef);

        if (chef != null) {
            App.setCenterView("consulte.fxml");
        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Erreur de connexion");
            alert.setHeaderText("Identifiant ou mot de passe incorrect");
            alert.setContentText("Veuillez vérifier vos informations de connexion et réessayer.");
            alert.showAndWait();
        }
    }
}
