package com.forknife.Controlleur;

import java.util.Date;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

import com.forknife.App;
import com.forknife.DAO.ChefSingleton;
import com.forknife.DAO.CommentaireDAO;
import com.forknife.DAO.RecetteSingleton;
import com.forknife.Modele.Chef;
import com.forknife.Modele.Commentaire;
import com.forknife.Modele.Recette;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;

public class CommentaireCreerControlleur {
    @FXML
    private VBox vueCreerCommentaire;
    @FXML
    private TextField noteTexteField;
    @FXML
    private TextArea contenuComment;
    // Singletons
    private Chef chef;
    private ChefSingleton chefSingleton = ChefSingleton.getInstance(chef);
    private Recette recette;

    @FXML
    public void addComment() {
        // Récupérer l'instance du chef et recette à partir des singletons
        chef = chefSingleton.getChef();
        recette = RecetteSingleton.getInstance().getRecette();

        chef.getIdChef();
        CommentaireDAO commentaireDAO = new CommentaireDAO();

        // Obtenir la date et l'heure actuelles
        Date date = new Date();
        Timestamp timestamp = new Timestamp(date.getTime());
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String formattedDate = dateFormat.format(timestamp);

        // Créer un nouvel objet Commentaire avec la date et l'heure actuelles
        Commentaire commentaire = new Commentaire();
        commentaire.setRecette(recette);
        commentaire.setChef(chef);
        commentaire.setDatePoste(Timestamp.valueOf(formattedDate));
        commentaire.setContenu(contenuComment.getText());
        commentaire.setNote(Integer.parseInt(noteTexteField.getText()));

        // Enregistrer le commentaire dans la base de données
        commentaireDAO.save(commentaire);

        //informe que c'est enregistrer
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Information");
        alert.setContentText("Commentaire créée avec succès !");
        alert.showAndWait();

        App.setRightView("voirCommentaire.fxml");
    }
}
