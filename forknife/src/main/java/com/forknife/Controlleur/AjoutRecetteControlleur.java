package com.forknife.Controlleur;

import java.util.List;
import java.util.Optional;
import org.controlsfx.control.SearchableComboBox;
import com.forknife.App;
import com.forknife.DAO.ChefSingleton;
import com.forknife.DAO.CompositionDAO;
import com.forknife.DAO.IngredientDAO;
import com.forknife.DAO.RecetteDAO;
import com.forknife.DAO.RecetteSingleton;
import com.forknife.DAO.TypeRecetteDAO;
import com.forknife.Modele.Chef;
import com.forknife.Modele.Composition;
import com.forknife.Modele.Ingredient;
import com.forknife.Modele.Recette;
import com.forknife.Modele.TypeRecette;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ButtonBar;

public class AjoutRecetteControlleur {
    @FXML
    private TableView<Composition> tableViewingredient;
    @FXML
    private TableColumn<Composition, String> nomIngredient;
    @FXML
    private TableColumn<Composition, Integer> quantiteTableColumnIngredient;
    @FXML
    private CheckBox vegeCheck;
    @FXML
    private TextArea prepaTexteArea;
    @FXML
    private TextField quantiteEntre;
    @FXML
    private TextField textFieldNomRecette;
    @FXML
    private TextField textfieldTempsPrepa;
    @FXML
    private TextField textfieldNbrCouvert;
    @FXML
    private SearchableComboBox<Ingredient> searchableComboBox = new SearchableComboBox<Ingredient>();
    @FXML
    private Label untiMesure;
    @FXML
    private ComboBox<TypeRecette> typePlat;

    private RecetteSingleton recetteSelected = RecetteSingleton.getInstance();
    private ObservableList<TypeRecette> infoTypes = FXCollections.observableArrayList();
    private ObservableList<Composition> listeIngredients = FXCollections.observableArrayList();
    private CompositionDAO compositionDAO = new CompositionDAO();
    private IngredientDAO ingredientDAO = new IngredientDAO();
    private List<Ingredient> ingredients = ingredientDAO.findAll();
    private TypeRecetteDAO typeRecetteDAO = new TypeRecetteDAO();
    private List<TypeRecette> types = typeRecetteDAO.findAll();
    private RecetteDAO recetteDAO = new RecetteDAO();

    public void initialize() {

        infoTypes.addAll(types);

        if (recetteSelected.getRecette() != null) {
            textFieldNomRecette.setText(recetteSelected.getRecette().getNom());
            listeIngredients.setAll(compositionDAO.findByRecetteId(recetteSelected.getRecette().getIdRecette()));
            textfieldTempsPrepa.setText(Integer.toString(recetteSelected.getRecette().getTempsPrepa()));
            textfieldNbrCouvert.setText(Integer.toString(recetteSelected.getRecette().getNbrCouvert()));
            typePlat.getSelectionModel().select(recetteSelected.getRecette().getType());
            vegeCheck.setSelected(recetteSelected.getRecette().getVegetarien());
            prepaTexteArea.setText(recetteSelected.getRecette().getPrepa());
        }
        
        tableViewingredient.setItems(listeIngredients);


        typePlat.setItems(infoTypes);

        searchableComboBox.setEditable(true);
        searchableComboBox.getItems().addAll(ingredients);

        // initialize colonne tableau
        nomIngredient.setCellValueFactory(
                cellData -> new SimpleStringProperty(cellData.getValue().getIngredient().getNom()));
        quantiteTableColumnIngredient
                .setCellValueFactory(
                        cellData -> new SimpleIntegerProperty(cellData.getValue().getQuantite()).asObject());

        // initialize les unités au changements du choix de la liste
        searchableComboBox.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                untiMesure.setText(newValue.getUnitMesure());
            }
        });

    }

    @FXML
    public void ajoutIngredientsTableau() {
        Composition composition = new Composition();
        Ingredient ingredientName = ingredientDAO
                .findByName(searchableComboBox.getSelectionModel().getSelectedItem().getNom());
        // TODO
        // Utiliser l'API Stream, s'il te plait (et ça plaira à CapGemini)

        composition.setIngredient(ingredientName);
        composition.setQuantite(Integer.valueOf(quantiteEntre.getText()));
        listeIngredients.add(composition);
    }

    public void supprimeringredient () {

        Composition compo = tableViewingredient.getSelectionModel().getSelectedItem();

        if (compo.getIdComposition() != 0) {
        compositionDAO.deleteComposition(compo.getIdComposition());
        }
        int compoIngredient = tableViewingredient.getSelectionModel().getSelectedIndex();
        listeIngredients.remove(compoIngredient);
        tableViewingredient.refresh();
    }

    @FXML
    public void creerRecette() {

        Chef chefId = ChefSingleton.getInstance(null).chef;
        chefId.getIdChef();

        CompositionDAO compositionDAO = new CompositionDAO();

        String nomRecette = textFieldNomRecette.getText();
        int tempsPrepa = Integer.parseInt(textfieldTempsPrepa.getText());
        int nbrCouvert = Integer.parseInt(textfieldNbrCouvert.getText());
        String prepa = prepaTexteArea.getText();
        Boolean vege = vegeCheck.isSelected();
        TypeRecette typeRecette = typePlat.getSelectionModel().getSelectedItem();
        System.out.println(typeRecette.getIdTypeRecette());

        Recette recette = new Recette(nomRecette, tempsPrepa, nbrCouvert, prepa, vege, typeRecette, chefId);

        if (recetteSelected.getRecette() != null) {
            recette.setIdRecette(recetteSelected.getRecette().getIdRecette());
            for (Composition composition : listeIngredients) {
                composition.setRecette(recette);
                if (composition.getIdComposition() > 0) {
                    compositionDAO.update(composition);
                } else {
                    compositionDAO.save(composition);
                }
            }

            recetteDAO.update(recette);
           
            App.setCenterView("consulte.fxml");

        } else {
            int recetteId = recetteDAO.save(recette);
            recette.setIdRecette(recetteId);

            for (Composition composition : listeIngredients) {
                composition.setRecette(recette);
                compositionDAO.save(composition);
            }

            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Confirmation");
            alert.setHeaderText("Recette créée avec succès !");
            alert.setContentText("Voulez-vous ajouter une nouvelle recette ?");

            ButtonType buttonNouvelleRecette = new ButtonType("Nouvelle recette");
            ButtonType buttonFermer = new ButtonType("Fermer", ButtonBar.ButtonData.CANCEL_CLOSE);

            alert.getButtonTypes().setAll(buttonNouvelleRecette, buttonFermer);

            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == buttonNouvelleRecette) {
                textFieldNomRecette.setText("");
                textfieldTempsPrepa.setText("");
                textfieldNbrCouvert.setText("");
                prepaTexteArea.setText("");
                vegeCheck.setSelected(false);
                typePlat.getSelectionModel().clearSelection();
                listeIngredients.clear();
                tableViewingredient.refresh();
            } else {
                // Fermer la fenêtre ou retourner à la liste des recettes
            }
            App.setCenterView("consulte.fxml");

        }
    }
}
