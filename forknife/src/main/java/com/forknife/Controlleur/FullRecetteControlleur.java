package com.forknife.Controlleur;

import java.io.IOException;
import java.util.List;

import com.forknife.App;
import com.forknife.DAO.RecetteDAO;
import com.forknife.Modele.Commentaire;
import com.forknife.Modele.Recette;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

public class FullRecetteControlleur {
    @FXML
    private TableView<Recette> fullRecetteTableview;
    @FXML
    private TableColumn<Recette, String> nomTableColumn; 
    @FXML
    private TableColumn<Recette, String> auteurTableColumn;
    @FXML
    private TableColumn<Recette, String> noteTableColumn;
    private ObservableList<Recette> recettesVue = FXCollections.observableArrayList();

    public void initialize () {
        RecetteDAO recetteDAO = new RecetteDAO();

        List<Recette> recettes = recetteDAO.findAll();

        recettesVue.addAll(recettes);

        nomTableColumn.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getNom()));
        auteurTableColumn.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getChef().getNom()));
        noteTableColumn.setCellValueFactory(cellData -> new SimpleStringProperty(String.valueOf(cellData.getValue().getListCom().stream().mapToInt(Commentaire::getNote).average().orElse(0))));
       
        fullRecetteTableview.setItems(recettesVue);
    }

    public void voirRecette () throws IOException {
        Recette choix = fullRecetteTableview.getSelectionModel().getSelectedItem();
        RecetteDAO recetteDAO = new RecetteDAO();
        Recette recette = recetteDAO.findById(choix.getIdRecette());
        
        if (recette != null) {
            // Charger la vue pour afficher les informations de la recette
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/com/forknife/ficheRecette.fxml"));
            Parent parent = loader.load();
            ConsulteControlleur controlleur = loader.getController();
            controlleur.setRecette(recette);
            App.setCenterView(parent);
        }
    }

    public void retourTopRecette () {
        App.setCenterView("consulte.fxml");
    }
}
