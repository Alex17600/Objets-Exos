package com.forknife.Controlleur;

import java.io.IOException;
import java.util.Comparator;
import java.util.List;

import org.controlsfx.control.SearchableComboBox;

import com.forknife.App;
import com.forknife.DAO.CommentaireDAO;
import com.forknife.DAO.RecetteDAO;
import com.forknife.Modele.Commentaire;
import com.forknife.Modele.Recette;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class ConsulteTopControlleur {
    @FXML
    private Label nomRecette1;
    @FXML
    private Label nomRecette2;
    @FXML
    private Label nomRecette3;
    @FXML
    private Label nomRecette4;
    @FXML
    private Label note1;
    @FXML
    private Label note2;
    @FXML
    private Label note3;
    @FXML
    private Label note4;
    @FXML
    private ImageView imageView1;
    @FXML
    private ImageView imageView2;
    @FXML
    private ImageView imageView3;
    @FXML
    private ImageView imageView4;
    @FXML
    private SearchableComboBox<Recette> searchBar = new SearchableComboBox<Recette>();
    private RecetteDAO recetteDAO = new RecetteDAO();
    private List<Recette> recettes = recetteDAO.findAll();

    
    public void initialize() {
        
        //init des recettes en searchBar
        searchBar.setEditable(true);
        searchBar.getItems().addAll(recettes);

        //affichage recettes selectionné
        searchBar.valueProperty().addListener((observable, oldValue, newValue) -> {
            try {
                if (newValue != null) {
                    Recette choix = searchBar.getSelectionModel().getSelectedItem();
                    Recette recette = recetteDAO.findById(choix.getIdRecette());
                    consulteRecette(recette.getIdRecette());
                    }
            } catch (Exception e) {
                e.getMessage();
            }
        });


        if (!recettes.isEmpty()) {
            // Tri des recettes selon leur notes moyenne décroissante
            recettes.sort(Comparator.comparingDouble(r -> moyenneNotes(((Recette) r).getIdRecette())).reversed());
    
            int i = 1;
            for (Label nomLabel : new Label[] { nomRecette1, nomRecette2, nomRecette3, nomRecette4 }) {
                if (i <= recettes.size()) {
                    Recette recette = recettes.get(i - 1);
                    String nomRecette = recette.getNom();
                    nomLabel.setText(nomRecette);
    
                    String cheminImage = App.cheminImage + recette.getImage();
                    if (recette.getImage() != null ) {
                        Image image = new Image(cheminImage);
                        ImageView imageView = getImageViewByIndex(i);
                        imageView.setImage(image);
    
                        // Ajouter un événement pour ouvrir la fiche de la recette correspondante
                        nomLabel.setOnMouseClicked(event -> {
                            try {
                                consulteRecette(recette.getIdRecette());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        });
                    }else {
                        cheminImage = App.cheminImage + "placeholder.png";
                        Image image = new Image(cheminImage);
                        ImageView imageView = getImageViewByIndex(i);
                        imageView.setImage(image);
                        nomLabel.setOnMouseClicked(event -> {
                            try {
                                consulteRecette(recette.getIdRecette());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        });
                    }
    
                    // Afficher la note moyenne de la recette
                    double moyenne = moyenneNotes(recette.getIdRecette());
                    Label noteLabel = getNoteLabelByIndex(i);
                    noteLabel.setText(String.format("%.1f", moyenne));
                } else {
                    nomLabel.setText("");
                }
                i++;
            }
        }
    }
    

    private ImageView getImageViewByIndex(int index) {
        switch (index) {
            case 1:
                return imageView1;
            case 2:
                return imageView2;
            case 3:
                return imageView3;
            case 4:
                return imageView4;
            default:
                throw new IllegalArgumentException("Index must be between 1 and 4");
        }
    }

    private Label getNoteLabelByIndex(int index) {
        switch (index) {
            case 1:
                return note1;
            case 2:
                return note2;
            case 3:
                return note3;
            case 4:
                return note4;
            default:
                throw new IllegalArgumentException("Index must be between 1 and 4");
        }
    }

    public void openFullRecipes () {
        App.setCenterView("touteslesrecettes.fxml");
    }

    public void openAddRecipeWindow() {
        App.setCenterView("ajoutRecette.fxml");
    }

    public void consulteRecette(int index) throws IOException {
        RecetteDAO recetteDAO = new RecetteDAO();
        Recette recette = recetteDAO.findById(index);
        // Vérifier si la recette existe
        if (recette != null) {
            // Charger la vue pour afficher les informations de la recette
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/com/forknife/ficheRecette.fxml"));
            Parent parent = loader.load();
            ConsulteControlleur controlleur = loader.getController();

            controlleur.setRecette(recette);
            App.setCenterView(parent);
        }
    }

    public double moyenneNotes(int recetteId) {
        CommentaireDAO commentaireDAO = new CommentaireDAO();
        List<Commentaire> commentaires = commentaireDAO.findByRecetteid(recetteId);
        double sommeNotes = 0.0;
        int nbCommentaires = commentaires.size();
        for (Commentaire commentaire : commentaires) {
            sommeNotes += commentaire.getNote();
        }
        double moyenneNotes = (nbCommentaires > 0) ? sommeNotes / nbCommentaires : 0.0;
        return moyenneNotes;
    }
}
