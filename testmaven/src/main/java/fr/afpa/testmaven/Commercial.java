package fr.afpa.testmaven;

import java.sql.Date;

public class Commercial extends Employes {


    private double tauxComission;

    public Commercial (String matricule, String nom, String prenom, int salaireBrut, double tauxCharge, Date dateNaissance, double tauxComission) throws Exception {
        super(matricule, nom, prenom, salaireBrut, tauxCharge, dateNaissance);

    }

    // public chiffreAffaire(){
    // }

    // public calculComission(){
    // }

    public double getTauxComission() {
        return tauxComission;
    }

    

    @Override
    public String toString() {
        return "Commercial [tauxComission=" + tauxComission + "]";
    }

    @Override
    public double calculerSalaire() {
        
        return 0;
    }

}

