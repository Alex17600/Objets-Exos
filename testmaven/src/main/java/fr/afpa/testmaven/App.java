package fr.afpa.testmaven;

import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class App {
    /**
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {

        Date dateNaissance = new Date("02/25/2022");
        Date now = new Date();
        long diffInMillies = Math.abs(now.getTime() - dateNaissance.getTime());
        long diff = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);
        int i = 0;
        while (diff >= 365) {
            diff -= 365;
            i++;
            if (i % 4 == 0) {
                diff--;
            }
        }
        long diffInDays = 365 - diff;

        ArrayList<Employes> testings = new ArrayList<>();

        try {

            Employes newEmployes1 = new Employes("11AVC25", "Dupont", "Etienne", 1800, 0.2, dateNaissance);
            testings.add(newEmployes1);
            Employes newEmployes2 = new Employes("75GHS25", "Douglas", "Jacques", 2500, 0.3, dateNaissance);
            testings.add(newEmployes2);
            Employes newEmployes3 = new Employes("75FRE45", "Mitrocey", "Illona", 1500, 0.1, dateNaissance);
            testings.add(newEmployes3);
            Employes newEmployes4 = new Employes("78MIP38", "MacQueen", "Steve", 7500, 0.6, dateNaissance);
            testings.add(newEmployes4);


        } catch (Exception e) {
            System.out.println(e);
        }
        


        for (Employes sals : testings) {
            JsonTool.writeJSON(sals);
            // JsonTool.readJSON(sals.getNom());
            XMLTools.serializeToXML(sals);
            // XMLTools.deserializeFromXML(sals.getNom() + "donnees.xml");
            double salaireJP = sals.calculerSalaire();
            System.out.println(sals);
            System.out.println("Le salaire net est de: " + salaireJP);
            System.out.println("Son anniversaire sera dans: " + diffInDays + " jours");
        }

        // try {
        // FileOutputStream fos = new FileOutputStream("salaries.xml");
        // ObjectOutputStream oos = new ObjectOutputStream(fos);
        // oos.writeObject(testings);
        // oos.close();

        // } catch (IOException e) {
        // e.printStackTrace();
        // }
    }

}