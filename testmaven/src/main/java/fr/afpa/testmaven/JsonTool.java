package fr.afpa.testmaven;

import java.io.File;
import java.io.IOException;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonTool {
    public static void writeJSON(Employes employes) throws JsonGenerationException, JsonMappingException, IOException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(new File(employes.getNom() + "employes.json"), employes);
    }

    // public static Employes readJSON(String name) throws JsonParseException, JsonMappingException, IOException {
    //     ObjectMapper mapper = new ObjectMapper();
    //     Employes employes = mapper.readValue(new File("json"), Employes.class);
    //     return employes;
    // }

}
