package fr.afpa.testmaven;

import java.io.Serializable;
import java.util.Date;
import java.util.regex.*;

public class Employes implements Serializable {
    
    public Employes(){}

    private String matricule;
    private String nom;
    private String prenom;
    private int salaireBrut;
    private double tauxCharge;
    private double salaireNet;
    private Date dateNaissance;
    private static Pattern pattern;

    public Employes(String matricule, String nom, String prenom, int salaireBrut, double tauxCharge,
    Date dateNaissance) throws Exception {
        setMatricule(matricule);
        setNom(nom);
        setPrenom(prenom);
        setSalaireBrut(salaireBrut);
        setTauxCharge(tauxCharge);
        setDateNaissance(dateNaissance);
    }

    public Employes(Employes emp)throws Exception  {
        setMatricule(emp.matricule);
        setNom(emp.nom);
        setPrenom(emp.prenom);
        setSalaireBrut(emp.salaireBrut);
        setTauxCharge(emp.tauxCharge);
        setDateNaissance(emp.dateNaissance);
    }

    public String getMatricule() {
        return this.matricule;
    }

    public void setMatricule(String matricule) throws Exception {
        if (!checkMatricule(matricule)) {
            throw new Exception("T'es sur?");
        }
        this.matricule = matricule;

    }

    public String getNom() {
        return this.nom;
    }

    public void setNom(String nom) throws Exception {
        if (!checkNom(nom)) {
            throw new Exception("Tu connais pas ton nom ?");   
        } 
        this.nom = nom; 
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) throws Exception {
        if (!checkPrenom(prenom)) {
            throw new Exception("Tu connais pas ton prénom ?");
        }
        this.prenom = prenom;
    }

    public int getSalaireBrut() {
        return salaireBrut;
    }

    public void setSalaireBrut(int salaireBrut) {
        this.salaireBrut = salaireBrut;
    }

    public double getTauxCharge() {
        return tauxCharge;
    }

    public void setTauxCharge(double tauxCharge) throws Exception {
        if (!checkTaux(tauxCharge)) {
            throw new Exception("T'abuse pas sur les charges là?");
        }      
        this.tauxCharge = tauxCharge;
    }

    public Date getDateNaissance() {
        return this.dateNaissance;
    }

    public void setDateNaissance(Date dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    public String toString() {
        return "Nom: " + this.nom + " Prenom: " + this.prenom + " Matricule: " + this.matricule + " Salaire brut: "
                + this.salaireBrut + " Taux charge: " + this.tauxCharge + " Date de naissance: " + this.dateNaissance;
    }

    public double calculerSalaire() {
        salaireNet = this.salaireBrut - (this.salaireBrut * this.tauxCharge);
        return salaireNet;
    }

    public boolean checkMatricule(String matricule) {

        pattern = Pattern.compile("\\d{2}[a-zA-Z]{3}\\d{2}");
        boolean matcher = pattern.matcher(matricule).matches();
        return matcher;
    }

    public boolean checkNom(String nom) {
        Pattern pattern = Pattern.compile("^[a-zA-Z]*$");
        boolean matcher = pattern.matcher(nom).matches();
        return matcher;
    }

    public boolean checkPrenom(String prenom) {
        pattern = Pattern.compile("^[a-zA-Z]*$");
        boolean matcher = pattern.matcher(prenom).matches();
        return matcher;
    }

    public boolean checkTaux(double tauxCharge) {
        boolean bonTaux = false;
        if (tauxCharge > 0 && tauxCharge <= 0.6) {
            bonTaux = true;

        }
        return bonTaux;
    }


    /**
     * @return
     */

}
