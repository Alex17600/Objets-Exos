package fr.afpa.testmaven;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.beans.ExceptionListener;
import java.beans.XMLDecoder;
import java.beans.XMLEncoder;


public class XMLTools {
    public static void serializeToXML(Employes newEmployes) throws IOException {
        FileOutputStream fos = new FileOutputStream(newEmployes.getNom() + "donnees.xml");
        XMLEncoder encoder = new XMLEncoder(fos);
        encoder.setExceptionListener(new ExceptionListener() {
            public void exceptionThrown(Exception e) {
                System.out.println("Exception! :" + e.toString());
            }
        });
        encoder.writeObject(newEmployes);
        encoder.close();
        fos.close();
    }

    public static Employes deserializeFromXML(String nomDeFichier) throws IOException {
        FileInputStream fis = new FileInputStream(nomDeFichier);
        XMLDecoder decoder = new XMLDecoder(fis);
        Employes decodedSettings = (Employes) decoder.readObject();
        decoder.close();
        fis.close();
        System.out.println(decodedSettings);
        return decodedSettings;

      }
}
