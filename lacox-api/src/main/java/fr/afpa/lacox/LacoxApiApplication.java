package fr.afpa.lacox;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LacoxApiApplication {

		public static void main(String[] args) {
		SpringApplication.run(LacoxApiApplication.class, args);
	}
}
