package fr.afpa.lacox.models;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonView;

import fr.afpa.lacox.models.views.View;
import jakarta.persistence.Column;
import jakarta.persistence.Table;

@Entity

@Table(name = "evenement")
public class Evenement {
    @JsonView(value= {View.Media.class, View.Evenement.class})
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_evenement")
    private Integer id;

    private Date date_event;

    @Column(name = "nbr_place")
    private int nbrPlace;
    
    @Column(name = "nom_event")
    private String nomEvent;

    @JsonBackReference
    @ManyToMany
    @JoinTable(name = "reservation", joinColumns = @JoinColumn(name = "id_evenement"), inverseJoinColumns = @JoinColumn(name = "id_utilisateur"))
    private List<Utilisateur> utilisateurs;

    @OneToMany(mappedBy = "evenement")
    private List<Media> medias;

    @ManyToOne
    @JoinColumn(name = "id_type")
    private Type type;

    private String presentation;

    public Evenement() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNomEvent() {
        return nomEvent;
    }

    public void setNomEvent(String nomEvent) {
        this.nomEvent = nomEvent;
    }

    public Date getDate_event() {
        return date_event;
    }

    public void setDate_event(Date date_event) {
        this.date_event = date_event;
    }

    public int getNbrPlace() {
        return nbrPlace;
    }

    public void setNbrPlace(int nbrPlace) {
        this.nbrPlace = nbrPlace;
    }

    public List<Utilisateur> getUtilisateurs() {
        return utilisateurs;
    }

    public void setUtilisateurs(List<Utilisateur> utilisateurs) {
        this.utilisateurs = utilisateurs;
    }

    public List<Media> getMedias() {
        return medias;
    }

    public void setMedias(List<Media> medias) {
        this.medias = medias;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public String getPresentation() {
        return presentation;
    }

    public void setPresentation(String presentation) {
        this.presentation = presentation;
    }
}
