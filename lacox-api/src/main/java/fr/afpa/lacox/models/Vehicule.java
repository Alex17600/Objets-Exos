package fr.afpa.lacox.models;

import java.util.List;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;


@Entity
@Table(name = "vehicule")
public class Vehicule {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_vehicule")
    private Integer id;

    @Column(name = "type")
    private String type;

    @Column(name = "nbr_place")
    private Integer nbrPlace;

    @OneToMany(mappedBy = "vehicule")
    private List<Navette> navettes;

    public Vehicule() {
    }

    public Vehicule(Integer id, String type, Integer nbrPlace) {
        this.id = id;
        this.type = type;
        this.nbrPlace = nbrPlace;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getNbrPlace() {
        return nbrPlace;
    }

    public void setNbrPlace(Integer nbrPlace) {
        this.nbrPlace = nbrPlace;
    }

    public List<Navette> getNavettes() {
        return navettes;
    }

    public void setNavette(List<Navette> navettes) {
        this.navettes = navettes;
    }

}
