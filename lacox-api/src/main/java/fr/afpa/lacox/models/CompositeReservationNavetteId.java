package fr.afpa.lacox.models;

import jakarta.persistence.Embeddable;

@Embeddable
public class CompositeReservationNavetteId {
    private int idNavette;
    private int idUtilisateur;

    public CompositeReservationNavetteId() {
    }

    public CompositeReservationNavetteId(int idNavette, int idUtilisateur) {
        this.idNavette = idNavette;
        this.idUtilisateur = idUtilisateur;
    }

    public int getIdNavette() {
        return idNavette;
    }

    public void setIdNavette(int idNavette) {
        this.idNavette = idNavette;
    }

    public int getIdUtilisateur() {
        return idUtilisateur;
    }

    public void setIdUtilisateur(int idUtilisateur) {
        this.idUtilisateur = idUtilisateur;
    }
}
