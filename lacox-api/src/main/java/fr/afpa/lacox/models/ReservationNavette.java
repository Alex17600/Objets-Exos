package fr.afpa.lacox.models;

import com.fasterxml.jackson.annotation.JsonView;

import fr.afpa.lacox.models.views.View;
import jakarta.persistence.Column;
import jakarta.persistence.EmbeddedId;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.MapsId;
import jakarta.persistence.Table;

@Entity
@Table(name = "resa_navette")
public class ReservationNavette {

    @EmbeddedId
    private CompositeReservationNavetteId compositeId;

    @ManyToOne
    @JsonView(View.Utilisateur.class)
    @JoinColumn(name = "id_navette")
    @MapsId("idNavette")
    private Navette navette;

    @ManyToOne
    @JoinColumn(name = "id_utilisateur")
    @MapsId("idUtilisateur")
    private Utilisateur utilisateur;

    @JsonView(View.UtilisateurDetailled.class)
    @Column(name = "nbr_resa")
    private Integer nombreReservation;

    public ReservationNavette() {
    }

    public ReservationNavette(Navette navette, Utilisateur utilisateur, Integer nombreReservation) {
        this.navette = navette;
        this.utilisateur = utilisateur;
        this.nombreReservation = nombreReservation;
    }

    public Navette getNavette() {
        return navette;
    }

    public void setNavette(Navette navette) {
        this.navette = navette;
    }

    public Utilisateur getUtilisateur() {
        return utilisateur;
    }

    public void setUtilisateur(Utilisateur utilisateur) {
        this.utilisateur = utilisateur;
    }

    public Integer getNombreReservation() {
        return nombreReservation;
    }

    public void setNombreReservation(Integer nombreReservation) {
        this.nombreReservation = nombreReservation;
    }
}
