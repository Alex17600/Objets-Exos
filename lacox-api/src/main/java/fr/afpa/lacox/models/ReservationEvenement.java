package fr.afpa.lacox.models;

import java.sql.Date;
import java.sql.Timestamp;

import jakarta.persistence.EmbeddedId;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.MapsId;
import jakarta.persistence.Table;

@Entity
@Table(name = "reservation")
public class ReservationEvenement {

    @EmbeddedId
    private CompositeReservationEvenement compositeResaEventId;
    
    @ManyToOne
    @JoinColumn(name = "id_evenement")
    @MapsId("idEvenement")
    private Evenement evenement;

    @ManyToOne
    @JoinColumn(name = "id_utilisateur")
    @MapsId("idUtilisateur")
    private Utilisateur utilisateur;

    @JoinColumn(name = "date_resa")
    private Timestamp dateResa;

    public ReservationEvenement() {
    }

    public ReservationEvenement(CompositeReservationEvenement compositeResaEventId, Evenement evenement,
            Utilisateur utilisateur, Timestamp dateResa) {
        this.compositeResaEventId = compositeResaEventId;
        this.evenement = evenement;
        this.utilisateur = utilisateur;
        this.dateResa = dateResa;
    }

    public CompositeReservationEvenement getCompositeResaEventId() {
        return compositeResaEventId;
    }

    public void setCompositeResaEventId(CompositeReservationEvenement compositeResaEventId) {
        this.compositeResaEventId = compositeResaEventId;
    }
    public Evenement getEvenement() {
        return evenement;
    }

    public void setEvenement(Evenement evenement) {
        this.evenement = evenement;
    }

    public Utilisateur getUtilisateur() {
        return utilisateur;
    }

    public void setUtilisateur(Utilisateur utilisateur) {
        this.utilisateur = utilisateur;
    }

    public Timestamp getDateResa() {
        return dateResa;
    }

    public void setDateResa(Timestamp timestampResa) {
        this.dateResa = timestampResa;
    }

}
