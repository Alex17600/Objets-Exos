package fr.afpa.lacox.models.views;

public class View {
    public static class Utilisateur {}

    public static class UtilisateurDetailled extends Utilisateur {}

    public static class Media {}

    public static class Evenement {}
}
