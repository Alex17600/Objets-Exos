package fr.afpa.lacox.models;

import java.sql.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonView;

import fr.afpa.lacox.models.views.View;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

@Entity
@Table(name = "navette")
public class Navette {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_navette")
    private Integer id;

    @JsonView(View.Utilisateur.class)
    @Column(name = "date_heure")
    private Date dateheure;

    @JsonView(View.Utilisateur.class)
    @ManyToOne
    @JoinColumn(name = "id_vehicule")
    private Vehicule vehicule;

    @OneToMany(mappedBy = "navette")
    private List<ReservationNavette> navettes;

    public Navette() {
    }

    public Navette(Integer id, Date dateheure, Vehicule vehicule) {
        this.id = id;
        this.dateheure = dateheure;
        this.vehicule = vehicule;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDateheure() {
        return dateheure;
    }

    public void setDateheure(Date dateheure) {
        this.dateheure = dateheure;
    }

    public Vehicule getVehicule() {
        return vehicule;
    }

    public void setVehicule(Vehicule vehicule) {
        this.vehicule = vehicule;
    }

    public List<ReservationNavette> getTrajets() {
        return navettes;
    }

    public void setTrajets(List<ReservationNavette> navettes) {
        this.navettes = navettes;
    }

}
