package fr.afpa.lacox.models;

import jakarta.persistence.Embeddable;

@Embeddable
public class CompositeReservationEvenement {
    private int idEvenement;
    private int idUtilisateur;
    
    public CompositeReservationEvenement() {
    }
  
    public CompositeReservationEvenement(int idEvenement, int idUtilisateur) {
        this.idEvenement = idEvenement;
        this.idUtilisateur = idUtilisateur;
    }

    public int getIdEvenement() {
        return idEvenement;
    }

    public void setIdEvenement(int idEvenement) {
        this.idEvenement = idEvenement;
    }

    public int getIdUtilisateur() {
        return idUtilisateur;
    }

    public void setIdUtilisateur(int idUtilisateur) {
        this.idUtilisateur = idUtilisateur;
    }    
}
