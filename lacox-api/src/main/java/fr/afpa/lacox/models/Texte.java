package fr.afpa.lacox.models;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name = "texte")
public class Texte {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_texte")
    private Integer id;

    @Column(name = "nom")
    private String nom;

    @Column(name = "contenu")
    private String contenu;

    @Column(name = "page")
    private String page;

    public Texte() {
    }

    public Texte(Integer id, String nom, String contenu, String page) {
        this.id = id;
        this.nom = nom;
        this.contenu = contenu;
        this.page = page;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getContenu() {
        return contenu;
    }

    public void setContenu(String contenu) {
        this.contenu = contenu;
    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }
}
