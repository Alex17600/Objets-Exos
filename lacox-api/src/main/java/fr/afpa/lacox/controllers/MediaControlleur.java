package fr.afpa.lacox.controllers;

import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigInteger;

import java.nio.file.Files;
import java.nio.file.Path;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import fr.afpa.lacox.models.Media;

import fr.afpa.lacox.repositories.MediaRepository;
import fr.afpa.lacox.services.FileStorageService;

@CrossOrigin
@RestController
@RequestMapping(value = "/medias")
public class MediaControlleur {

    Logger logger = LoggerFactory.getLogger(FileStorageService.class);

    @Autowired
    private MediaRepository mediaRepository;

    @Autowired
    private FileStorageService fileStorageService;

    @GetMapping(value = "")
    @ResponseStatus(HttpStatus.OK)
    public Iterable<Media> get() {
        return mediaRepository.findAll();
    }

    @GetMapping(value = "/{id}") /// ---> GET localhost:8000/?/?
    @ResponseStatus(HttpStatus.OK)
    public Optional<Media> get(@PathVariable(required = true) Integer id) {
        return mediaRepository.findById(id);
    }

    @CrossOrigin
    @GetMapping(value = "/image", produces = { MediaType.IMAGE_JPEG_VALUE, MediaType.IMAGE_PNG_VALUE })
    public @ResponseBody byte[] getImage() {

        Path rootLocation = this.fileStorageService.getRootLocation();
        Optional<Media> media = mediaRepository.findTopByOrderByIdDesc();

        if (media.isPresent()) {

            String imageName = media.get().getContenu();
            Path imagePath = rootLocation.resolve(imageName);
            try {
                return Files.readAllBytes(imagePath);
            } catch (IOException e) {
                logger.error(e.getMessage());
            }

        }

        throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Impossible de trouver l'image demandée.");
    }

    @PostMapping(value = "/download", consumes = { MediaType.MULTIPART_FORM_DATA_VALUE })
    @ResponseStatus(HttpStatus.OK)
    public Media post(@RequestPart Media media, @RequestPart MultipartFile image) {

        try {
            // on récupère l'image provenant de la classe Media (traitement automatique à
            // partir de la requête)
            // MultipartFile imageFile = media.getContenu();
            if (!image.isEmpty()) {
                logger.info("Sauvegarde du fichier image");

                // calcul du hash du fichier pour obtenir un nom unique
                String storageHash = getStorageHash(image).get();
                Path rootLocation = this.fileStorageService.getRootLocation();
                // récupération de l'extension
                String fileExtension = mimeTypeToExtension(image.getContentType());
                // ajout de l'extension au nom du fichier
                storageHash = storageHash + fileExtension;
                // on retrouve le chemin de stockage de l'image
                Path saveLocation = rootLocation.resolve(storageHash);

                // suppression du fichier au besoin
                Files.deleteIfExists(saveLocation);

                // tentative de sauvegarde
                Files.copy(image.getInputStream(), saveLocation);

                // à ce niveau il n'y a pas eu d'exception
                // on ajoute le nom utilisé pour stocké l'image
                media.setContenu(storageHash);
            }

            // on modifie la BDD même sans image
            return mediaRepository.save(media);

        } catch (IOException e) {
            logger.error(e.getMessage());
        }

        // Si on arrive là alors erreur
        throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Impossible de sauvegarder la ressource.");
    }

    @PostMapping(value = "/cover/download", consumes = { MediaType.MULTIPART_FORM_DATA_VALUE })
    @ResponseStatus(HttpStatus.OK)
    public Media postCover(@RequestPart Media media, @RequestPart MultipartFile image) {

        try {
            // on récupère l'image provenant de la classe Media (traitement automatique à
            // partir de la requête)
            // MultipartFile imageFile = media.getContenu();
            if (!image.isEmpty()) {
                logger.info("Sauvegarde du fichier image");

                // calcul du hash du fichier pour obtenir un nom unique
                String storageHash = getStorageHash(image).get();
                Path rootLocation = this.fileStorageService.getRootLocation();
                // récupération de l'extension
                String fileExtension = mimeTypeToExtension(image.getContentType());
                // ajout de l'extension au nom du fichier
                storageHash = storageHash + fileExtension;
                // on retrouve le chemin de stockage de l'image
                Path saveLocation = rootLocation.resolve(storageHash);

                // suppression du fichier au besoin
                Files.deleteIfExists(saveLocation);

                // tentative de sauvegarde
                Files.copy(image.getInputStream(), saveLocation);
                media.setCover(true);
                // à ce niveau il n'y a pas eu d'exception
                // on ajoute le nom utilisé pour stocké l'image
                media.setContenu(storageHash);
            }

            // on modifie la BDD même sans image
            return mediaRepository.save(media);

        } catch (IOException e) {
            logger.error(e.getMessage());
        }

        // Si on arrive là alors erreur
        throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Impossible de sauvegarder la ressource.");
    }

    /**
     * Retourne l'extension d'un fichier en fonction d'un type MIME
     * pour plus d'informations sur les types MIME :
     * https://developer.mozilla.org/fr/docs/Web/HTTP/Basics_of_HTTP/MIME_types/Common_types
     */
    private String mimeTypeToExtension(String mimeType) {
        return switch (mimeType) {
            case "image/jpeg" -> ".jpeg";
            case "image/png" -> ".png";
            case "image/svg" -> ".svg";
            default -> "";
        };
    }

    /**
     * Permet de retrouver un hash qui pourra être utilisé comme nom de fichier
     * uniquement pour le stockage.
     *
     * Le hash sera calculé à partir du nom du fichier, de son type MIME
     * (https://developer.mozilla.org/fr/docs/Web/HTTP/Basics_of_HTTP/MIME_types/Common_types)
     * et de la date d'upload.
     *
     * @return Le hash encodé en base64
     */
    private Optional<String> getStorageHash(MultipartFile file) {
        String hashString = null;

        if (!file.isEmpty()) {
            try {
                MessageDigest messageDigest = MessageDigest.getInstance("MD5");

                // La méthode digest de la classe "MessageDigest" prend en paramètre un byte[]
                // il faut donc transformer les différents objets utilisés pour le hachage en
                // tableau d'octets
                // Nous utiliserons la classe "ByteArrayOutputStream" pour se faire
                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                outputStream.write(file.getOriginalFilename().getBytes());
                outputStream.write(file.getContentType().getBytes());
                LocalDate date = LocalDate.now();
                outputStream.write(date.toString().getBytes());

                // calcul du hash, on obtient un tableau d'octets
                byte[] hashBytes = messageDigest.digest(outputStream.toByteArray());

                // on retrouve une chaîne de caractères à partir d'un tableau d'octets
                hashString = String.format("%032x", new BigInteger(1, hashBytes));
            } catch (NoSuchAlgorithmException | IOException e) {
                logger.error(e.getMessage());
            }
        }

        return Optional.ofNullable(hashString);
    }

    @DeleteMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void delete(@PathVariable(required = true) Integer id) {
        mediaRepository.deleteById(id);
    }

    @PutMapping(value = "/{id}", consumes = { MediaType.APPLICATION_JSON_VALUE })
    @ResponseStatus(HttpStatus.CREATED)
    public Media put(@RequestBody Media media, @PathVariable("id") Integer id) {

        Media existingMedia = mediaRepository.findById(id).orElse(null);
        if (existingMedia == null) {

            media.setId(id);
            return mediaRepository.save(media);
        } else {

            existingMedia.setNom(media.getNom());
            existingMedia.setContenu(media.getContenu());

            return mediaRepository.save(existingMedia);
        }
    }

    @GetMapping(value= "/types/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Iterable<Media> getMediaByType(@PathVariable(required = true) Integer id) {
        return mediaRepository.findByEvenementAndIdType(id);
    }

    @CrossOrigin
    @GetMapping(value = "/{id}/image", produces = { MediaType.IMAGE_JPEG_VALUE, MediaType.IMAGE_PNG_VALUE })
    public @ResponseBody byte[] getImageByType(@PathVariable(required = true) Integer id) {

        Path rootLocation = this.fileStorageService.getRootLocation();
        Optional<Media> media = mediaRepository.findById(id);

        if (media.isPresent()) {

            String imageName = media.get().getContenu();
            Path imagePath = rootLocation.resolve(imageName);
            try {
                return Files.readAllBytes(imagePath);
            } catch (IOException e) {
                logger.error(e.getMessage());
            }
        }

        throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Impossible de trouver l'image demandée.");
    }

    
}
