package fr.afpa.lacox.controllers;


import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonView;

import fr.afpa.lacox.models.Evenement;
import fr.afpa.lacox.models.views.View;
import fr.afpa.lacox.repositories.EvenementRepository;

@CrossOrigin
@RestController
@RequestMapping(value = "/evenements")
public class EvenementControlleur {

    @Autowired
    private EvenementRepository evenementRepository;

    @JsonView(View.Evenement.class)
    @GetMapping(value="")
    @ResponseStatus(HttpStatus.OK)
    public Iterable<Evenement> get() {
        return evenementRepository.findAll();
    }
    
    @GetMapping(value = "/{id}") /// --->   GET localhost:8000/events/?
    @ResponseStatus(HttpStatus.OK)
    public Optional<Evenement> get(@PathVariable(required = true) Integer id) {
        return evenementRepository.findById(id);
    }

    @DeleteMapping(value="/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void delete(@PathVariable(required = true) Integer id) {
        evenementRepository.deleteById(id);
    }

    @PostMapping(value="", consumes = { MediaType.APPLICATION_JSON_VALUE })
    @ResponseStatus(HttpStatus.CREATED)
    public Evenement post(@RequestBody Evenement evenement) {
        return evenementRepository.save(evenement);
    }
    
    @GetMapping(value = "/{id}/test")
    public ResponseEntity<?> getOne(@PathVariable Integer id){
        try {
            return ResponseEntity.ok().body(evenementRepository.findById(id).orElseThrow());
        }catch (Exception exception){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(exception.fillInStackTrace());
        }
    }

    @GetMapping(value="/type/soiree")
    @ResponseStatus(HttpStatus.OK)
    public Iterable<Evenement> getSoiree() {
        return evenementRepository.findBySoiree();
    }
}
