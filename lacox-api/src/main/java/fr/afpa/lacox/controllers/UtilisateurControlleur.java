package fr.afpa.lacox.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import com.fasterxml.jackson.annotation.JsonView;
import fr.afpa.lacox.models.Role;
import fr.afpa.lacox.models.Utilisateur;
import fr.afpa.lacox.models.views.View;
import fr.afpa.lacox.repositories.RoleRepository;
import fr.afpa.lacox.repositories.UtilisateurRepository;

@RestController
@RequestMapping(value = "/utilisateurs")
public class UtilisateurControlleur {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UtilisateurRepository utilisateurRepository;

    @Autowired
    private RoleRepository roleRepository;

    /**
     * @return Iterable<Utilisateur>
     */
    @JsonView(View.Utilisateur.class)
    @CrossOrigin
    @GetMapping(value = "")
    @ResponseStatus(HttpStatus.OK)
    public Iterable<Utilisateur> get() {
        return utilisateurRepository.findAll();
    }

    @CrossOrigin
    @GetMapping(value = "/{id}") /// ---> GET localhost:8000/events/?
    @ResponseStatus(HttpStatus.OK)
    public Optional<Utilisateur> get(@PathVariable(required = true) Integer id) {
        return utilisateurRepository.findById(id);
    }

    @CrossOrigin
    @DeleteMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void delete(@PathVariable(required = true) Integer id) {
        utilisateurRepository.deleteById(id);
    }

    @CrossOrigin
    @PostMapping(value = "", consumes = { MediaType.APPLICATION_JSON_VALUE })
    @ResponseStatus(HttpStatus.CREATED)
    public Utilisateur post(@RequestBody Utilisateur utilisateur) {

        String password = utilisateur.getMdp(); 
        String encryptedPassword = passwordEncoder.encode(password); 
        utilisateur.setMdp(encryptedPassword); 

        List<Role> list = new ArrayList<Role>();
        Role role = roleRepository.findByName("ROLE_USER");
        list.add(role);
        utilisateur.setRoles(list);
        utilisateur.setRole(role.getNom());

        return utilisateurRepository.save(utilisateur);
    }

    @CrossOrigin
    @GetMapping(value = "/{id}/test")
    public ResponseEntity<?> getOne(@PathVariable Integer id) {
        try {
            return ResponseEntity.ok().body(utilisateurRepository.findById(id).orElseThrow());
        } catch (Exception exception) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(exception.fillInStackTrace());
        }
    }
}
