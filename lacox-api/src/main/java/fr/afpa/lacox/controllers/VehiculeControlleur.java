package fr.afpa.lacox.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import fr.afpa.lacox.models.Vehicule;
import fr.afpa.lacox.repositories.VehiculeRepository;

@CrossOrigin
@RestController
@RequestMapping(value = "/vehicules")
public class VehiculeControlleur {
    
    @Autowired
    private VehiculeRepository vehiculeRepository;

    @GetMapping(value = "")
    @ResponseStatus(HttpStatus.OK)
    public Iterable<Vehicule> get() {
        return vehiculeRepository.findAll();
    }

    @GetMapping(value = "/{id}") /// --->   GET localhost:8000/?/?
    @ResponseStatus(HttpStatus.OK)
    public Optional<Vehicule> get(@PathVariable(required = true) Integer id) {
        return vehiculeRepository.findById(id);
    }

    @DeleteMapping(value="/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void delete(@PathVariable(required = true) Integer id) {
        vehiculeRepository.deleteById(id);
    }

    @PostMapping(value="", consumes = { MediaType.APPLICATION_JSON_VALUE })
    @ResponseStatus(HttpStatus.CREATED)
    public Vehicule post(@RequestBody Vehicule vehicule) {
        return vehiculeRepository.save(vehicule);
    }

    @PutMapping(value = "/{id}", consumes = { MediaType.APPLICATION_JSON_VALUE})
    @ResponseStatus(HttpStatus.CREATED)
    public Vehicule put(@RequestBody Vehicule vehicule, @PathVariable("id") Integer id) {
        // Vérifiez si le vehicule avec l'ID donné existe dans le référentiel
        Vehicule existingVehicule = vehiculeRepository.findById(id).orElse(null);
        if (existingVehicule == null) {
            // Le vehicule avec l'ID donné n'existe pas, vous pouvez retourner une erreur ou créer un nouveau vehicule
            // dans ce cas, nous créons un nouveau vehicule avec l'ID donné
            vehicule.setId(id);
            return vehiculeRepository.save(vehicule);
        } else {
            // Le vehicule avec l'ID donné existe, vous pouvez mettre à jour ses propriétés ici
            existingVehicule.setType(vehicule.getType());
            existingVehicule.setNbrPlace(vehicule.getNbrPlace());
            // ... mettez à jour d'autres propriétés
            return vehiculeRepository.save(existingVehicule);
        }
    }
}
