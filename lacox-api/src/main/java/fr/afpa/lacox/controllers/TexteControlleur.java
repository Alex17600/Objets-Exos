package fr.afpa.lacox.controllers;

import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import fr.afpa.lacox.models.Texte;
import fr.afpa.lacox.repositories.TexteRepository;

@RestController
@RequestMapping(value = "/textes")
public class TexteControlleur {

    @Autowired
    private TexteRepository texteRepository;

    @CrossOrigin
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public Iterable<Texte> get() {
        return texteRepository.findAll();
    }

    @CrossOrigin
    @GetMapping(value = "/{id}") /// --->   GET localhost:8000/?/?
    @ResponseStatus(HttpStatus.OK)
    public Optional<Texte> get(@PathVariable(required = true) Integer id) {
        return texteRepository.findById(id);
    }

    @CrossOrigin
    @DeleteMapping(value="/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void delete(@PathVariable(required = true) Integer id) {
        texteRepository.deleteById(id);
    }


    @CrossOrigin
    @PostMapping(value="", consumes = { MediaType.APPLICATION_JSON_VALUE })
    @ResponseStatus(HttpStatus.CREATED)
    public Texte post(@RequestBody Texte texte) {
        return texteRepository.save(texte);
    }

    @CrossOrigin
    @PutMapping(value = "/{id}", consumes = { MediaType.APPLICATION_JSON_VALUE})
    @ResponseStatus(HttpStatus.CREATED)
    public Texte put(@RequestBody Texte texte, @PathVariable("id") Integer id) {
        // Vérifiez si le texte avec l'ID donné existe dans le référentiel
        Texte existingTexte = texteRepository.findById(id).orElse(null);
        if (existingTexte == null) {
            // Le texte avec l'ID donné n'existe pas, vous pouvez retourner une erreur ou créer un nouveau texte
            // dans ce cas, nous créons un nouveau texte avec l'ID donné
            texte.setId(id);
            return texteRepository.save(texte);
        } else {
            // Le texte avec l'ID donné existe, vous pouvez mettre à jour ses propriétés ici
            existingTexte.setContenu(texte.getContenu());
            existingTexte.setNom(texte.getNom());
            existingTexte.setPage(texte.getPage());
            // ... mettez à jour d'autres propriétés
            return texteRepository.save(existingTexte);
        }
    }
    

    @CrossOrigin
    @GetMapping(value = "/{id}/test")
    public ResponseEntity<?> getOne(@PathVariable Integer id){
        try {
            return ResponseEntity.ok().body(texteRepository.findById(id).orElseThrow());
        }catch (Exception exception){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(exception.fillInStackTrace());
        }
    }
}
