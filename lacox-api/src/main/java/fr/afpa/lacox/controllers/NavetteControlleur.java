package fr.afpa.lacox.controllers;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import fr.afpa.lacox.models.Navette;
import fr.afpa.lacox.repositories.NavetteRepository;

@CrossOrigin
@RestController
@RequestMapping(value = "/navettes")
public class NavetteControlleur {

    @Autowired
    private NavetteRepository navetteRepository;

    @GetMapping(value = "")
    @ResponseStatus(HttpStatus.OK)
    public Iterable<Navette> get() {
        return navetteRepository.findAll();
    }

    @GetMapping(value = "/{id}") /// --->   GET localhost:8000/?/?
    @ResponseStatus(HttpStatus.OK)
    public Optional<Navette> get(@PathVariable(required = true) Integer id) {
        return navetteRepository.findById(id);
    }

    @DeleteMapping(value="/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void delete(@PathVariable(required = true) Integer id) {
        navetteRepository.deleteById(id);
    }

    @PostMapping(value="", consumes = { MediaType.APPLICATION_JSON_VALUE })
    @ResponseStatus(HttpStatus.CREATED)
    public Navette post(@RequestBody Navette navette) {
        return navetteRepository.save(navette);
    }

    @PutMapping(value = "/{id}", consumes = { MediaType.APPLICATION_JSON_VALUE})
    @ResponseStatus(HttpStatus.CREATED)
    public Navette put(@RequestBody Navette navette, @PathVariable("id") Integer id) {
        
        Navette existingNavette = navetteRepository.findById(id).orElse(null);
        if (existingNavette == null) {

            navette.setId(id);
            return navetteRepository.save(navette);
        } else {
           
            existingNavette.setDateheure(navette.getDateheure());
            existingNavette.setVehicule(navette.getVehicule());

            return navetteRepository.save(existingNavette);
        }
    }
}
