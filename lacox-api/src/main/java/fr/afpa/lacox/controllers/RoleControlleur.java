package fr.afpa.lacox.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.HttpStatus;

import fr.afpa.lacox.models.Role;
import fr.afpa.lacox.repositories.RoleRepository;

@CrossOrigin
@RestController
@RequestMapping(value = "/roles")
public class RoleControlleur {

    @Autowired
    private RoleRepository roleRepository;

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public Iterable<Role> get() {
        return roleRepository.findAll();
    }
}
