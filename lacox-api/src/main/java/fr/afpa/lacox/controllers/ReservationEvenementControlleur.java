package fr.afpa.lacox.controllers;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Optional;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import fr.afpa.lacox.models.CompositeReservationEvenement;
import fr.afpa.lacox.models.Evenement;
import fr.afpa.lacox.models.ReservationEvenement;
import fr.afpa.lacox.models.Utilisateur;
import fr.afpa.lacox.repositories.EvenementRepository;
import fr.afpa.lacox.repositories.ReservationEvenementRepository;
import fr.afpa.lacox.repositories.UtilisateurRepository;
import fr.afpa.lacox.security.JwtUtil;
import jakarta.servlet.http.HttpServletRequest;

@CrossOrigin
@RestController
@RequestMapping(value = "/resevents")
public class ReservationEvenementControlleur {

    @Autowired
    private HttpServletRequest request;

    @Autowired
    private ReservationEvenementRepository reservationEvenementRepository;

    @Autowired
    private UtilisateurRepository utilisateurRepository;

    @Autowired
    private EvenementRepository evenementRepository;

    @GetMapping("")
    @ResponseStatus(HttpStatus.OK)
    public Iterable<ReservationEvenement> get() {
        return reservationEvenementRepository.findAll();
    }

    @GetMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Optional<ReservationEvenement> get(@PathVariable(required = true) Integer id) {
        return reservationEvenementRepository.findById(id);
    }

    @DeleteMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void delete(@PathVariable(required = true) Integer id) {
        reservationEvenementRepository.deleteById(id);
    };

    @PostMapping(value = "/{id}/evenement", consumes = { MediaType.APPLICATION_JSON_VALUE })
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<?> post(@PathVariable(required = true) Integer id) {
        ReservationEvenement reservationEvenement = new ReservationEvenement();
        try {
            String email = JwtUtil.parseTokentoEmail(request.getHeader(HttpHeaders.AUTHORIZATION));

            // recuperation de l'id utilisateur
            Utilisateur utilisateur = utilisateurRepository.findByEmail(email);
            Optional<Evenement> evenement = evenementRepository.findById(id);
            if (evenement.isPresent()) {
                // date et heure de la resa
                LocalDateTime currentDateTime = LocalDateTime.now();
                Timestamp timestampResa = Timestamp.valueOf(currentDateTime);
                reservationEvenement.setDateResa(timestampResa);

                reservationEvenement.setUtilisateur(utilisateur);
                reservationEvenement.setEvenement(evenement.get());

                // Création de la clé composite
                CompositeReservationEvenement compositeId = new CompositeReservationEvenement(id, utilisateur.getId());
                reservationEvenement.setCompositeResaEventId(compositeId);
                ReservationEvenement savedReservation = reservationEvenementRepository.save(reservationEvenement);

                return ResponseEntity.ok(savedReservation);
            }

        } catch (Exception e) {
            return ResponseEntity.badRequest().build();
        }

        return null;
    }
}
