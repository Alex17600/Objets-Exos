package fr.afpa.lacox.controllers;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import fr.afpa.lacox.models.Type;
import fr.afpa.lacox.repositories.TypeRepository;

@CrossOrigin
@RestController
@RequestMapping(value = "types")
public class TypeControlleur {
    
    @Autowired
    private TypeRepository typeRepository;

    @GetMapping(value = "")
    @ResponseStatus(HttpStatus.OK)
    public Iterable<Type> get() {
        return typeRepository.findAll();
    }

    @GetMapping(value = "/{id}") /// --->   GET localhost:8000/?/?
    @ResponseStatus(HttpStatus.OK)
    public Optional<Type> get(@PathVariable(required = true) Integer id) {
        return typeRepository.findById(id);
    }

    @DeleteMapping(value="/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void delete(@PathVariable(required = true) Integer id) {
        typeRepository.deleteById(id);
    }

    @PostMapping(value="", consumes = { MediaType.APPLICATION_JSON_VALUE })
    @ResponseStatus(HttpStatus.CREATED)
    public Type post(@RequestBody Type type) {
        return typeRepository.save(type);
    }

    @PutMapping(value = "/{id}", consumes = { MediaType.APPLICATION_JSON_VALUE})
    @ResponseStatus(HttpStatus.CREATED)
    public Type put(@RequestBody Type type, @PathVariable("id") Integer id) {
        // Vérifiez si le texte avec l'ID donné existe dans le référentiel
        Type existingType = typeRepository.findById(id).orElse(null);
        if (existingType == null) {

            type.setId(id);
            return typeRepository.save(type);
        } else {
           
            existingType.setNom(type.getNom());

            return typeRepository.save(existingType);
        }
    }
}
