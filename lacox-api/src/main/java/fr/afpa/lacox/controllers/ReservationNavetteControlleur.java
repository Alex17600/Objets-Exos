package fr.afpa.lacox.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import fr.afpa.lacox.models.ReservationNavette;
import fr.afpa.lacox.repositories.ReservationNavetteRepository;

@CrossOrigin
@RestController
@RequestMapping(value = "/trajets")
public class ReservationNavetteControlleur {
    
    @Autowired
    private ReservationNavetteRepository trajetRepository;

    @GetMapping("")
    @ResponseStatus(HttpStatus.OK)
    public Iterable<ReservationNavette> get() {
        return trajetRepository.findAll();
    }

    @GetMapping(value = "/{id}") /// --->   GET localhost:8000/events/?
    @ResponseStatus(HttpStatus.OK)
    public Optional<ReservationNavette> get(@PathVariable(required = true) Integer id) {
        return trajetRepository.findById(id);
    }

    @DeleteMapping(value="/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void delete(@PathVariable(required = true) Integer id) {
        trajetRepository.deleteById(id);
    }

    @PostMapping(value="", consumes = { MediaType.APPLICATION_JSON_VALUE })
    @ResponseStatus(HttpStatus.CREATED)
    public ReservationNavette post(@RequestBody ReservationNavette trajet) {
        return trajetRepository.save(trajet);
    }
}
