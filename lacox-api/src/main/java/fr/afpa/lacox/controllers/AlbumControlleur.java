package fr.afpa.lacox.controllers;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

import fr.afpa.lacox.models.Album;
import fr.afpa.lacox.models.Media;
import fr.afpa.lacox.repositories.AlbumRepository;
import fr.afpa.lacox.repositories.MediaRepository;
import fr.afpa.lacox.services.FileStorageService;

@CrossOrigin
@RestController
@RequestMapping(value = "/albums")
public class AlbumControlleur {

    Logger logger = LoggerFactory.getLogger(FileStorageService.class);

    @Autowired
    private AlbumRepository albumRepository;

    @Autowired
    private MediaRepository mediaRepository;

    @Autowired
    private FileStorageService fileStorageService;

    @GetMapping(value = "")
    @ResponseStatus(HttpStatus.OK)
    public Iterable<Album> get() {
        return albumRepository.findAll();
    }

    @GetMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Optional<Album> get(@PathVariable(required = true) Integer id) {
        return albumRepository.findById(id);
    }

    @PostMapping(value = "/create", consumes = { MediaType.APPLICATION_JSON_VALUE })
    @ResponseStatus(HttpStatus.CREATED)
    public Album post(@RequestBody Album album) {
        Album savedAlbum = albumRepository.save(album);

        return savedAlbum;
    }

    @CrossOrigin
    @GetMapping(value = "/{id}/media", produces = { MediaType.IMAGE_JPEG_VALUE, MediaType.IMAGE_PNG_VALUE })
    public @ResponseBody byte[] getImage(@PathVariable int id) {

        Path rootLocation = this.fileStorageService.getRootLocation();
        Optional<Media> media = mediaRepository.findMediaByIdAlbumCoverTrue(id);

        if (media.isPresent()) {

            String imageName = media.get().getContenu();
            Path imagePath = rootLocation.resolve(imageName);
            try {
                return Files.readAllBytes(imagePath);
            } catch (IOException e) {
                logger.error(e.getMessage());
            }
        }

        throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Impossible de trouver l'image demandée.");
    }

}
