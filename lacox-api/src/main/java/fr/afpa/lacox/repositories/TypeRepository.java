package fr.afpa.lacox.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import fr.afpa.lacox.models.Type;

@Repository
public interface TypeRepository extends CrudRepository<Type, Integer> {
    
}
