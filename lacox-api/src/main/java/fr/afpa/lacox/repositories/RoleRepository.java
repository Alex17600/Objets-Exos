package fr.afpa.lacox.repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import fr.afpa.lacox.models.Role;
@Repository
public interface RoleRepository extends CrudRepository<Role, Integer> {
    @Query("select r from Role r where r.nom = ?1")
    Role findByName(String nom);

}
