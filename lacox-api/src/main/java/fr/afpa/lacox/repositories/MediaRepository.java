package fr.afpa.lacox.repositories;

import java.util.Optional;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import fr.afpa.lacox.models.Media;

@Repository
public interface MediaRepository extends CrudRepository<Media, Integer>{
    @Query("select m from Media m where m.album.id = ?1 and m.cover = true")
    Optional<Media> findMediaByIdAlbumCoverTrue(int id);

    @Query("SELECT m FROM Media m JOIN m.evenement e WHERE e.type.id = ?1")
    Iterable<Media> findByEvenementAndIdType(int id);

    Optional<Media> findTopByOrderByIdDesc();


}
