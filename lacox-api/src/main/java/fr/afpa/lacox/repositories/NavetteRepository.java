package fr.afpa.lacox.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import fr.afpa.lacox.models.Navette;

@Repository
public interface NavetteRepository extends CrudRepository<Navette, Integer> {
    
}
