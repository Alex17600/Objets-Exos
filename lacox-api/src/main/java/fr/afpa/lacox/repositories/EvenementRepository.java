package fr.afpa.lacox.repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import fr.afpa.lacox.models.Evenement;

@Repository
public interface EvenementRepository extends CrudRepository<Evenement, Integer> {
    @Query("select e from Evenement e where e.type.id = 1")
    Iterable<Evenement> findBySoiree();
      
}
