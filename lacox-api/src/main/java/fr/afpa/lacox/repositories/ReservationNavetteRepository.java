package fr.afpa.lacox.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import fr.afpa.lacox.models.ReservationNavette;

@Repository
public interface ReservationNavetteRepository extends CrudRepository <ReservationNavette, Integer> {    
}
