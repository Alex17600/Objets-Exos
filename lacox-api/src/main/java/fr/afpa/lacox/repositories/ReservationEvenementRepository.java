package fr.afpa.lacox.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import fr.afpa.lacox.models.ReservationEvenement;

@Repository
public interface ReservationEvenementRepository extends CrudRepository<ReservationEvenement, Integer> {
    
}
