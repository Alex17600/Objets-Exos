package fr.afpa.lacox.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import fr.afpa.lacox.models.Album;


@Repository
public interface AlbumRepository extends CrudRepository<Album, Integer> {
    
}
