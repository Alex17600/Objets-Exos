package fr.afpa.lacox.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import fr.afpa.lacox.models.Texte;

@Repository
public interface TexteRepository extends CrudRepository<Texte, Integer> {}
