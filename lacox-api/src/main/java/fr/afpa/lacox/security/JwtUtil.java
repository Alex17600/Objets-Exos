package fr.afpa.lacox.security;

import java.text.ParseException;
import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jose.JWSObject;
import com.nimbusds.jose.Payload;
import com.nimbusds.jose.crypto.MACSigner;
import com.nimbusds.jose.crypto.MACVerifier;
import com.nimbusds.jose.jwk.source.ImmutableSecret;
import com.nimbusds.jose.proc.BadJOSEException;
import com.nimbusds.jose.proc.JWSKeySelector;
import com.nimbusds.jose.proc.JWSVerificationKeySelector;
import com.nimbusds.jose.proc.SecurityContext;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;
import com.nimbusds.jwt.proc.ConfigurableJWTProcessor;
import com.nimbusds.jwt.proc.DefaultJWTProcessor;

public class JwtUtil {

    private static final int expireMinuteToken = 60;
    private static final int expireHourRefreshToken = 1;

    /**
     * Clef secrète permettant d'effectuer le chiffrement du jeton
     */
    private static final String SECRET = "FAAFEZDX7458CDBC534E7EFGTGDEB309E0A21AEC6C9B130E0369C";

    /**
     * Création d'un JWT.
     * 
     * @param mail   Le mail du l'utilisateur authentifié
     * @param issuer L'url du client qui a fait la demandé d'authentification
     * @param roles  Les rôles de l'utilisateur
     * @return Une représentation sous forme de chaîne de caractères des rôles de
     *         l'utilisateur
     */
    public static String createAccessToken(String email, String issuer, List<String> roles) {
        try {
            JWTClaimsSet claims = new JWTClaimsSet.Builder()
                    .subject(email)
                    .issuer(issuer)
                    .claim("email", email)
                    .claim("roles", roles)
                    .expirationTime(Date.from(Instant.now().plusSeconds(expireMinuteToken * 60)))
                    .issueTime(new Date())
                    .build();

            Payload payload = new Payload(claims.toJSONObject());

            JWSObject jwsObject = new JWSObject(new JWSHeader(JWSAlgorithm.HS256), payload);

            jwsObject.sign(new MACSigner(SECRET));
            return jwsObject.serialize();
        } catch (JOSEException e) {
            throw new RuntimeException("Erreur lors de la création du JWT", e);
        }
    }

    /**
     * Même chose que la création d'un JWT d'accès mais sans le fournisseur ni les
     * rôles.
     */
    public static String createRefreshToken(String username) {
        try {
            JWTClaimsSet claims = new JWTClaimsSet.Builder()
                    .subject(username)
                    .expirationTime(Date.from(Instant.now().plusSeconds(expireHourRefreshToken * 3600)))
                    .build();

            Payload payload = new Payload(claims.toJSONObject());

            JWSObject jwsObject = new JWSObject(new JWSHeader(JWSAlgorithm.HS256),
                    payload);

            jwsObject.sign(new MACSigner(SECRET));
            return jwsObject.serialize();
        } catch (JOSEException e) {
            throw new RuntimeException("Erreur lors de la création d'un JWT", e);
        }
    }

    /**
     * Analyse d'un JWT.
     * 
     * @param token Le JWT à traiter
     */
    public static UsernamePasswordAuthenticationToken parseToken(String token)
            throws JOSEException, ParseException, BadJOSEException {

        byte[] secretKey = SECRET.getBytes();
        SignedJWT signedJWT = SignedJWT.parse(token);
        signedJWT.verify(new MACVerifier(secretKey));
        ConfigurableJWTProcessor<SecurityContext> jwtProcessor = new DefaultJWTProcessor<>();

        JWSKeySelector<SecurityContext> keySelector = new JWSVerificationKeySelector<>(JWSAlgorithm.HS256,
                new ImmutableSecret<>(secretKey));
        jwtProcessor.setJWSKeySelector(keySelector);
        jwtProcessor.process(signedJWT, null);
        JWTClaimsSet claims = signedJWT.getJWTClaimsSet();
        String username = claims.getSubject();

        // récupération de la liste des rôles
        List<String> roles = (List<String>) claims.getClaim("roles");
        List<SimpleGrantedAuthority> authorities = null;
        if (roles != null) {
            authorities = roles.stream()
                    .map(SimpleGrantedAuthority::new)
                    .collect(Collectors.toList());
        }

        // classe encapsulant les information d'un utilisateur
        return new UsernamePasswordAuthenticationToken(username, null, authorities);
    }

    /**
     * Analyse d'un JWT.
     * 
     * @param token Le JWT à traiter
     */
    public static String parseTokentoEmail(String authorizationHeader)
            throws JOSEException, ParseException, BadJOSEException {

        // le fameux token contient "Bearer", on s'en débarasse
        String token = authorizationHeader.substring("Bearer ".length());
        byte[] secretKey = SECRET.getBytes();
        SignedJWT signedJWT = SignedJWT.parse(token);
        signedJWT.verify(new MACVerifier(secretKey));
        ConfigurableJWTProcessor<SecurityContext> jwtProcessor = new DefaultJWTProcessor<>();

        JWSKeySelector<SecurityContext> keySelector = new JWSVerificationKeySelector<>(JWSAlgorithm.HS256,
                new ImmutableSecret<>(secretKey));
        jwtProcessor.setJWSKeySelector(keySelector);
        jwtProcessor.process(signedJWT, null);
        JWTClaimsSet claims = signedJWT.getJWTClaimsSet();

        // récupération de la liste des rôles
        String email = (String) claims.getClaim("email");

        // classe encapsulant les information d'un utilisateur
        return email;
    }

}
