package fr.checkbox;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import java.io.IOException;

/**
 * JavaFX App
 */
public class App extends Application {

    private static Scene scene;
    private static String background;

    @Override
    public void start(Stage stage) throws IOException {
        GridPane root = new GridPane();
        root.setHgap(10);
        root.setPadding(new Insets(10));

        TextField textField = new TextField();
        Label textEmpty = new Label();

        Label titreTexteField = new Label("Saississez votre texte");

        // fonction recuperer lettres par lettres le textField
        textField.setOnKeyReleased(event -> {
            String text = textField.getText();
            String[] letters = text.split("");

            String temp = "";
            for (String letter : letters) {
                temp += letter;
                textEmpty.setText(temp);
            }
        });

        // partie gauche
        VBox vBox = new VBox();
        vBox.setPadding(new Insets(10, 10, 10, 10));
        vBox.setSpacing(10);
        vBox.getChildren().addAll(titreTexteField, textField, textEmpty);

        // partie droite
        VBox paramBox = new VBox();
        CheckBox checkBox1 = new CheckBox("Couleur de fond");
        CheckBox checkBox2 = new CheckBox("Couleur des caractères");
        CheckBox checkBox3 = new CheckBox("Casse");
        paramBox.getChildren().addAll(checkBox1, checkBox2, checkBox3);
        TitledPane titledPane = new TitledPane("Paramètres", paramBox);

        // ajout Vboxs haut gauche et haut droite
        root.add(vBox, 1, 1);
        root.add(titledPane, 3, 1);

        // instanciation d'un vbox pour les radiobuttons
        VBox fond = new VBox();
        fond.setSpacing(10);
        // création radiobutton
        RadioButton rouge = new RadioButton("rouge");
        RadioButton vert = new RadioButton("vert");
        RadioButton bleu = new RadioButton("bleu");

        // le toggle fait en sorte qu'un seul RadioButton soit selectionné
        ToggleGroup couleursFond = new ToggleGroup();
        rouge.setToggleGroup(couleursFond);
        vert.setToggleGroup(couleursFond);
        bleu.setToggleGroup(couleursFond);

        // affiche vbox des radiobuttons
        boolean a = checkBox1.isSelected();
        if (a == false) {
            checkBox1.setOnAction(event -> {
                fond.getChildren().addAll(rouge, vert, bleu);
                TitledPane fondTitle = new TitledPane("Fonds", fond);
                root.add(fondTitle, 1, 2);
            });
        }

        VBox slidersBox = new VBox();
        slidersBox.setSpacing(5);
        Label rougeLabelSlider = new Label("Rouge");
        Slider rougeSlide = new Slider(0, 255, 0);
        Label vertLabelSlider = new Label("Vert");
        Slider vertSlide = new Slider(0, 255, 0);
        Label bleuLabelSlider = new Label("Bleu");
        Slider bleuSlide = new Slider(0, 255, 0);
        TitledPane sliderTitle = new TitledPane("Couleur des caractères", slidersBox);


        slidersBox.getChildren().addAll(rougeLabelSlider, rougeSlide, vertLabelSlider, vertSlide, bleuLabelSlider,
                bleuSlide);

        checkBox2.setOnAction(event -> {
            root.add(sliderTitle, 2, 2);
        });

        rougeSlide.setOnMouseDragged(event -> {
            String cssString = "-fx-background-color:" + background + ";-fx-text-fill: rgb(" + rougeSlide.getValue()
                    + "," + vertSlide.getValue() + "," + bleuSlide.getValue() + ")";
            textEmpty.setStyle(cssString);
        });
        vertSlide.setOnMouseDragged(event -> {
            String cssString = "-fx-background-color:" + background + ";-fx-text-fill: rgb(" + rougeSlide.getValue()
                    + "," + vertSlide.getValue() + "," + bleuSlide.getValue() + ")";
            textEmpty.setStyle(cssString);
        });
        bleuSlide.setOnMouseDragged(event -> {
            String cssString = "-fx-background-color:" + background + ";-fx-text-fill: rgb(" + rougeSlide.getValue()
                    + "," + vertSlide.getValue() + "," + bleuSlide.getValue() + ")";
            textEmpty.setStyle(cssString);
        });

        // ajout background choisi
        rouge.setOnAction(event -> {
            background = "red";
            textEmpty.setStyle("-fx-background-color:" + background + ";-fx-text-fill: rgb(" + rougeSlide.getValue()
                    + "," + vertSlide.getValue() + "," + bleuSlide.getValue() + ")");
        });

        vert.setOnAction(event -> {
            background = "green";
            textEmpty.setStyle("-fx-background-color:" + background + ";-fx-text-fill: rgb(" + rougeSlide.getValue()
                    + "," + vertSlide.getValue() + "," + bleuSlide.getValue() + ")");
        });

        bleu.setOnAction(event -> {
            background = "blue";
            textEmpty.setStyle("-fx-background-color:" + background + ";-fx-text-fill: rgb(" + rougeSlide.getValue()
                    + "," + vertSlide.getValue() + "," + bleuSlide.getValue() + ")");
        });

        VBox casse = new VBox();
        casse.setSpacing(10);
        RadioButton majuscule = new RadioButton("Majuscule");
        RadioButton minuscule = new RadioButton("Minuscule");
        casse.getChildren().addAll(majuscule, minuscule);
        ToggleGroup casseCaractere = new ToggleGroup();
        majuscule.setToggleGroup(casseCaractere);
        minuscule.setToggleGroup(casseCaractere);
        TitledPane casseTitle = new TitledPane("Couleur des caractères", casse);
        checkBox3.setOnAction(event -> {
            root.add(casseTitle, 3, 2);
        });

        majuscule.setOnAction(event -> {
            textEmpty.setText(textEmpty.getText().toUpperCase());
        });

        minuscule.setOnAction(event -> {
            textEmpty.setText(textEmpty.getText().toLowerCase());
            
        });

        // affichage Scene générale
        scene = new Scene(root, 500, 300);
        stage.setTitle("CheckBox - Radiobutton - Slider");
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        launch();
    }

}