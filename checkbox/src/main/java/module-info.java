module fr.checkbox {
    requires transitive javafx.controls;
    requires javafx.fxml;

    opens fr.checkbox to javafx.fxml;
    exports fr.checkbox;
}
