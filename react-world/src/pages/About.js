import React from "react";
import Navigation from "../components/Navigation";

const About = () => {
  return (
    <div>
        <Navigation />
      <h1>A propos</h1>
      <br />
      <p>
        Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sequi expedita
        fuga voluptates sed deleniti quam molestias voluptatum ducimus officiis
        debitis. Rerum, delectus possimus soluta nulla corporis itaque. Ab,
        ducimus dolores? Pariatur possimus inventore suscipit magnam minus
        commodi tempore deleniti officia cupiditate quam quia, ea repellendus
        nobis assumenda, asperiores veritatis molestias voluptates quasi optio
        praesentium ducimus. Saepe consequuntur excepturi soluta tempore
        aspernatur, incidunt id inventore labore nisi ullam repellendus
        doloribus possimus temporibus architecto atque. Consequuntur, illo
        dignissimos! Omnis facere dolorum cupiditate nihil ducimus nulla
        praesentium esse, possimus dignissimos quod corrupti voluptatem corporis
        neque laudantium veniam tenetur rerum recusandae dolores nemo vel.
      </p>
      <br />
      <p>
        Lorem, ipsum dolor sit amet consectetur adipisicing elit. Cumque a
        itaque perferendis! Maiores quas dolorem similique? Modi non, cumque
        laborum quam minima, dicta reprehenderit, saepe ullam ex delectus minus
        magnam! Vero aperiam porro saepe corporis magnam obcaecati recusandae
        quaerat quia assumenda, nihil reprehenderit vel. Aliquid enim ipsam
        tenetur officiis sit.
      </p>
    </div>
  );
};

export default About;
