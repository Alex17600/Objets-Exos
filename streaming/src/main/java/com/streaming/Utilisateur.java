package com.streaming;

public class Utilisateur {
    private String nom;
    private int numCompte;
    private String typeCompte;
    private Famille[] familles = new Famille[5];

    public Utilisateur(String nom, int numCompte, String typeCompte) {
        this.nom = nom;
        this.numCompte = numCompte;
        this.typeCompte = typeCompte;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getNumCompte() {
        return numCompte;
    }

    public void setNumCompte(int numCompte) {
        this.numCompte = numCompte;
    }

    public String getTypeCompte() {
        return typeCompte;
    }

    public void setTypeCompte(String typeCompte) {
        this.typeCompte = typeCompte;
    }

    public void ajoutPlaylist(Playlist playlist) {
    }

    public void supprimerPlaylist(Playlist playlist) {
    }

    public Famille[] getFamilles() {
        return familles;
    }

}
