package com.streaming;

import java.util.Date;

public class Film {
    private String titre;
    private Date dateSortie;
    private String acteur;

    public Film(String titre, Date dateSortie, String acteur) {
        this.titre = titre;
        this.dateSortie = dateSortie;
        this.acteur = acteur;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public Date getDateSortie() {
        return dateSortie;
    }

    public void setDateSortie(Date dateSortie) {
        this.dateSortie = dateSortie;
    }

    public String getActeur() {
        return acteur;
    }

    public void setActeur(String acteur) {
        this.acteur = acteur;
    }

}
