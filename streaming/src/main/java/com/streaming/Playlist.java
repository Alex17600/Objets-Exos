package com.streaming;

import java.util.ArrayList;
import java.util.List;

public class Playlist {
    private String nom;
    private List<Film> listeFilm = new ArrayList<Film>();
    
    public Playlist(String nom, List<Film> listeFilm) {
        this.nom = nom;
        this.listeFilm = listeFilm;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public List<Film> getListeFilm() {
        return listeFilm;
    }

    public void setListeFilm(List<Film> listeFilm) {
        this.listeFilm = listeFilm;
    }



}
