package fr.afpa.boutique;

import java.util.ArrayList;
import java.util.Random;

public class App {
    /**
     * Objet de la classe Random pour génération de nombre aléatoire, va être
     * utilisé dans cette classe (d'où le "private")
     */
    private static Random rand = new Random();

    public static void main(String[] args) {
        System.out.println("---- Début de l'application de gestion d'une boutique ----");

        // Instanciation de quelques clients
        Client client1 = new Client(0, "+33645854512", "bob@gmail.fr");
        Client client2 = new Client(1, "+33632154511", "elsa@gmail.fr");
        Client client3 = new Client(1, "+33644154569", "louise@gmail.fr");
        Client client4 = new Client(1, "+33644154588", "luc@gmail.fr");

        // Chargement des articles à partir d'un fichier CSV
        ArrayList<Article> articles = ArticlesCSVLoader.chargerArticles("produits.csv");

        // Ajout de 4 achats aux clients 1 et 2
        // le choix des articles se fait aléatoirement
        for (int i = 0; i < 4; i++) {
            // la méthode nextInt() de la classe Random
            // permet de générer un entier aléatoire compris entre 0 inclus et l'entier
            // passé en
            // paramètre exclus.
            int randomIdArticle = randomNumber(articles.size());

            // on prend un article au hasard dans la liste
            Article randomArticle = articles.get(randomIdArticle);
            client1.ajouterAchat(randomArticle);

            randomIdArticle = randomNumber(articles.size());
            randomArticle = articles.get(randomIdArticle);
            client2.ajouterAchat(randomArticle);
        }

        // On ajoute des achats aux clients 3 et 4
        int randomIdArticle = randomNumber(articles.size());
        client3.ajouterAchat(articles.get(randomIdArticle));
        client3.ajouterAchat(articles.get(randomIdArticle));
        randomIdArticle = randomNumber(articles.size());
        client4.ajouterAchat(articles.get(randomIdArticle));

        // Création de la base de clients
        BaseClients baseClient = new BaseClients();

        // TODO ajouter les clients créés à la Base de clients
        baseClient.ajouterClient(client1);
        baseClient.ajouterClient(client2);
        baseClient.ajouterClient(client3);
        baseClient.ajouterClient(client4);

        // TODO afficher les informations de tous les clients considérés comme fidèles
        System.out.println("Liste des clients fidèles: ");
        for (Client client : baseClient.getClientsFideles()) {
            System.out.println(client.getEmail());
        }
        System.out.println("-------------------------");

        System.out.println("Le meilleur client est: " + baseClient.bestCustomers().getEmail());
        System.out.println("-------------------------");

    }

    /**
     * Renvoie une un nombre aléatoire entre 0 et valeurMax (inclus)
     */
    public static int randomNumber(int valeurMax) {
        return rand.nextInt(valeurMax);
    }
}
