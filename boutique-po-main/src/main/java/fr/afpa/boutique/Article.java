package fr.afpa.boutique;

public class Article {

//// Début des attributs

    /**
     * Dénomination textuelle de l'article
     */
    private String name;
    private double prixHorsTaxe;

    /**
     * TVA exprimée en pourcentage
     * Exemples : 10; 5.5
     */
    private double tva;
    private String description;
/// Fin de la déclaration des attributs

    public Article(String name, double prixHorsTaxe, double tva, String description) {
        this.name = name;
        this.prixHorsTaxe = prixHorsTaxe;
        this.tva = tva;
        this.description = description;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrixHorsTaxe() {
        return this.prixHorsTaxe;
    }

    public void setPrixHorsTaxe(double prixHorsTaxe) {
        this.prixHorsTaxe = prixHorsTaxe;
    }

    public double getTva() {
        return this.tva;
    }

    public void setTva(double tva) {
        this.tva = tva;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    //ToString
    public String toString() {
        return "Nom de l'article: " + this.name +"," + "\n" + "Prix hors taxe: " + this.prixHorsTaxe + "," + "\n" + "TVA: " + this.tva + "," + "\n" + "Description" + this.description;
        
    }

    //calcul du prix total
    public double calculateNetPrix (){
        double prixNet = this.prixHorsTaxe + (this.prixHorsTaxe * this.tva);
        return prixNet;
    }

} // fin de la classe Article
