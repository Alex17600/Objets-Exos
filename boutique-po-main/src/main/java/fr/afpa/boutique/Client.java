package fr.afpa.boutique;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;

public class Client {

    private int numeroClient;
    private LocalDate dateDernierAchat;
    private String numeroTelephone;
    private String email;
    private ArrayList<Article> articlesAchetes = new ArrayList<Article>();


    public Client(int numeroClient, String numeroTelephone, String email) {
        this.numeroClient = numeroClient;
        this.numeroTelephone = numeroTelephone;
        this.email = email;
    }

    
    public int getNumeroClient() {
        return this.numeroClient;
    }

    public void setNumeroClient(int numeroClient) {
        this.numeroClient = numeroClient;
    }

    public LocalDate getDateDernierAchat() {
        return this.dateDernierAchat;
    }

    public void setDateDernierAchat(LocalDate dateDernierAchat) {
        this.dateDernierAchat = dateDernierAchat;
    }

    public String getNumeroTelephone() {
        return this.numeroTelephone;
    }

    public void setNumeroTelephone(String numeroTelephone) {
        this.numeroTelephone = numeroTelephone;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public ArrayList<Article> getListAchats (){
        return this.articlesAchetes;
    }


    public boolean clientFidele() {
        boolean estFidele = false;
        int nbAchat = 0;
        for(Article article : this.articlesAchetes) {
            nbAchat++;
        }
        if(nbAchat >= 4) {
            estFidele = true;
        }
        return estFidele;
    }

    /**
     * Calcule la somme des dépenses du client
     * @return
     */
    public double totalDepense() {
        double totalDepense = 0;
        for(Article articles : articlesAchetes){
            totalDepense += articles.calculateNetPrix();
        }

        return totalDepense; 
    }

    /**
     * Ajoute un achat à la liste des achats du client
     * @param article L'article acheté
     */
    public void ajouterAchat(Article article) {
        this.articlesAchetes.add(article);
        this.dateDernierAchat = LocalDate.now();
  
    }
}
