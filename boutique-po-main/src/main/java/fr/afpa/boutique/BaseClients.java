package fr.afpa.boutique;

import java.util.ArrayList;

public class BaseClients {

    private ArrayList<Client> clients = new ArrayList<Client>();

    /**
     * Constructeur vide, pas de paramètres
     * okay car au départ pas de clients créés
     */
    public BaseClients() {

    }

    public void ajouterClient(Client client) {
        clients.add(client);
    }

    public ArrayList<Client> getClientsFideles() {
        ArrayList<Client> clientsFideles = new ArrayList<Client>();
        for(Client client : clients){
           if(client.clientFidele()) {
            clientsFideles.add(client);
           }
        }return clientsFideles;
    }

    public ArrayList<Client> getClients() {
        return this.clients;
    }

    public Client bestCustomers(){
        double maxDepense = 0.0;
        Client bestClient = clients.get(0);
        for(Client client : clients){
            if(maxDepense < client.totalDepense()){
                maxDepense = client.totalDepense();
                bestClient = client;
            }
        }
        return bestClient;
    }

    /**
     * Retourne une liste des clients considérés comme fidèles
     */
    public ArrayList<Client> clientsFideles() {

        ArrayList<Client> clientsFideles = new ArrayList<>();
        return clientsFideles;
    } 
}
