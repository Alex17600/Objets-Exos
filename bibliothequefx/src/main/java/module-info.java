module com.bibliothequefx {
    requires transitive javafx.controls;
    requires javafx.fxml;
    opens com.bibliothequefx to javafx.fxml;
    opens com.bibliothequefx.Controlleurs to javafx.fxml;
    exports com.bibliothequefx;
    exports com.bibliothequefx.Controlleurs;
}
