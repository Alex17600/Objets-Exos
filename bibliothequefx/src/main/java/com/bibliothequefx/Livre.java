package com.bibliothequefx;

public class Livre extends Document {
    private String auteur;


    public Livre(String id, String titre, String auteur) {
        super(id, titre);
        this.auteur = auteur;
    }

    public String getAuteur() {
        return auteur;
    }

    public void setAuteur(String auteur) {
        this.auteur = auteur;
    }
    
}
