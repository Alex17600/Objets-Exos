package com.bibliothequefx;

public class Employe extends Personne implements ValideEmprunt {

    public Employe(String nom, String prenom) {
        super(nom, prenom);
    }

    public void envoyerLettre(Emprunteur emprunteur) {
    }

    public void profilEmprunteur(long id) {
    }

    public void modifierDocument(Document document) {
    }

    public void modifierEmprunteur(Emprunteur emprunteur) {
    }

    @Override
    public boolean valideEmprunt(Emprunt emprunt) {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Unimplemented method 'valideEmprunt'");
    }
}
