package com.bibliothequefx;

import java.util.Date;

public class Emprunt {
    private Date date;
    private Emprunteur emprunteur;
    private Document document;

    public Emprunt(Date date, Emprunteur emprunteur, Document document) {
        this.date = date;
        this.emprunteur = emprunteur;
        this.document = document;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Emprunteur getEmprunteur() {
        return emprunteur;
    }

    public void setEmprunteur(Emprunteur emprunteur) {
        this.emprunteur = emprunteur;
    }

    public Document getDocument() {
        return document;
    }

    public void setDocument(Document document) {
        this.document = document;
    }



}
