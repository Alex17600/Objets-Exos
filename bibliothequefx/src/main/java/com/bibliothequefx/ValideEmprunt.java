package com.bibliothequefx;

public interface ValideEmprunt {

    public boolean valideEmprunt(Emprunt emprunt);
    
}
