package com.bibliothequefx.Controlleurs;

import java.io.IOException;

import com.bibliothequefx.App;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class AccueilControlleur {
    @FXML
    private BorderPane accueil;
    @FXML
    private BorderPane documentaliste;
    @FXML
    private BorderPane emprunteur;
    @FXML
    private BorderPane employe;

    // Une fonction qu'elle est vachement bien pratique!!
    public ControlleurRoute changeView(String fxml) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource(fxml));
        Pane center = (Pane) loader.load();
        ControlleurRoute controller = loader.getController();
        controller.setMainController(this);
        accueil.setCenter(center);
        return controller;
    }
    
    @FXML
    public void documentaliste(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader(App.class.getResource("documentaliste.fxml"));
        documentaliste = (BorderPane) loader.load();
        Scene scene = new Scene(documentaliste);
        Stage stage = (Stage) accueil.getScene().getWindow();
        stage.setScene(scene);
        stage.show();
    }

    @FXML
    public void emprunteur(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader(App.class.getResource("emprunteur.fxml"));
        emprunteur = (BorderPane) loader.load();
        Scene scene = new Scene(emprunteur);
        Stage stage = (Stage) accueil.getScene().getWindow();
        stage.setScene(scene);
        stage.show();
    }
    @FXML
    public void employe(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader(App.class.getResource("employe.fxml"));
        employe = (BorderPane) loader.load();
        Scene scene = new Scene(employe);
        Stage stage = (Stage) accueil.getScene().getWindow();
        stage.setScene(scene);
        stage.show();
    }


}
