module fr.scenebuilder2 {
    requires transitive javafx.controls;
    requires javafx.fxml;

    opens fr.scenebuilder2 to javafx.fxml;
    exports fr.scenebuilder2;
}
