package fr.scenebuilder2;

import javafx.util.Duration;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.paint.Color;

public class TableViewController {
    @FXML
    private TextField firstName;
    @FXML
    private TextField lastName;
    @FXML
    private TextField city;
    @FXML
    private TableView<Person> tableView;
    @FXML
    private TableColumn<Person, String> tableViewFirstName;
    @FXML
    private TableColumn<Person, String> tableViewName;
    @FXML
    private TableColumn<Person, String> tableViewCity;
    @FXML
    private Button saveButton;
    @FXML
    private Button cancelButton;
    @FXML
    private Button deleButton;
    @FXML
    private Label labelSucces;

    private ObservableList<Person> persons = FXCollections.observableArrayList();

    @FXML
    public void initialize() {
        persons.add(new Person("Josh", "Homme", "Joshua Tree"));
        persons.add(new Person("Dave", "Grohl", "Warren"));
        persons.add(new Person("Robert", "Trujillo", "Santa Monica"));

        tableViewFirstName.setCellValueFactory(cellData -> cellData.getValue().getFirstName());
        tableViewName.setCellValueFactory(cellData -> cellData.getValue().getLastName());
        tableViewCity.setCellValueFactory(cellData -> cellData.getValue().getCity());

        tableView.getItems().addAll(persons);
        labelSucces.setText("");
    }

    @FXML
    public void save(ActionEvent event) {

        String firstNameProperty = firstName.getText();
        String lastNameProperty = lastName.getText();
        String cityProperty = city.getText().toUpperCase();
        tableView.getItems().add(new Person(firstNameProperty, lastNameProperty, cityProperty));

        BackgroundFill greenFill = new BackgroundFill(Color.GREEN, null, null);
        Background greenBackground = new Background(greenFill);
        BackgroundFill whiteFill = new BackgroundFill(Color.WHITE, null, null);
        Background whiteBackground = new Background(whiteFill);
        firstName.setBackground(greenBackground);
        Timeline timeline = new Timeline(
                new KeyFrame(Duration.seconds(2), e -> firstName.setBackground(whiteBackground)));
        timeline.play();

        labelSucces.setText("Ajout réussi");
    }

    @FXML
    public void cancel(ActionEvent event) {
        firstName.setText("");
        lastName.setText("");
        city.setText("");

        firstName.setStyle("");
        lastName.setStyle("");
        city.setStyle("");

        labelSucces.setText("");
    }

    @FXML
    public void delete(ActionEvent event) {
        Person selectedPerson = tableView.getSelectionModel().getSelectedItem();
        if (selectedPerson != null) {
            tableView.getItems().remove(selectedPerson);
        }
        firstName.setStyle("");
        lastName.setStyle("");
        city.setStyle("");
        labelSucces.setText("Suppression réussie");
    }
}
