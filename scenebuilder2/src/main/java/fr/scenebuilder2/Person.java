package fr.scenebuilder2;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Person {
    private StringProperty firstname;
    private StringProperty lastname;    
    private StringProperty city;

    
    public Person(String firstname, String lastname, String city) {
        this.firstname = new SimpleStringProperty (firstname);
        this.lastname = new SimpleStringProperty (lastname);
        this.city = new SimpleStringProperty (city);
    }


    public StringProperty getFirstName() {
        return firstname;
    }


    public void setFirstName(StringProperty firstname) {
        this.firstname = firstname;
    }


    public StringProperty getLastName() {
        return lastname;
    }


    public void setLastName(StringProperty lastname) {
        this.lastname = lastname;
    }


    public StringProperty getCity() {
        return city;
    }


    public void setCity(StringProperty city) {
        this.city = city;
    }

}
