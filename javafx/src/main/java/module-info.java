module fr.testjavafx {
    requires transitive javafx.controls;
    requires javafx.fxml;

    opens fr.testjavafx to javafx.fxml;
    exports fr.testjavafx;
}
