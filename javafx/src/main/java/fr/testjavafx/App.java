package fr.testjavafx;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Labeled;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * JavaFX App
 */
public class App extends Application {

    @Override
    public void start(Stage stage) throws IOException {
        System.out.println("start ... ");

        Label label = new Label("VBox");
        label.setStyle("-fx-font-size: 30px; -fx-text-fill: white;");
        label.relocate(50, 20);

        Button button1 = new Button("Button 1");
        Button button2 = new Button("Button 2");
        Button button3 = new Button("Button 3");
        Button button4 = new Button("Button 4");

        button1.addEventHandler(MouseEvent.MOUSE_CLICKED, new MouseEventHandler() {
        });

        
        VBox root = new VBox();
        root.setPadding(new Insets(10, 10, 10, 10));
        
        ///
        root.getChildren().add(label);
        root.getChildren().add(button1);
        root.getChildren().add(button2);
        root.getChildren().add(button3);
        root.getChildren().add(button4);
        ///

        Scene scene = new Scene( root , 300 , 100 , Color.LIGHTGREEN);
        stage.setTitle( "Une Fenêtre en JFX") ;
        stage.setScene(scene) ;
        stage.show() ;
    }

    public class MouseEventHandler implements EventHandler<MouseEvent> {

        @Override
         public void handle(MouseEvent event) {
            System.out.println("Click sur " + ((Labeled) event.getSource()).getText());
         }
    }

    public static void main(String[] args) {
        launch();
    }
  

}