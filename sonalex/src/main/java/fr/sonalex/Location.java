package fr.sonalex;

import java.sql.Date;
import java.util.ArrayList;

public class Location {

    private Date dateDebut;
    private Date dateFin;
    private ArrayList<Article> listMateriel = new ArrayList<Article>();

    public Location(Date dateDebut, Date dateFin, ArrayList<Article> listMateriel) {
        this.listMateriel = listMateriel;
    }

    public Date getDateDebut() {
        return dateDebut;
    }

    public void setDateDebut(Date dateDebut) {
        this.dateDebut = dateDebut;
    }

    public Date getDateFin() {
        return dateFin;
    }

    public void setDateFin(Date dateFin) {
        this.dateFin = dateFin;
    }

    public ArrayList<Article> getListMateriel() {
        return listMateriel;
    }

    public void setListMateriel(ArrayList<Article> listMateriel) {
        this.listMateriel = listMateriel;
    }

    //create function to add a materiel to the list
    public ArrayList<Article> addArticleOnLocation(Article article) {
        this.listMateriel.add(article);
        return listMateriel;
    }


    @Override
    public String toString() {
        return "Location [dateDebut=" + dateDebut + ", dateFin=" + dateFin + ", listMateriel=" + listMateriel + "]";
    }

}
