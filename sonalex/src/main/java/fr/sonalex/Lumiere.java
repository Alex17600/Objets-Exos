package fr.sonalex;

public class Lumiere extends Article{
    
    private int wattPower;

    public Lumiere(String nom, String reference, String type, int wattPower, double prix) {
        super(nom, reference, type, prix);
        this.wattPower = wattPower;
    }

    public int getWattPower() {
        return wattPower;
    }

    public void setWattPower(int wattPower) {
        this.wattPower = wattPower;
    } 
}
