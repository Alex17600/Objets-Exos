package fr.sonalex;

import java.util.ArrayList;
import java.util.Collections;
import java.sql.Date;


public class App {
    private static ArrayList<Client> clients = new ArrayList<Client>();
    private static ArrayList<Article> articles = new ArrayList<Article>();

    public static void main(String[] args) {
        // jeu de test client list
        Client client1 = new Client("Dupond", "Jean-pierre", "jpdeupond@gmail.com", "0685963025");
        clients.add(client1);
        Client client2 = new Client("Eclair", "Elodie", "eclairelodie@gmail.com", "05460258365");
        clients.add(client2);
        Client client3 = new Client("Klus", "Max", "mklus@gmail.com", "0658472514");
        clients.add(client3);
        Client client4 = new Client("Fitero", "Noemie", "fitno@gmail.com", "0625966658");
        clients.add(client4);

        for (Client client : clients) {
            System.out.println(client);
        }


        // jeu de test article
        Article enceinte1 = new Enceinte("Roam", "AA256IJ", "Sans fil", 80, 299);
        articles.add(enceinte1);
        Article enceinte2 = new Enceinte("BoomTone DJ", "1574MIOL", "Amplifié", 500, 199);
        articles.add(enceinte2);
        Article lumiere1 = new Lumiere("Mac spot 100", "758KLMO", "Lyre", 150, 250);
        articles.add(lumiere1);
        Article ampli1 = new Amplificateur("Elokance", "HA2400", "Analogique", 800, 999);
        articles.add(ampli1);


        // 1 er test
        Date newDate = new Date(2022, 2, 12);

        Location loc1 = new Location(newDate, newDate, articles);

        client1.addLocationToClient(loc1);
 

        // 2 eme test

        Location loc2 = new Location(newDate, newDate, articles);
        System.out.println(loc2);

        //marche aussi avec le Collections.sort()
        Collections.reverse(articles);
        for (Article article : articles) {
            System.out.println(article.toString());
        }

    }
}
