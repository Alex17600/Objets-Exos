package fr.sonalex;

public class Article implements Comparable<Article> {


    private String nom;
    private String reference;
    private String type;
    private double prix;


    //constructeur
    public Article(String nom, String reference, String type, double prix) {
        this.nom = nom;
        this.reference = reference;
        this.type = type;
        this.prix = prix;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int compareTo(Article o) {
        if (this.prix > o.prix) {
            return 1;
        } else if (this.prix < o.prix) {
            return -1;
        } else {
            return 0;
        }
    }

    @Override
    public String toString() {
        return "Article [nom=" + nom + ", reference=" + reference + ", type=" + type + ", prix=" + prix + "]";
    }

    
}
