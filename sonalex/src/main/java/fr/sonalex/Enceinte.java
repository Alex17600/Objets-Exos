package fr.sonalex;

public class Enceinte extends Article {
    

    private int rmsPower;
    
    public Enceinte(String nom, String reference, String type, int rmsPower, double prix) {
        super(nom , reference, type, prix);
        this.rmsPower = rmsPower;
    }

    public int getRmsPower() {
        return rmsPower;
    }

    public void setRmsPower(int rmsPower) {
        this.rmsPower = rmsPower;
    }
    
}
