package fr.sonalex;

import java.util.ArrayList;

public class Client {
    private String nom;
    private String prenom;
    private String email;
    private String numTel;

    //modifié : client en article, un client a une liste d'articles
    private ArrayList<Location> listLocation = new ArrayList<Location>();
    
    

    // Constructeur class Client
    public Client(String nom, String prenom, String email, String numTel) {
        this.nom = nom;
        this.prenom = prenom;
        this.email = email;
        this.numTel = numTel;
    }

    //modif client -> article
    public Client(String nom, String prenom, String email, String numTel, ArrayList<Location> listLocation) {
        this.nom = nom;
        this.prenom = prenom;
        this.email = email;
        this.numTel = numTel;
        this.listLocation = listLocation;
    }


    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNumTel() {
        return numTel;
    }

    public void setNumTel(String numTel) {
        this.numTel = numTel;
    }

    public ArrayList<Location> getListLocation() {
        return listLocation;
    }

    public void setListLocation(ArrayList<Location> listLocation) {
        this.listLocation = listLocation;
    }

    public ArrayList<Location> addLocationToClient(Location loc){
        this.getListLocation().add(loc);
        return this.listLocation;
    }

    @Override
    public String toString() {
        return "Client " + "\n" + "nom = " + nom + "\n" + "prenom = " + prenom + "\n" + "email = " + email + "\n" +"numTel = " + numTel + "\n";
    }

    
}
