package fr.sonalex;

import scala.collection.mutable.StringBuilder;

public class Amplificateur extends Article {
    
    private int power;

    public Amplificateur(StringBuilder nom, String reference, String type, int power, double prix) {
        super(nom, reference, type, prix);
        this.power = power;
    }

    public int getWattPower() {
        return power;
    }

    public void setWattPower(int power) {
        this.power = power;
    }

}
