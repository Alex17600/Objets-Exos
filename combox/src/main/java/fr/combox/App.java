package fr.combox;

import javafx.geometry.Insets;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import java.io.IOException;

/**
 * JavaFX App
 */
public class App extends Application {

    private static Scene scene;

    @Override
    public void start(Stage stage) throws IOException {
        GridPane root = new GridPane();
        root.setPadding(new Insets(10));

        Label labelComboBox = new Label("Pays disponibles : ");

        ObservableList<Country> comboBoxList = FXCollections.observableArrayList();
        comboBoxList.add(new Country("France", "250"));
        comboBoxList.add(new Country("Belgique", "056"));
        comboBoxList.add(new Country("Allemagne", "278"));
        comboBoxList.add(new Country("Suisse", "756"));
        comboBoxList.add(new Country("Espagne", "724"));
        ComboBox<Country> comboBox = new ComboBox<>(comboBoxList);

        ObservableList<String> listViewList = FXCollections.observableArrayList();
        ListView<String> listView = new ListView<>(listViewList);

        root.add(labelComboBox, 0, 0);
        root.add(comboBox, 0, 1);

        VBox buttonBox = new VBox();
        buttonBox.setSpacing(10);
        buttonBox.setPadding(new Insets(0, 20, 0, 20));
        Button addElement = new Button(">");
        Button addAllElement = new Button(">>");
        Button deleteElement = new Button("<");
        Button deleteAllElement = new Button("<<");

        Button haut = new Button("haut");
   
        Button bas = new Button("bas");


        root.add(bas, 2, 2);
        root.add(haut, 2, 1);
        root.add(listView, 2, 0);

        buttonBox.getChildren().addAll(addElement, addAllElement, deleteElement, deleteAllElement);

        root.add(buttonBox, 1, 0);

        haut.setOnAction(event -> {
            int selectedIndex = listView.getSelectionModel().getSelectedIndex();
            if (selectedIndex > 0) {
                String selectedItem = listView.getSelectionModel().getSelectedItem();
                listView.getItems().remove(selectedIndex);
                listView.getItems().add(selectedIndex - 1, selectedItem.toString());
                listView.getSelectionModel().select(selectedIndex - 1);
            }
        });

        bas.setOnAction(event -> {
            int selectedIndex = listView.getSelectionModel().getSelectedIndex();
            if (selectedIndex < listView.getItems().size() - 1) {
                listView.getItems().remove(selectedIndex);
                listView.getItems().add(selectedIndex + 1, listView.getSelectionModel().getSelectedItem());
                listView.getSelectionModel().select(selectedIndex + 1);
            }
        });

        addElement.setOnAction(event -> {
            Country selectedItem = comboBox.getSelectionModel().getSelectedItem();
            if (selectedItem != null) {
                listView.getItems().add(selectedItem.toString());
            }
        });

        addAllElement.setOnAction(event -> {
            for (Country country : comboBoxList) {
                listViewList.add(country.toString());
            }
        });

        deleteElement.setOnAction(event -> {
            ObservableList<String> selectedItems = listView.getSelectionModel().getSelectedItems();
            listViewList.removeAll(selectedItems);
        });

        deleteAllElement.setOnAction(event -> {
            listViewList.clear();
        });

        scene = new Scene(root, 500, 300);
        stage.setTitle("Combox");
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        launch();
    }

}
