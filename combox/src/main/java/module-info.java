module fr.combox {
    requires transitive javafx.controls;
    requires javafx.fxml;

    opens fr.combox to javafx.fxml;
    exports fr.combox;
}
