package com.hotel;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import com.hotel.Database.DAORoom;
import com.hotel.Modeles.Room;

public class RoomDAOTest {

    @Test
    public void testGetId () {
        DAORoom daoRoom = new DAORoom();
        Room roomExpected = new Room (1, "test", 4, 5, 4 ); 
        Room roomResult = daoRoom.findById(1);
        assertEquals(roomExpected, roomResult);
    }
}
