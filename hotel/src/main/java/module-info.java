module com.hotel {
    requires transitive javafx.controls;
    requires javafx.fxml;
    requires transitive java.sql;

    opens com.hotel to javafx.fxml;
    opens com.hotel.Controlleur to javafx.fxml;
    opens com.hotel.Modeles to javafx.fxml;
    opens com.hotel.Database to javafx.fxml;

    exports com.hotel;
    exports com.hotel.Controlleur;
    exports com.hotel.Modeles;
    exports com.hotel.Database;

}
