package com.hotel.Controlleur;

import java.util.List;
import com.hotel.Database.DAOHotel;
import com.hotel.Database.DAORoom;
import com.hotel.Modeles.Hotel;
import com.hotel.Modeles.Room;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListCell;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;

public class PopupRoomControlleur {
    @FXML
    private Button addButton;
    @FXML
    private TextField numberRoomLabel;
    @FXML
    private TextField capacityRoomLabel;
    @FXML
    private TextField typeRoomLabel;
    @FXML
    private ComboBox<Hotel> comboboxAllHotel;
    private ObservableList<Hotel> hotelSelection = FXCollections.observableArrayList();

    public void initialize() {
        DAOHotel hotelDAO = new DAOHotel();
        List<Hotel> hotels = hotelDAO.findAll();
        hotelSelection.addAll(hotels);

        // Ajouter les hôtels à la ComboBox en affichant seulement leur nom
        comboboxAllHotel.setItems(hotelSelection);
        comboboxAllHotel.setCellFactory(cell -> new ListCell<Hotel>() {
            @Override
            protected void updateItem(Hotel hotel, boolean empty) {
                super.updateItem(hotel, empty);
                if (empty) {
                    setText(null);
                } else {
                    setText(hotel.getName());
                }
            }
        });
        comboboxAllHotel.setButtonCell(comboboxAllHotel.getCellFactory().call(null));
    }

    public void addRoomButton() {
        DAORoom roomDAO = new DAORoom();
        Room roomCreate = new Room(
                numberRoomLabel.getText(),
                Integer.parseInt(capacityRoomLabel.getText()),
                Integer.parseInt(typeRoomLabel.getText()),
                comboboxAllHotel.getSelectionModel().getSelectedItem().getId());

        roomDAO.create(roomCreate);
        Alert alert = new Alert(AlertType.INFORMATION);
        alert.setTitle("Ajout Accepté");
        alert.setContentText("Rappel de l'ajout: Chambre N° " + numberRoomLabel.getText() + ", Capacité: "
                + capacityRoomLabel.getText() + ", Type de chambre: " + typeRoomLabel.getText());
        alert.showAndWait();

        Stage stage = (Stage) addButton.getScene().getWindow();
        stage.close();
    }
}
