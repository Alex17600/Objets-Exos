package com.hotel.Controlleur;

import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;
import com.hotel.Database.DAOBooking;
import com.hotel.Database.DAOClient;
import com.hotel.Database.DAOHotel;
import com.hotel.Database.DAORoom;
import com.hotel.Database.DAOStation;
import com.hotel.Modeles.Booking;
import com.hotel.Modeles.Client;
import com.hotel.Modeles.Hotel;
import com.hotel.Modeles.Room;
import com.hotel.Modeles.Station;
import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.converter.IntegerStringConverter;

public class BookingControlleur {
    private LocalDate dateResa = LocalDate.now();
    @FXML
    private Button addReservation;
    @FXML
    private BorderPane bookingBorderPane;
    @FXML
    private DatePicker datePickerStart;
    @FXML
    private DatePicker datePickerEnd;
    @FXML
    private TableView<Station> tableViewStation;
    @FXML
    private TableView<Hotel> tableViewHotel;
    @FXML
    private TableView<Room> tableViewRoom;
    @FXML
    private TableView<Client> listClients;
    @FXML
    private Label labelStation;
    @FXML
    private Label labelHotel;
    @FXML
    private Label labelChambre;
    @FXML
    private TextField textFieldLastName;
    @FXML
    private TextField textFieldFirstName;
    @FXML
    private TextField texteFieldAddress;
    @FXML
    private TextField textefieldCity;
    @FXML
    private TextField texteFieldPrice;
    @FXML
    private TextField texteFieldDeposit;
    @FXML
    private TableColumn<Station, String> nom;
    @FXML
    private TableColumn<Station, Integer> altitude;
    @FXML
    private TableColumn<Hotel, String> nomHotel;
    @FXML
    private TableColumn<Hotel, String> villeHotel;
    @FXML
    private TableColumn<Room, String> numberRoom;
    @FXML
    private TableColumn<Room, Integer> capacityRoom;
    @FXML
    private TableColumn<Client, String> nomClient;
    @FXML
    private TableColumn<Client, String> prenomClient;
    @FXML
    private VBox vBoxCreateClient;
    private ObservableList<Station> stations = FXCollections.observableArrayList();
    private ObservableList<Client> clients = FXCollections.observableArrayList();

    public void initialize() {
        // tableViewAllBookings.setVisible(false);
        vBoxCreateClient.setVisible(false);
        tableViewStation.setEditable(true);
        tableViewHotel.setVisible(false);
        tableViewRoom.setVisible(false);
        DAOStation daoStation = new DAOStation();
        List<Station> allStation = daoStation.findAll();

        if (!allStation.isEmpty()) {
            stations.addAll(allStation);
        }

        if (stations != null) {
            tableViewStation.getItems().clear();

            nom.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getName()));
            nom.setCellFactory(TextFieldTableCell.forTableColumn());
            nom.setOnEditCommit(event -> {
                Station station = event.getRowValue();
                station.setName(event.getNewValue());
                daoStation.update(station);
            });

            altitude.setCellValueFactory(cellData -> {
                int altitudeValue = cellData.getValue().getAltitude();
                return Bindings.createObjectBinding(() -> altitudeValue);
            });
            altitude.setCellFactory(TextFieldTableCell.forTableColumn(new IntegerStringConverter()));
            altitude.setOnEditCommit(event -> {
                Station station = event.getRowValue();
                station.setAltitude(event.getNewValue());
                daoStation.update(station);
            });

            tableViewStation.getItems().addAll(stations);
        }

        listClients.setEditable(true);
        DAOClient daoClient = new DAOClient();
        List<Client> allClient = daoClient.findAll();

        if (allClient != null) {
            nomClient.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getLast_name()));
            nomClient.setCellFactory(TextFieldTableCell.forTableColumn());
            nomClient.setOnEditCommit(event -> {
                Client client = event.getRowValue();
                client.setLast_name(event.getNewValue());
                daoClient.update(client);
            });
            prenomClient.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getFirst_name()));
            prenomClient.setCellFactory(TextFieldTableCell.forTableColumn());
            prenomClient.setOnEditCommit(event -> {
                Client client = event.getRowValue();
                client.setFirst_name(event.getNewValue());
                daoClient.update(client);
            });

            clients.addAll(allClient);
            listClients.setItems(clients);

        }
    }

    public void loadHotelOfStationSelected() {
        tableViewStation.setEditable(true);
        tableViewHotel.setEditable(true);
        // recup de la station selectionné
        Station station = tableViewStation.getSelectionModel().getSelectedItem();
        // instance du DAO hotel
        DAOHotel daoHotel = new DAOHotel();
        List<Hotel> hotelsSelected = daoHotel.findAllByStation(station);

        if (hotelsSelected != null) {
            tableViewHotel.getItems().clear();

            nomHotel.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getName()));
            nomHotel.setCellFactory(TextFieldTableCell.forTableColumn());
            nomHotel.setOnEditCommit(event -> {
                Hotel hotel = event.getRowValue();
                hotel.setName(event.getNewValue());
                daoHotel.update(hotel);
            });
            villeHotel.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getCity()));
            villeHotel.setCellFactory(TextFieldTableCell.forTableColumn());
            villeHotel.setOnEditCommit(event -> {
                Hotel hotel = event.getRowValue();
                hotel.setCity(event.getNewValue());
                daoHotel.update(hotel);
            });
            tableViewHotel.setVisible(true);
            tableViewHotel.getItems().addAll(hotelsSelected);
        }
    }

    public void loadRoomOfHotelSelected() {
        Hotel hotel = tableViewHotel.getSelectionModel().getSelectedItem();

        DAORoom daoRoom = new DAORoom();
        List<Room> rooms = daoRoom.findRoombyHotel(hotel);

        if (rooms != null) {
            tableViewRoom.getItems().clear();

            numberRoom.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getNumber()));
            numberRoom.setCellFactory(TextFieldTableCell.forTableColumn());
            numberRoom.setOnEditCommit(event -> {
                Room room = event.getRowValue();
                room.setNumber(event.getNewValue());
                daoRoom.update(room);
            });
            altitude.setCellValueFactory(cellData -> {
                int altitudeValue = cellData.getValue().getAltitude();
                return Bindings.createObjectBinding(() -> altitudeValue);
            });
            capacityRoom.setCellFactory(TextFieldTableCell.forTableColumn(new IntegerStringConverter()));
            capacityRoom.setOnEditCommit(event -> {
                Room room = event.getRowValue();
                room.setCapacity(event.getNewValue());
                daoRoom.update(room);
            });

            tableViewRoom.setVisible(true);
            tableViewRoom.getItems().addAll(rooms);
        }
    }

    @FXML
    public void showCreateclient() throws SQLException {
        listClients.setVisible(false);
        vBoxCreateClient.setVisible(true);
    }

    @FXML
    public void ajoutClient() throws SQLException {
        // creation du client avant le booking
        DAOClient daoClient = new DAOClient();
        Client client = new Client(
                textFieldLastName.getText(),
                textFieldFirstName.getText(),
                texteFieldAddress.getText(),
                textefieldCity.getText());
        daoClient.create(client);

        clients.add(client);
        listClients.refresh();
        listClients.setVisible(true);
        vBoxCreateClient.setVisible(false);
    }

    @FXML
    public void createBooking() {
        Client client = listClients.getSelectionModel().getSelectedItem();
        Room room = tableViewRoom.getSelectionModel().getSelectedItem();
        if (client != null && room != null) {
            DAOBooking daoBooking = new DAOBooking();
            Booking bookingCreate = new Booking(
                    dateResa,
                    datePickerStart.getValue(),
                    datePickerEnd.getValue(),
                    Float.parseFloat(texteFieldPrice.getText()),
                    Float.parseFloat(texteFieldDeposit.getText()),
                    room.getId(),
                    client.getId());
            daoBooking.create(bookingCreate);
            Alert alert = new Alert(AlertType.INFORMATION);
            alert.setTitle("Reservation Accepté");
            alert.setContentText("Votre réservation au nom de " + client.getLast_name() + " à été accepté");
            alert.showAndWait();

            Stage stage = (Stage) addReservation.getScene().getWindow();
            stage.close();
        } else {
            Alert alert = new Alert(AlertType.ERROR);
            alert.setTitle("Erreur Résa");
            alert.setContentText("Veuillez selectionné les données");
            alert.showAndWait();
        }

    }

}
