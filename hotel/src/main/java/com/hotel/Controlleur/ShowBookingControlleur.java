package com.hotel.Controlleur;

import java.sql.SQLException;
import java.util.List;
import com.hotel.App;
import com.hotel.Database.DAOBooking;
import com.hotel.Modeles.Booking;
import com.hotel.utils.DateUtil;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.beans.property.SimpleFloatProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.util.converter.FloatStringConverter;
import javafx.util.converter.IntegerStringConverter;

public class ShowBookingControlleur {
    @FXML
    private TableView<Booking> tableViewBooking;
    @FXML
    private TableColumn<Booking, String> dateResa;
    @FXML
    private TableColumn<Booking, String> arriveLe;
    @FXML
    private TableColumn<Booking, String> partiLe;
    @FXML
    private TableColumn<Booking, Float> coutSejour;
    @FXML
    private TableColumn<Booking, Float> acompte;
    @FXML
    private TableColumn<Booking, Integer> numeroChambre;
    @FXML
    private TableColumn<Booking, Integer> numeroClient;

    public void initialize() {
        DAOBooking daoBooking = new DAOBooking();
        List<Booking> bookings = daoBooking.findAll();

        if (bookings != null) {

            dateResa.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getBookingDate().format(DateUtil.frFormatter)));

            arriveLe.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getStayStartDate().format(DateUtil.frFormatter)));

            partiLe.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getStayEndDate().format(DateUtil.frFormatter)));

            coutSejour.setCellValueFactory(
                    cellData -> new SimpleFloatProperty(cellData.getValue().getPrice()).asObject());
            coutSejour.setCellFactory(TextFieldTableCell.forTableColumn(new FloatStringConverter()));

            acompte.setCellValueFactory(
                    cellData -> new SimpleFloatProperty(cellData.getValue().getDeposit()).asObject());
            acompte.setCellFactory(TextFieldTableCell.forTableColumn(new FloatStringConverter()));

            numeroChambre.setCellValueFactory(
                    cellData -> new SimpleIntegerProperty(cellData.getValue().getRoomId()).asObject());
            numeroChambre.setCellFactory(TextFieldTableCell.forTableColumn(new IntegerStringConverter()));

            numeroClient.setCellValueFactory(
                    cellData -> new SimpleIntegerProperty(cellData.getValue().getClientId()).asObject());
            numeroClient.setCellFactory(TextFieldTableCell.forTableColumn(new IntegerStringConverter()));

            tableViewBooking.getItems().addAll(bookings);
        }

    }

    @FXML
    public void addBooking() throws SQLException {
        App.openBookingPopup(1000, 650);
    }
}
