package com.hotel.Controlleur;

import java.util.List;

import com.hotel.App;
import com.hotel.Database.DAOHotel;
import com.hotel.Database.DAOStation;
import com.hotel.Modeles.Hotel;
import com.hotel.Modeles.Station;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

public class PopupControlleur {

    private ImageView theRockImage = new ImageView(new Image(App.class.getResourceAsStream("rock.png")));

    @FXML
    private Button closeButton;
    @FXML
    private TextField nameHotelTextfield;;
    @FXML
    private TextField adresseHotelTextField;
    @FXML
    private TextField villeHotelTextField;
    @FXML
    private ComboBox<Integer> comboCategorieHotel;
    ObservableList<Integer> categoryList = FXCollections.observableArrayList();
    @FXML
    private ComboBox<Station> stationIdComboBox;
    ObservableList<Station> stationList = FXCollections.observableArrayList();

    @FXML
    public void initialize() {
        DAOStation daoStation = new DAOStation();
        List<Station> stations = daoStation.findAll();
        categoryList.addAll(1, 2, 3, 4, 5);
        stationIdComboBox.getItems().addAll(stations);
        comboCategorieHotel.setItems(categoryList);
    }

    @FXML
    public void close() {
        Stage stage = (Stage) closeButton.getScene().getWindow();
        stage.close();
    }

    @FXML
    public void addHotel() {
        DAOHotel daoHotel = new DAOHotel();
        Hotel hotelCreate = new Hotel(
                nameHotelTextfield.getText(),
                comboCategorieHotel.getSelectionModel().getSelectedItem(),
                adresseHotelTextField.getText(),
                villeHotelTextField.getText(),
                stationIdComboBox.getSelectionModel().getSelectedItem().getId());
        daoHotel.create(hotelCreate);
        Alert alert = new Alert(AlertType.INFORMATION);
        alert.setTitle("Ajout Accepté");
        alert.setHeaderText("Choc Accepté");
        alert.setGraphic(theRockImage);
        alert.getGraphic().setScaleX(0.2);
        alert.getGraphic().setScaleY(0.2);
        alert.setContentText("Rappel de l'ajout: " + nameHotelTextfield.getText() + " "
                + adresseHotelTextField.getText() + " " + villeHotelTextField.getText());
        alert.showAndWait();

        Stage stage = (Stage) closeButton.getScene().getWindow();
        stage.close();
    }

}
