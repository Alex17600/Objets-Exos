package com.hotel.Controlleur;

import java.sql.SQLException;
import java.util.List;

import com.hotel.App;
import com.hotel.Database.DAOHotel;
import com.hotel.Database.DAORoom;
import com.hotel.Database.DAOStation;
import com.hotel.Modeles.Hotel;
import com.hotel.Modeles.Room;
import com.hotel.Modeles.Station;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;

public class AccueilControlleur {
    @FXML
    private BorderPane accueiBorderPane;
    @FXML
    private VBox navbarAccueil;
    @FXML
    private Label messageDoubleClicked;
    @FXML
    private Label labelStation1;
    @FXML
    private Button buttonRemoveHotel;
    @FXML
    private Button addRoomButton;
    @FXML
    private Button buttonAddHotel;
    @FXML
    private Button deleteRoomButton;
    @FXML
    private TableColumn<Hotel, String> columnName;
    @FXML
    private TableColumn<Hotel, String> columnCity;
    @FXML
    private TableColumn<Hotel, String> columnAddress;
    @FXML
    private TableColumn<Room, String> numeroChambre;
    @FXML
    private TableColumn<Room, String> capaciteChambre;
    @FXML
    private TableView<Hotel> tableViewStation;
    @FXML
    private TableView<Room> tableViewChambre;
    @FXML
    private GridPane gridButtom;
    @FXML
    private ImageView shumi;

    private ObservableList<Station> stations = FXCollections.observableArrayList();
    private ObservableList<Hotel> hotels = FXCollections.observableArrayList();

    @FXML
    public void initialize() {
        gridButtom.setVisible(false);
        messageDoubleClicked.setVisible(false);
        tableViewChambre.setVisible(false);

        DAOStation daoStation = new DAOStation();
        List<Station> allStation = daoStation.findAll();

        if (!allStation.isEmpty()) {
            stations.addAll(allStation);
            for (Station station : stations) {
                Label label = new Label(station.getName() + " " + station.getAltitude() + " mètres");
                label.setStyle("-fx-font-size: 18px;");
                label.addEventHandler(MouseEvent.MOUSE_CLICKED, new MouseEventHandler());
                navbarAccueil.getChildren().add(label);
            }
        }

        
    }

    /**
     * @param station
     */
    public void loadHotels(Station station) {
        deleteRoomButton.setVisible(false);
        addRoomButton.setVisible(false);
        tableViewStation.setEditable(true);
        DAOHotel daoHotel = new DAOHotel();
        List<Hotel> hotelsSelected = daoHotel.findAllByStation(station);

        if (hotelsSelected != null) {
            tableViewStation.getItems().clear();

            columnName.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getName()));
            columnName.setCellFactory(TextFieldTableCell.forTableColumn());
            columnName.setOnEditCommit(event -> {
                Hotel hotel = event.getRowValue();
                hotel.setName(event.getNewValue());
                daoHotel.update(hotel);
            });
            columnCity.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getCity()));
            columnCity.setCellFactory(TextFieldTableCell.forTableColumn());
            columnCity.setOnEditCommit(event -> {
                Hotel hotel = event.getRowValue();
                hotel.setCity(event.getNewValue());
                daoHotel.update(hotel);
            });
            columnAddress.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getAddress()));
            columnAddress.setCellFactory(TextFieldTableCell.forTableColumn());
            columnAddress.setOnEditCommit(event -> {
                Hotel hotel = event.getRowValue();
                hotel.setAddress(event.getNewValue());
                daoHotel.update(hotel);
            });
            tableViewStation.getItems().addAll(hotelsSelected);
        }
    }

    private class MouseEventHandler implements EventHandler<Event> {
        @Override
        public void handle(Event event) {
            gridButtom.setVisible(true);
            messageDoubleClicked.setVisible(true);
            Label labelClicked = (Label) event.getSource();
            int indexLabel = navbarAccueil.getChildren().indexOf(labelClicked);
            Station stationSelected = stations.get(indexLabel - 3);
            hotels.clear();
            loadHotels(stationSelected);
        }
    }

    @FXML
    public void removeHotel() throws SQLException {
        Hotel hotelSelected = tableViewStation.getSelectionModel().getSelectedItem();
        DAOHotel daoHotel = new DAOHotel();

        if (daoHotel.findByRoom(hotelSelected.getId()) == null) {
            tableViewStation.getItems().remove(hotelSelected);
            daoHotel.delete(hotelSelected);
            tableViewStation.refresh();

        } else {
            Alert alert = new Alert(AlertType.INFORMATION);
            alert.setTitle("Alerte");
            alert.setContentText(
                    "Vous ne pouvez pas supprimer un hotel qui contient des chambres, soyez pas bêtes non plus!");
            alert.showAndWait();
        }
    }

    @FXML
    public void removeRoom() throws SQLException {
        Room roomSelected = tableViewChambre.getSelectionModel().getSelectedItem();
        DAORoom daoRoom = new DAORoom();
        System.out.println(daoRoom.findById(roomSelected.getId()));
        try {
            tableViewChambre.getItems().remove(roomSelected);
            daoRoom.delete(roomSelected);
            tableViewChambre.refresh();
            ;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @FXML
    public void loadRoom() {
        deleteRoomButton.setVisible(true);
        addRoomButton.setVisible(true);
        shumi.setVisible(false);
        Hotel selectedHotel = tableViewStation.getSelectionModel().getSelectedItem();
        
        if (selectedHotel != null) {
            try {
                DAORoom room = new DAORoom();
                List<Room> roomsSelected = room.findRoombyHotel(selectedHotel);

                tableViewChambre.getItems().clear();

                if (roomsSelected != null) {
                    numeroChambre
                            .setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getNumber()));
                    numeroChambre.setCellFactory(TextFieldTableCell.forTableColumn());
                    numeroChambre.setOnEditCommit(event -> {
                        Room roomUpdate = event.getRowValue();
                        roomUpdate.setNumber(event.getNewValue());
                        room.update(roomUpdate);
                    });
                    capaciteChambre.setCellValueFactory(
                            cellData -> new SimpleStringProperty(String.valueOf(cellData.getValue().getCapacity())));
                    capaciteChambre.setCellFactory(TextFieldTableCell.forTableColumn());
                    capaciteChambre.setOnEditCommit(event -> {
                        Room roomUpdate = event.getRowValue();
                        int newCapacity = Integer.parseInt(event.getNewValue());
                        roomUpdate.setCapacity(newCapacity);
                        room.update(roomUpdate);
                    });

                    tableViewChambre.setVisible(true);
                    tableViewChambre.getItems().addAll(roomsSelected);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @FXML
    public void addHotelPopup() {
        App.openPopupAddHotel(450, 800);
    }

    @FXML
    public void addRoomPopup() {
        App.openRoomPopup(1200, 800);
    }

    @FXML
    public void addBooking() throws SQLException {
        App.openBookingPopup(1000, 650);
    }

    @FXML
    public void showAllBookings () {
        App.openShowBookingPopup(1000, 700);
    }

}
