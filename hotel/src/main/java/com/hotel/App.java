package com.hotel;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;
import java.io.IOException;


/**
 * JavaFX App
 */
public class App extends Application {
    private static Scene scene;

    @Override
    public void start(Stage stage) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("accueil.fxml"));
        scene = new Scene(root, 1200, 900);
        stage.setScene(scene);
        stage.show();
    }

    public static void openPopupAddHotel (double width, double height) {
        try {
            final Stage popup = new Stage();
            FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("popupAddHotel.fxml"));
            Scene scenepopup = new Scene(fxmlLoader.load(), width, height);
            popup.setMinWidth(width);
            popup.setMinHeight(height);
            if(!popup.getModality().equals(Modality.APPLICATION_MODAL)){
                popup.initModality(Modality.APPLICATION_MODAL);
            }
            popup.setScene(scenepopup);
            popup.show();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void openRoomPopup (double width, double height) {
        try {
            final Stage popup = new Stage();
            FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("popupAddRoom.fxml"));
            Scene scenepopup = new Scene(fxmlLoader.load(), width, height);
            popup.setMinWidth(width);
            popup.setMinHeight(height);
            if(!popup.getModality().equals(Modality.APPLICATION_MODAL)){
                popup.initModality(Modality.APPLICATION_MODAL);
            }
            popup.setScene(scenepopup);
            popup.show();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void openBookingPopup (double width, double height) {
        try {
            final Stage popup = new Stage();
            FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("booking.fxml"));
            Scene scenepopup = new Scene(fxmlLoader.load(), width, height);
            popup.setMinWidth(width);
            popup.setMinHeight(height);
            if(!popup.getModality().equals(Modality.APPLICATION_MODAL)){
                popup.initModality(Modality.APPLICATION_MODAL);
            }
            popup.setScene(scenepopup);
            popup.show();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void openShowBookingPopup (double width, double height) {
        try {
            final Stage popup = new Stage();
            FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("showBooking.fxml"));
            Scene scenepopup = new Scene(fxmlLoader.load(), width, height);
            popup.setMinWidth(width);
            popup.setMinHeight(height);
            if(!popup.getModality().equals(Modality.APPLICATION_MODAL)){
                popup.initModality(Modality.APPLICATION_MODAL);
            }
            popup.setScene(scenepopup);
            popup.show();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public static void main(String[] args) {
        launch();
    }

}