package com.hotel.utils;

import java.sql.Date;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class DateUtil {
    
    public static DateTimeFormatter frFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");

    public static Date localDateToDate (LocalDate localDate) {
        Date date = Date.valueOf(localDate);
        return date;
    } 

    public static LocalDate dateToLocalDate (Date date) {
        LocalDate localdate = date.toLocalDate();
        return localdate;
    }
}
