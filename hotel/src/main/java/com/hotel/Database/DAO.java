package com.hotel.Database;

import java.util.List;

public abstract class DAO<T> {
    public abstract T findById(int id) throws Exception;
    public abstract List<T> findAll();
    public abstract T create(T object);
    public abstract void update(T object);
    public abstract void delete(T object);
}
