package com.hotel.Database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.hotel.Modeles.Hotel;
import com.hotel.Modeles.Room;

public class DAORoom extends DAO<Room> {

    private DAOConnect connect = DAOConnect.getInstance();

    @Override
    public Room findById(int id) {
        Connection conn = connect.getConnection();
        Room room = null;
        try {
            PreparedStatement stm = conn.prepareStatement("SELECT * FROM hotel WHERE id = ?");
            stm.setInt(1, id);
            ResultSet result = stm.executeQuery();
            if (result.next()) {
                room = new Room();
                room.setId(result.getInt("id"));
                room.setNumber(result.getString("number"));
                room.setCapacity(result.getInt("capacity"));
            }
            result.close();
            stm.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return room;
    }

    @Override
    public List<Room> findAll() {
        Connection conn = connect.getConnection();
        List<Room> rooms = new ArrayList<>();
        try {
            PreparedStatement stm = conn.prepareStatement("SELECT * FROM room");
            ResultSet result = stm.executeQuery();
            while (result.next()) {
                Room room = new Room();
                room.setId(result.getInt("id"));
                room.setNumber(result.getString("number"));
                room.setCapacity(result.getInt("capacity"));
                room.setType(result.getInt("type"));
                room.setHotel_id(result.getInt("hotel_id"));
                rooms.add(room);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rooms;
    }

    public List<Room> findRoombyHotel(Hotel hotel) {
        Connection conn = connect.getConnection();
        List<Room> rooms = new ArrayList<>();
        try {
            PreparedStatement stm = conn.prepareStatement("SELECT * FROM room where hotel_id = " + hotel.getId());
            ResultSet result = stm.executeQuery();
            while (result.next()) {
                Room room = new Room();
                room.setId(result.getInt("id"));
                room.setNumber(result.getString("number"));
                room.setCapacity(result.getInt("capacity"));
                room.setType(result.getInt("type"));
                room.setHotel_id(result.getInt("hotel_id"));
                rooms.add(room);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rooms;
    }

    @Override
    public Room create(Room object) {
        Connection conn = connect.getConnection();
        try {
            PreparedStatement stm = conn
                    .prepareStatement("INSERT INTO room (number, capacity, type, hotel_id) VALUES (?, ?, ?, ?)");
            stm.setString(1, object.getNumber());
            stm.setInt(2, object.getCapacity());
            stm.setInt(3, object.getType());
            stm.setInt(4, object.getHotel_id());
            stm.executeUpdate();
            stm.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return object;
    }

    @Override
    public void update(Room object) {
        Connection conn = connect.getConnection();
        try {
            PreparedStatement stm = conn.prepareStatement("UPDATE room set number, capacity, type, hotel_id");
            stm.setString(1, object.getNumber());
            stm.setInt(2, object.getCapacity());
            stm.setInt(3, object.getType());
            stm.setInt(4, object.getType());
            stm.executeUpdate();
            stm.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void delete(Room object) {
        Connection conn = connect.getConnection();
        try {
            PreparedStatement stm = conn.prepareStatement("DELETE FROM room where id =?");
            stm.setInt(1, object.getId());
            stm.executeUpdate();
            stm.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
