package com.hotel.Database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.hotel.Modeles.Station;

public class DAOStation extends DAO<Station> {

    private DAOConnect connect = DAOConnect.getInstance();

    @Override
    public Station findById(int id) {
       Connection conn = connect.getConnection();
       Station station = null;

       try {
            PreparedStatement stm = conn.prepareStatement("SELECT * FROM station WHERE id = ?");
            stm.setInt(1, id);
            ResultSet result = stm.executeQuery();
            if (result.next()) {
                station = new Station();
                station.setId(result.getInt("id"));
                station.setName(result.getString("name"));
                station.setAltitude(result.getInt("altitude"));
            }
            result.close();
            stm.close();
       } catch (Exception e) {
        e.printStackTrace();
       }
       return station;
    }

    @Override
    public List<Station> findAll() {
        Connection conn = connect.getConnection();
        List<Station> stations = new ArrayList<>();
        try {
            PreparedStatement stm = conn.prepareStatement("SELECT * FROM station");
            ResultSet result = stm.executeQuery();
            while (result.next()) {
                Station station = new Station();
                station.setId(result.getInt("id"));
                station.setName(result.getString("name"));
                station.setAltitude(result.getInt("altitude"));
                stations.add(station);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return stations;
    }

    @Override
    public Station create(Station object) {
        Connection conn = connect.getConnection();
        try {
            PreparedStatement stm = conn.prepareStatement("INSERT INTO room (name, altitude) VALUES (?, ?)");
            stm.setString(1, object.getName());
            stm.setInt(2, object.getAltitude());
            stm.executeUpdate();
            stm.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return object;
    }

    @Override
    public void update(Station object) {
        Connection conn = connect.getConnection();
        try {
            PreparedStatement stm = conn.prepareStatement("UPDATE station set name, altitude");
            stm.setString(1, object.getName());
            stm.setInt(2, object.getAltitude());
            stm.executeUpdate();
            stm.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void delete(Station object) {
        Connection conn = connect.getConnection();
       try {
        PreparedStatement stm = conn.prepareStatement("DELETE FROM station where id =?");
        stm.setInt(1, object.getId());
        stm.executeUpdate();
        stm.close();
       } catch (Exception e) {
        e.printStackTrace();
       }
    }
}
