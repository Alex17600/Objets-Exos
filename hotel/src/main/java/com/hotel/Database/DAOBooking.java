package com.hotel.Database;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import com.hotel.Modeles.Booking;
import com.hotel.utils.DateUtil;

public class DAOBooking extends DAO<Booking> {

    private DAOConnect connect = DAOConnect.getInstance();

    @Override
    public Booking findById(int id) throws Exception {
        Connection conn = connect.getConnection();
        Booking booking = null;
        
        try {
            PreparedStatement stm = conn.prepareStatement("SELECT * FROM bookings WHERE id = ?", id);
            stm.setInt(1, id);
            ResultSet r = stm.executeQuery();
            if (r.next()) {
                LocalDate dateResa = DateUtil.dateToLocalDate(r.getDate("booking_date"));
                LocalDate dateDebut = DateUtil.dateToLocalDate(r.getDate("stay_start_date"));
                LocalDate dateFin = DateUtil.dateToLocalDate(r.getDate("stay_end_date"));
                booking = new Booking();
                booking.setId(r.getInt("id"));
                booking.setBookingDate(dateResa);
                booking.setStayStartDate(dateDebut);
                booking.setStayEndDate(dateFin);
                booking.setPrice(r.getFloat("price"));
                booking.setDeposit(r.getFloat("deposit"));
                booking.setRoomId(r.getInt("room_id"));
                booking.setClientId(r.getInt("client_id"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return booking;
    }

    @Override
    public List<Booking> findAll() {
        Connection conn = connect.getConnection();
        List<Booking> bookings = new ArrayList<>();
        try {
            PreparedStatement stm = conn.prepareStatement("SELECT * FROM booking");
            ResultSet r = stm.executeQuery();
            while (r.next()) {
                LocalDate dateResa = DateUtil.dateToLocalDate(r.getDate("booking_date"));
                LocalDate dateDebut = DateUtil.dateToLocalDate(r.getDate("stay_start_date"));
                LocalDate dateFin = DateUtil.dateToLocalDate(r.getDate("stay_end_date"));
                Booking booking = new Booking();
                booking.setId(r.getInt("id"));
                booking.setBookingDate(dateResa);
                booking.setStayStartDate(dateDebut);
                booking.setStayEndDate(dateFin);
                booking.setPrice(r.getFloat("price"));
                booking.setDeposit(r.getFloat("deposit"));
                booking.setRoomId(r.getInt("room_id"));
                booking.setClientId(r.getInt("client_id"));

                bookings.add(booking);
            }
            r.close();
            stm.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bookings;
    }
    
    @Override
    public Booking create(Booking object) {
        Connection conn = connect.getConnection();
        try {
            PreparedStatement stm = conn.prepareStatement(
                    "INSERT INTO booking (booking_date, stay_start_date, stay_end_date, price, deposit, room_id, client_id) VALUES (?, ?, ?, ?, ?, ?, ?)");

            Date dateResa = DateUtil.localDateToDate(object.getBookingDate());
            Date dateDebut = DateUtil.localDateToDate(object.getStayStartDate());
            Date dateFin = DateUtil.localDateToDate(object.getStayEndDate());

            stm.setDate(1, dateResa);
            stm.setDate(2, dateDebut);
            stm.setDate(3, dateFin);
            stm.setFloat(4, object.getPrice());
            stm.setFloat(5, object.getDeposit());
            stm.setInt(6, object.getRoomId());
            stm.setInt(7, object.getClientId());

            stm.executeUpdate();
            stm.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return object;
    }


    @Override
    public void update(Booking object) {
        throw new UnsupportedOperationException("Unimplemented method 'update'");
    }

    @Override
    public void delete(Booking object) {
        throw new UnsupportedOperationException("Unimplemented method 'delete'");
    }

}
