package com.hotel.Database;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public final class DAOConnect {
    private static DAOConnect instance;
    private Connection connection;

    private DAOConnect() {
        try {
            connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/Hotel", "postgres", "Djaltek30");
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public static DAOConnect getInstance() {
        if (instance == null) {
            instance = new DAOConnect();
        }
        return instance;
    }

    public Connection getConnection() {
        return connection;
    }
}
