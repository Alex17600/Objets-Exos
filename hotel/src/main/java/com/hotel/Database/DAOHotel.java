package com.hotel.Database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.hotel.Modeles.Hotel;
import com.hotel.Modeles.Station;

public class DAOHotel extends DAO<Hotel> {

    private DAOConnect connect = DAOConnect.getInstance();

    @Override
    public Hotel findById(int id) {
        Connection conn = connect.getConnection();
        Hotel hotel = null;
        try {
            PreparedStatement stm = conn.prepareStatement("SELECT * FROM hotel WHERE id = ?");
            stm.setInt(1, id);
            ResultSet result = stm.executeQuery();
            if (result.next()) {
                hotel = new Hotel();
                hotel.setId(result.getInt("id"));
                hotel.setName(result.getString("name"));
                hotel.setCategory(result.getInt("category"));
                hotel.setAddress(result.getString("address"));
                hotel.setCity(result.getString("city"));
                hotel.setStation_id(result.getInt("station_id"));
            }
            result.close();
            stm.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return hotel;
    }

    public Hotel findByRoom(int id) {
        Connection conn = connect.getConnection();
        Hotel hotel = null;
        try {
            PreparedStatement stm = conn.prepareStatement("SELECT * FROM hotel h join room r on h.id = r.hotel_id WHERE h.id = ?");
            stm.setInt(1, id);
            ResultSet result = stm.executeQuery();
            if (result.next()) {
                hotel = new Hotel();
                hotel.setId(result.getInt("id"));
                hotel.setName(result.getString("name"));
                hotel.setCategory(result.getInt("category"));
                hotel.setAddress(result.getString("address"));
                hotel.setCity(result.getString("city"));
                hotel.setStation_id(result.getInt("station_id"));
            }
            result.close();
            stm.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return hotel;
    }

    public List<Hotel> findHotelbyStation(Station station) {
        Connection conn = connect.getConnection();
        List<Hotel> hotels = new ArrayList<>();
        try {
            PreparedStatement stm = conn.prepareStatement("SELECT * FROM hotel where station_id = " + station.getId());
            ResultSet result = stm.executeQuery();
            while (result.next()) {
                Hotel hotel = new Hotel();
                hotel.setId(result.getInt("id"));
                hotel.setName(result.getString("name"));
                hotel.setCategory(result.getInt("category"));
                hotel.setAddress(result.getString("address"));
                hotel.setCity(result.getString("city"));
                hotel.setStation_id(result.getInt("station_id"));

                hotels.add(hotel);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return hotels;
    }

    @Override
    public List<Hotel> findAll() {
        Connection conn = connect.getConnection();
        List<Hotel> hotels = new ArrayList<>();
        try {
            PreparedStatement stm = conn.prepareStatement("SELECT * FROM hotel");
            ResultSet result = stm.executeQuery();
            while (result.next()) {
                Hotel hotel = new Hotel();
                hotel.setId(result.getInt("id"));
                hotel.setName(result.getString("name"));
                hotel.setCategory(result.getInt("category"));
                hotel.setAddress(result.getString("address"));
                hotel.setCity(result.getString("city"));
                hotel.setStation_id(result.getInt("station_id"));
                hotels.add(hotel);
            }
            result.close();
            stm.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return hotels;
    }

    public List<Hotel> findAllByStation(Station station) {
        Connection conn = connect.getConnection();
        List<Hotel> hotels = new ArrayList<>();
        try {
            PreparedStatement stm = conn.prepareStatement("SELECT * FROM hotel where station_id = " + station.getId());
            ResultSet result = stm.executeQuery();
            while (result.next()) {
                Hotel hotel = new Hotel();
                hotel.setId(result.getInt("id"));
                hotel.setName(result.getString("name"));
                hotel.setCategory(result.getInt("category"));
                hotel.setAddress(result.getString("address"));
                hotel.setCity(result.getString("city"));
                hotel.setStation_id(result.getInt("station_id"));
                hotels.add(hotel);
            }
            result.close();
            stm.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return hotels;
    }

    @Override
    public Hotel create(Hotel object) {
        Connection conn = connect.getConnection();
        try {
            PreparedStatement stm = conn.prepareStatement(
                    "INSERT INTO hotel (name, category, address, city, station_id) VALUES (?, ?, ?, ?, ?)");
            stm.setString(1, object.getName());
            stm.setInt(2, object.getCategory());
            stm.setString(3, object.getAddress());
            stm.setString(4, object.getCity());
            stm.setInt(5, object.getStation_id());
            stm.executeUpdate();
            stm.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return object;
    }

    @Override
    public void update(Hotel object) {
        Connection conn = connect.getConnection();
        try {
            PreparedStatement stm = conn.prepareStatement(
                    "UPDATE hotel SET name = ?, category = ?, address = ?, city = ?, station_id = ? WHERE id = ?");
            stm.setString(1, object.getName());
            stm.setInt(2, object.getCategory());
            stm.setString(3, object.getAddress());
            stm.setString(4, object.getCity());
            stm.setInt(5, object.getStation_id());
            stm.setInt(6, object.getId());
            stm.executeUpdate();
            stm.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void delete(Hotel object) {
        Connection conn = connect.getConnection();
        try {
            PreparedStatement stm = conn.prepareStatement("DELETE FROM hotel WHERE id = ?");
            stm.setInt(1, object.getId());
            stm.executeUpdate();
            stm.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
