package com.hotel.Database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.hotel.Modeles.Client;

public class DAOClient extends DAO<Client> {
    private DAOConnect connect = DAOConnect.getInstance();
    
    @Override
    public Client findById(int id) throws Exception {
        Connection conn = connect.getConnection();
        Client client = null;
        try {
            PreparedStatement stm = conn.prepareStatement("SELECT * FROM client WHERE id = ?");
            stm.setInt(1, id);
            ResultSet result = stm.executeQuery();
            if (result.next()) {
                client = new Client();
                client.setId(result.getInt("id"));
                client.setLast_name(result.getString("last_name"));
                client.setFirst_name(result.getString("first_name"));
                client.setAddress(result.getString("address"));
                client.setCity(result.getString("city"));

            }
            result.close();
            stm.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return client;
    }

    @Override
    public List<Client> findAll() {
        Connection conn = connect.getConnection();
        List<Client> clients = new ArrayList<>();
        try {
            PreparedStatement stm = conn.prepareStatement("SELECT * FROM client");
            ResultSet result = stm.executeQuery();
            while (result.next()) {
                Client client = new Client();
                client.setId(result.getInt("id"));
                client.setLast_name(result.getString("last_name"));
                client.setFirst_name(result.getString("first_name"));
                client.setAddress(result.getString("address"));
                client.setCity(result.getString("city"));

                clients.add(client);
            }
            result.close();
            stm.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return clients;
    }

    @Override
    public Client create(Client object) {
        Connection conn = connect.getConnection();
        try {
            PreparedStatement stm = conn.prepareStatement(
                    "INSERT INTO client (last_name, first_name, address, city) VALUES (?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
            stm.setString(1, object.getLast_name());
            stm.setString(2, object.getFirst_name());
            stm.setString(3, object.getAddress());
            stm.setString(4, object.getCity());

            stm.executeUpdate();
            ResultSet generatedKey = stm.getGeneratedKeys();
            generatedKey.next();
            int id = generatedKey.getInt(1);
            object.setId(id);
            stm.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return object;    
    }

    @Override
    public void update(Client object) {
        throw new UnsupportedOperationException("Unimplemented method 'update'");
    }

    @Override
    public void delete(Client object) {
        throw new UnsupportedOperationException("Unimplemented method 'delete'");
    }
}
