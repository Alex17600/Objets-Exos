package com.hotel.Modeles;

public class Client {
    private int id = -1;
    private String last_name;
    private String first_name;
    private String address;
    private String city;

    public Client(int id, String last_name, String first_name, String address, String city) {
        this.id = id;
        this.last_name = last_name;
        this.first_name = first_name;
        this.address = address;
        this.city = city;
    }

    public Client(String last_name, String first_name, String address, String city) {
        this.last_name = last_name;
        this.first_name = first_name;
        this.address = address;
        this.city = city;
    }

    public Client() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Override
    public String toString() {
        return "Client [id=" + id + ", last_name=" + last_name + ", first_name=" + first_name + ", address=" + address
                + ", city=" + city + "]";
    }

}
