package com.hotel.Modeles;

public class Hotel {
    private int id;
    private String name;
    private int category;
    private String address;
    private String city;
    private int station_id;

    public Hotel(int id, int station_id, String name, int category, String address, String city) {
        this.id = id;
        this.station_id = station_id;
        this.name = name;
        this.category = category;
        this.address = address;
        this.city = city;

    }

    public Hotel(String name, int category, String address, String city, int station_id) {
        this.name = name;
        this.category = category;
        this.address = address;
        this.city = city;
        this.station_id = station_id;
    }

    public Hotel() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCategory() {
        return category;
    }

    public void setCategory(int category) {
        this.category = category;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getStation_id() {
        return station_id;
    }

    public void setStation_id(int station_id) {
        this.station_id = station_id;
    }

    @Override
    public String toString() {
        return name + ", category=" + category + ", address=" + address + ", city="
                + city + ", station_id=" + station_id + "]";
    }

}
