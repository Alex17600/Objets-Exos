package com.hotel.Modeles;

import java.time.LocalDate;

public class Booking {
    private int id;
    private LocalDate bookingDate;
    private LocalDate stayStartDate;
    private LocalDate stayEndDate;
    private float price;
    private float deposit;
    private int roomId;
    private int clientId;

    public Booking(int id, LocalDate bookingDate, LocalDate stayStartDate, LocalDate stayEndDate, float price, float deposit,
            int roomId, int clientId) {
        this.id = id;
        this.bookingDate = bookingDate;
        this.stayStartDate = stayStartDate;
        this.stayEndDate = stayEndDate;
        this.price = price;
        this.deposit = deposit;
        this.roomId = roomId;
        this.clientId = clientId;
    }

    public Booking(LocalDate bookingDate, LocalDate stayStartDate, LocalDate stayEndDate, float price, float deposit, int roomId, int clientId) {
        this.bookingDate = bookingDate;
        this.stayStartDate = stayStartDate;
        this.stayEndDate = stayEndDate;
        this.price = price;
        this.deposit = deposit;
        this.roomId = roomId;
        this.clientId = clientId;
    }

    public Booking() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public LocalDate getBookingDate() {
        return bookingDate;
    }

    public void setBookingDate(LocalDate bookingDate) {
        this.bookingDate = bookingDate;
    }

    public LocalDate getStayStartDate() {
        return stayStartDate;
    }

    public void setStayStartDate(LocalDate stayStartDate) {
        this.stayStartDate = stayStartDate;
    }

    public LocalDate getStayEndDate() {
        return stayEndDate;
    }

    public void setStayEndDate(LocalDate stayEndDate) {
        this.stayEndDate = stayEndDate;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public float getDeposit() {
        return deposit;
    }

    public void setDeposit(float deposit) {
        this.deposit = deposit;
    }

    public int getRoomId() {
        return roomId;
    }

    public void setRoomId(int roomId) {
        this.roomId = roomId;
    }

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    @Override
    public String toString() {
        return "Booking [id=" + id + ", bookingDate=" + bookingDate + ", stayStartDate=" + stayStartDate
                + ", stayEndDate=" + stayEndDate + ", price=" + price + ", deposit=" + deposit + ", roomId="
                + roomId + ", clientId=" + clientId + "]";
    }

}
