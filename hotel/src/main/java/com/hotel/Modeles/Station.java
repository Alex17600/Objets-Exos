package com.hotel.Modeles;

public class Station {
    private int id;
    private String name;
    private int altitude;

    public Station(int id, String name, int altitude) {
        this.id = id;
        this.name = name;
        this.altitude = altitude;
    }

    public Station() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAltitude() {
        return altitude;
    }

    public void setAltitude(int altitude) {
        this.altitude = altitude;
    }
    
    @Override
    public String toString() {
        return name + " " + altitude ;
    }

}
