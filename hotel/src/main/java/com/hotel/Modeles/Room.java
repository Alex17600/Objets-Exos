package com.hotel.Modeles;

public class Room {
    private int id;
    private String number;
    private int capacity;
    private int type;
    private int hotel_id;

    public Room(int id, String number, int capacity, int type, int hotel_id) {
        this.id = id;
        this.number = number;
        this.capacity = capacity;
        this.type = type;
        this.hotel_id = hotel_id;
    }

    public Room(String number, int capacity, int type, int hotel_id) {
        this.number = number;
        this.capacity = capacity;
        this.type = type;
        this.hotel_id = hotel_id;
    }

    public Room() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getHotel_id() {
        return hotel_id;
    }

    public void setHotel_id(int hotel_id) {
        this.hotel_id = hotel_id;
    }

    @Override
    public String toString() {
        return "Room [id=" + id + ", number=" + number + ", capacity=" + capacity + ", type=" + type + ", hotel_id="
                + hotel_id + "]";
    }

    @Override
    public boolean equals(Object obj) {
        Room room = (Room) obj;
        return (this.id == room.id);
    }

}
