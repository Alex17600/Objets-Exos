module fr.customgroup {
    requires transitive javafx.controls;
    requires javafx.fxml;
    requires com.google.gson;
    opens fr.customgroup to com.google.gson, javafx.fxml;
    exports fr.customgroup;
}
