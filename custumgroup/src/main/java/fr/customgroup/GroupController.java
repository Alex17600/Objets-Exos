package fr.customgroup;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import com.google.gson.Gson;

import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;

public class GroupController {

    @FXML
    private TableView<Stagiaire> tableViewResultStagiaire;
    @FXML
    private TableView<Stagiaire> tableViewStagiaire;
    @FXML
    private TextField nombreStagiaireGroupe;
    @FXML
    private TextField nbrGroup;
    @FXML
    private ListView<String> resultView1;
    @FXML
    private ListView<String> resultView2;
    @FXML
    private ListView<String> resultView3;
    @FXML
    private ListView<String> resultView4;
    @FXML
    private ListView<String> resultView5;
    @FXML
    private ListView<String> resultView6;
    @FXML
    private Button buttonAdd;
    @FXML
    private Button buttonAddAll;
    @FXML
    private Button buttonDelete;
    @FXML
    private Button buttonDeleteAll;
    @FXML
    private TableColumn<Stagiaire, String> nomColumn;
    @FXML
    private TableColumn<Stagiaire, String> prenomColumn;
    @FXML
    private TableColumn<Stagiaire, String> nomColumnResultat;
    @FXML
    private TableColumn<Stagiaire, String> prenomColumnResultat;
    @FXML
    private HBox hboxResult;

    private ObservableList<Stagiaire> stagiaires = FXCollections.observableArrayList();

    
    /** 
     * @throws IOException
     */
    @FXML
    public void initialize() throws IOException {
        Gson gson = new Gson();
        BufferedReader reader;
        try {
            reader = new BufferedReader(new FileReader("C:\\Users\\17010-27-06\\Desktop\\Projets JAVA\\Objets-Exos\\custumgroup\\src\\main\\resources\\fr\\customgroup\\listStagiaire.json"
                ));
            stagiaires.clear();
            stagiaires.addAll(Arrays.asList(gson.fromJson(reader, Stagiaire[].class)));
            nomColumn.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getNom()));
            prenomColumn.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getPrenom()));

            // Assigner l'ObservableList à la TableView
            tableViewStagiaire.getItems().addAll(stagiaires);

            reader.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void addStagiaireOne(ActionEvent event) {
        Stagiaire addStagiaire = tableViewStagiaire.getSelectionModel().getSelectedItem();
        if (addStagiaire != null) {
            nomColumnResultat.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getNom()));
            prenomColumnResultat
                    .setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getPrenom()));
            tableViewResultStagiaire.getItems().add(addStagiaire);

        }
    }

    @FXML
    public void addStagiaireAll(ActionEvent event) {
        ObservableList<Stagiaire> allStagiaires = tableViewStagiaire.getItems();
        tableViewResultStagiaire.getItems().addAll(allStagiaires);
        nomColumnResultat.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getNom()));
        prenomColumnResultat.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getPrenom()));
    }

    @FXML
    public void delete(ActionEvent event) {
        Stagiaire selectedPerson = tableViewResultStagiaire.getSelectionModel().getSelectedItem();
        if (selectedPerson != null) {
            tableViewResultStagiaire.getItems().remove(selectedPerson);
        }
    }

    @FXML
    public void deleteAll(ActionEvent event) {
        ObservableList<Stagiaire> allStagiaires = tableViewStagiaire.getItems();
        tableViewResultStagiaire.getItems().removeAll(allStagiaires);
        nomColumnResultat.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getNom()));
        prenomColumnResultat.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getPrenom()));
    }

    @FXML
    public void randomList(ActionEvent event) {
        int nStagiaireParGroupe, nGroupes;
        try {
            nStagiaireParGroupe = Integer.parseInt(nombreStagiaireGroupe.getText());
            nGroupes = Integer.parseInt(nbrGroup.getText());
        } catch (NumberFormatException e) {
            // Afficher un message d'erreur et sortir de la méthode
            // ou retourner une liste vide, selon le contexte
            return;
        }

        // Calculer le nombre total de stagiaires
        int nStagiaires = tableViewResultStagiaire.getItems().size();

        // Vérifier que les valeurs sont valides
        if (nStagiaires < nGroupes || nStagiaireParGroupe < 1) {
            return;
        }

        // Mélanger les indices des stagiaires
        List<Integer> indices = new ArrayList<>();
        for (int i = 0; i < nStagiaires; i++) {
            indices.add(i);
        }
        Collections.shuffle(indices);

        // Répartir les stagiaires dans les groupes
        List<List<Stagiaire>> groupes = new ArrayList<>();
        for (int i = 0; i < nGroupes; i++) {
            groupes.add(new ArrayList<>());
        }
        int compteur = 0;
        for (int i : indices) {
            groupes.get(compteur % nGroupes).add(tableViewResultStagiaire.getItems().get(i));
            if (groupes.get(compteur % nGroupes).size() == nStagiaireParGroupe) {
                compteur++;
            }
        }

        // Afficher les résultats dans les vues correspondantes
        List<ListView<String>> resultViews = Arrays.asList(resultView1, resultView2, resultView3, resultView4,
                resultView5, resultView6);

        // clear le dernier resultat
        for (ListView<String> resultView : resultViews) {
            resultView.getItems().clear();
        }
        for (int i = 0; i < nGroupes; i++) {
            ObservableList<String> items = FXCollections.observableArrayList();
            for (Stagiaire s : groupes.get(i)) {
                items.add(s.getNom() + " " + s.getPrenom());
            }
            resultViews.get(i).setItems(items);
        }
    }
}
