package fr.customgroup;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Stagiaire {
    private transient StringProperty nomProperty;
    private transient StringProperty prenomProperty;
    private transient StringProperty formationProperty;
    private transient StringProperty matriculeProperty;
    private String nom;
    private String prenom;
    private String formation;
    private String matricule;

    public Stagiaire(String nom, String prenom, String formation, String matricule) {
        this.nomProperty = new SimpleStringProperty(nom);
        this.prenomProperty = new SimpleStringProperty(prenom);
        this.formationProperty = new SimpleStringProperty(formation);
        this.matriculeProperty = new SimpleStringProperty(matricule);
        this.nom = nom;
        this.prenom = prenom;
        this.formation = formation;
        this.matricule = matricule;
    }

    
    /** 
     * @return String
     */
    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
        this.nomProperty = new SimpleStringProperty(nom);
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
        this.prenomProperty = new SimpleStringProperty(prenom);
    }

    public String getFormation() {
        return formation;
    }

    public void setFormation(String formation) {
        this.formation = formation;
        this.formationProperty = new SimpleStringProperty(formation);
    }

    public String getMatricule() {
        return matricule;
    }

    public void setMatricule(String matricule) {
        this.matricule = matricule;
        this.matriculeProperty = new SimpleStringProperty(matricule);
    }

    public StringProperty getNomProperty() {
        return nomProperty;
    }

    public void setNomProperty(StringProperty nom) {
        this.nomProperty = nom;
    }

    public StringProperty getPrenomProperty() {
        return prenomProperty;
    }

    public void setPrenomProperty(StringProperty prenom) {
        this.prenomProperty = prenom;
    }

    public StringProperty getFormationProperty() {
        return formationProperty;
    }

    public void setFormationProperty(StringProperty formation) {
        this.formationProperty = formation;
    }

    public StringProperty getMatriculeProperty() {
        return matriculeProperty;
    }

    public void setMatriculeProperty(StringProperty matricule) {
        this.matriculeProperty = matricule;
    }

}
