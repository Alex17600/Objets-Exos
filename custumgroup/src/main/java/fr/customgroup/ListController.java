package fr.customgroup;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;

public class ListController {
    @FXML
    private TextField nomTextField;
    @FXML
    private TextField prenomTextField;
    @FXML
    private TextField formationTextField;
    @FXML
    private TextField matriculeTextField;
    @FXML
    private Button stagiaireButton;
    @FXML
    private Button listeButton;
    @FXML
    private TableColumn<Stagiaire, String> nomColumn;
    @FXML
    private TableColumn<Stagiaire, String> prenomColumn;
    @FXML
    private TableColumn<Stagiaire, String> formationColumn;
    @FXML
    private TableColumn<Stagiaire, String> matriculeColumn;
    @FXML
    private BorderPane sceneGeneral;
    @FXML
    private Pane listStagiairePane;
    

    private ObservableList<Stagiaire> stagiaires = FXCollections.observableArrayList();
    @FXML
    private TableView<Stagiaire> stagiaireListView;

    @FXML
    private void initialize() {
        stagiaireListView.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                nomTextField.setText(newValue.getNom());
                prenomTextField.setText(newValue.getPrenom());
                formationTextField.setText(newValue.getFormation());
                matriculeTextField.setText(newValue.getMatricule());
            }
        });
    }

    
    /** 
     * @param event
     * @throws IOException
     */
    @FXML
    public void showStagiaire(ActionEvent event) throws IOException {
        sceneGeneral.setCenter(listStagiairePane);

        Gson gson = new Gson();
        BufferedReader reader;
        try {
            reader = new BufferedReader(new FileReader("C:\\Users\\17010-27-06\\Desktop\\Projets JAVA\\Objets-Exos\\custumgroup\\src\\main\\resources\\fr\\customgroup\\listStagiaire.json"
                ));
            stagiaires.clear();
            stagiaires.addAll(Arrays.asList(gson.fromJson(reader, Stagiaire[].class)));

            nomColumn.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getNom()));
            prenomColumn.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getPrenom()));
            formationColumn
                    .setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getFormation()));
            matriculeColumn
                    .setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getMatricule()));

            // Assigner l'ObservableList à la ListView
            stagiaireListView.setItems(stagiaires);

            reader.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void add(ActionEvent event) {

        String nom = nomTextField.getText();
        String prenom = prenomTextField.getText();
        String formation = formationTextField.getText();
        String matricule = matriculeTextField.getText();
        stagiaires.add(new Stagiaire(nom, prenom, formation, matricule));
        
    }

    @FXML
    public void update(ActionEvent event) {
        Stagiaire selectedStagiaire = stagiaireListView.getSelectionModel().getSelectedItem();
        if (selectedStagiaire != null) {
            selectedStagiaire.setNom(nomTextField.getText());
            selectedStagiaire.setPrenom(prenomTextField.getText());
            selectedStagiaire.setFormation(formationTextField.getText());
            selectedStagiaire.setMatricule(matriculeTextField.getText());
            stagiaireListView.refresh();
        }
    }

    @FXML
    public void cancel(ActionEvent event) {
        nomTextField.setText("");
        prenomTextField.setText("");
        formationTextField.setText("");
        matriculeTextField.setText("");
    }

    @FXML
    public void delete(ActionEvent event) {
        Stagiaire selectedPerson = stagiaireListView.getSelectionModel().getSelectedItem();
        if (selectedPerson != null) {
            stagiaireListView.getItems().remove(selectedPerson);
        }
        nomTextField.setStyle("");
        prenomTextField.setStyle("");
        formationTextField.setStyle("");
        matriculeTextField.setStyle("");
    }

    @FXML
    public void serializationJson() {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        // Sérialiser l'objet en JSON
        String json = gson.toJson(stagiaires);
        BufferedWriter writer;
        try {
            writer = new BufferedWriter(new FileWriter("C:\\Users\\17010-27-06\\Desktop\\Projets JAVA\\Objets-Exos\\custumgroup\\src\\main\\resources\\fr\\customgroup\\listStagiaire.json"
                ));
            writer.write(json);
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void changePaneToGroup(ActionEvent event) throws IOException {
        try {
            // Afficher le panGroupeStagiaire
            Pane panGroupeStagiaire = FXMLLoader.load(getClass().getResource("panListStagiaire.fxml"));
            sceneGeneral.setCenter(panGroupeStagiaire);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
