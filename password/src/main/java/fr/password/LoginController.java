package fr.password;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import org.apache.commons.codec.digest.DigestUtils;
import com.google.gson.Gson;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

public class LoginController extends Controller {
    @FXML
    private BorderPane accueil;
    @FXML
    private Pane paneTitleLogin;
    @FXML
    private TextField emailLogin;
    @FXML
    private PasswordField passwordLogin;
    @FXML
    private Label messageErreur;
    @FXML
    private Label title;
    @FXML
    private VBox vboxResult;

    private String compteJson = "C:\\Users\\17010-27-06\\Desktop\\Projets JAVA\\Objets-Exos\\password\\src\\main\\resources\\fr\\password\\compte.json";

    @FXML
    public void initialize() throws Exception {
        messageErreur.setVisible(false);
    }

    @FXML
    public void login(ActionEvent event) throws IOException {
        Gson gson = new Gson();
        BufferedReader reader;
    
        try {
            reader = new BufferedReader(new FileReader(compteJson));
    
            Compte compte = gson.fromJson(reader, Compte.class);
            String hashedPassword = DigestUtils.sha256Hex(passwordLogin.getText());
    
            if (compte.getEmail().equals(emailLogin.getText())
                    && compte.getPassword().equals(hashedPassword)) {
                messageErreur.setVisible(false);
                createController.changeView("vboxResult.fxml", "Gérez vos Mots de passe");
            } else {
                messageErreur.setText("Erreur de connexion! Vérifier votre email et votre mot de passe.");
                messageErreur.setVisible(true);
            }
    
            reader.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
