package fr.password;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class JsonDataSerialize {

    private static JsonDataSerialize instance;
    private final String fileName = "C:\\Users\\17010-27-06\\Desktop\\Projets JAVA\\Objets-Exos\\password\\src\\main\\resources\\fr\\password\\data.json";
    private Gson gson;
    public Object data;

    private JsonDataSerialize() {
        gson = new Gson();
    }

    public static JsonDataSerialize getInstance() {
        if (instance == null) {
            instance = new JsonDataSerialize();
        }
        return instance;
    }

    public List<Data> loadData() throws IOException {
        File file = new File(fileName);
        if (!file.exists()) {
            file.createNewFile();
            return new ArrayList<>();
        }
        Type type = new TypeToken<List<Data>>() {}.getType();
        List<Data> dataList;
        try (FileReader fileReader = new FileReader(file)) {
            dataList = gson.fromJson(fileReader, type);
        } catch (JsonSyntaxException | JsonIOException | IOException e) {
            e.printStackTrace();
            dataList = new ArrayList<>();
        }
        if (dataList == null) {
            dataList = new ArrayList<>();
        }
        return dataList;
    }

    public void saveData(List<Data> dataList) throws IOException {
        File file = new File(fileName);
        try (FileWriter fileWriter = new FileWriter(file)) {
            gson.toJson(dataList, fileWriter);
        }
    }

    public void addData(Data data) throws IOException {
        List<Data> dataList = loadData();
        dataList.add(data);
        saveData(dataList);
    }

    public void updateData(Data updatedData, int selectedIndex) throws IOException {
        List<Data> dataList = loadData();
        dataList.set(selectedIndex, updatedData);
        saveData(dataList);
    }

    public void removeData(int selectedIndex) throws IOException {
        List<Data> dataList = loadData();
        dataList.remove(selectedIndex);
        saveData(dataList);
    }

    public int findIndex(Data data) throws IOException {
        List<Data> dataList = loadData();
        return dataList.indexOf(data);
    }

}
