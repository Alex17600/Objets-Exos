package fr.password;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import org.apache.commons.codec.digest.DigestUtils;

public class CreateController {
    @FXML
    private BorderPane accueil;
    @FXML
    private Label messageErreur;
    @FXML
    private VBox loginBox;
    @FXML
    private TextField emailCreate;
    @FXML
    private TextField passwordCreate;
    @FXML
    private Label title;
    private String originalPassword = "";
    private String compteJson = "C:\\Users\\17010-27-06\\Desktop\\Projets JAVA\\Objets-Exos\\password\\src\\main\\resources\\fr\\password\\compte.json";

    @FXML
    public void initialize() {
        messageErreur.setVisible(false);
    }

    @FXML
    public void savePassword(ActionEvent event) {
        String text = passwordCreate.getText();
        originalPassword = text;
    }

    @FXML
    public void star(ActionEvent event) {
        String text = passwordCreate.getText();
        passwordCreate.setTextFormatter(null);
        StringBuilder maskedPassword = new StringBuilder();
        for (int i = 0; i < text.length(); i++) {
            maskedPassword.append("*");
        }
        passwordCreate.setText(maskedPassword.toString());
    }

    @FXML
    public void create(ActionEvent event) throws IOException {
        String hashedPassword = DigestUtils.sha256Hex(originalPassword); // hasher le mot de passe original
        Compte compte = new Compte(emailCreate.getText(), hashedPassword);

        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        String json = gson.toJson(compte);

        try (BufferedWriter writer = new BufferedWriter(new FileWriter(compteJson))) {
            writer.write(json);
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (!emailCreate.getText().isEmpty() && !hashedPassword.isEmpty()) {
            changeView("loginPage.fxml", "Connectez-vous");
        } else {
            messageErreur.setText("Veuillez remplir tous les champs");
            messageErreur.setStyle("-fx-text-fill: red");
            messageErreur.setVisible(true);
        }
    }

    // Une fonction qu'elle est vachement bien pratique!!
    public Controller changeView(String fxml, String title) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource(fxml));
        Pane view = (Pane) loader.load();
        Controller controller = loader.getController();
        controller.setMainController(this);
        accueil.setCenter(view);
        this.title.setText(title);
        return controller;
    }

    @FXML
    public void loginUser(ActionEvent event) throws IOException {
        changeView("loginPage.fxml", "Connectez-vous");
    }

}
