package fr.password;

public class Data {
    private String site;
    private String email;
    private String password;

    public Data(String site, String email, String password) {
        this.site = site;
        this.email = email;
        this.password = password;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
