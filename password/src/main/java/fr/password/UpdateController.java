package fr.password;

import java.io.IOException;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;

public class UpdateController extends Controller {
    @FXML
    private TextField siteUpdate;
    @FXML
    private TextField emailUpdate;
    @FXML
    private TextField passwordUpdate;
    private Data data;
    private int selectedIndex;

    @FXML
    public void modified() throws IOException {
        // Mettre à jour les données
        Data updatedData = new Data(siteUpdate.getText(), emailUpdate.getText(), passwordUpdate.getText());
        JsonDataSerialize jsonData = JsonDataSerialize.getInstance();
        jsonData.updateData(updatedData, selectedIndex);

        createController.changeView("vboxResult.fxml", "Modifications");

        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Information");
        alert.setHeaderText(null);
        alert.setContentText("Mise à jour effectuée !");
        alert.showAndWait();

    }

    public void setData(Data data) {
        this.data = data;
        siteUpdate.setText(data.getSite());
        emailUpdate.setText(data.getEmail());
        passwordUpdate.setText(data.getPassword());
    }

    public void returnToMain() throws IOException {
        createController.changeView("vboxResult.fxml", "Modifications");
    }

}
