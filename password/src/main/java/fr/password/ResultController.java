package fr.password;

import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;
import javafx.scene.layout.HBox;
import java.io.IOException;
import java.util.List;
import java.util.Random;

public class ResultController extends Controller {
    @FXML
    private TableColumn<Data, String> siteColumn;
    @FXML
    private TableColumn<Data, String> emailColumn;
    @FXML
    private TableColumn<Data, String> passwordColumn;
    @FXML
    private Label siteLabel;
    @FXML
    private Label emailLabel;
    @FXML
    private Label passwordLabel;
    @FXML
    private TextField siteTexteField;
    @FXML
    private TextField emailTextField;
    @FXML
    private TextField passwordTextField;
    @FXML
    private Button addConfirm;
    @FXML
    private HBox textFieldAdd;
    @FXML
    private Button mdpView;
    @FXML
    private Button copie;
    @FXML
    private Button update;
    @FXML
    private TableView<Data> tableView;
    @FXML
    private TextField searchField;
    private ObservableList<Data> data = FXCollections.observableArrayList();

   
    public ResultController() {
        try {
            data = FXCollections.observableArrayList(JsonDataSerialize.getInstance().loadData());
        } catch (IOException e) {
            Alert alert = new Alert(Alert.AlertType.ERROR, "Un erreur s'est produite lors du chargement");
            alert.showAndWait();
            e.printStackTrace();
        }
    }

    public void initialize() {
        textFieldAdd.setVisible(false);
        siteLabel.setVisible(false);
        emailLabel.setVisible(false);
        passwordLabel.setVisible(false);
        siteTexteField.setVisible(false);
        emailTextField.setVisible(false);
        passwordTextField.setVisible(false);
        addConfirm.setVisible(false);

        //filtrer recherche
        FilteredList<Data> filteredData = new FilteredList<>(data, p -> true);
        searchField.textProperty().addListener((observable, oldValue, newValue) -> {
            filteredData.setPredicate(data -> data.getSite().toLowerCase().contains(newValue.toLowerCase()));
        });
        siteColumn.setCellFactory(TextFieldTableCell.<Data> forTableColumn());
        siteColumn.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getSite()));
        emailColumn.setCellFactory(TextFieldTableCell.<Data> forTableColumn());
        emailColumn.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getEmail()));
        passwordColumn.setCellFactory(TextFieldTableCell.<Data> forTableColumn());
        passwordColumn.setCellValueFactory(
                cellData -> new SimpleStringProperty("*".repeat(cellData.getValue().getPassword().length())));

            tableView.setItems(filteredData);
}

    @FXML
    public void viewMDP() throws IOException {
        try {
            String password = tableView.getSelectionModel().getSelectedItem().getPassword();
            Alert alert = new Alert(AlertType.INFORMATION);
            alert.setTitle("Mot de passe");
            alert.setHeaderText(null);
            alert.setContentText("Le mot de passe est : " + password);
            alert.showAndWait();

        } catch (Exception e) {

        }
    }

    @FXML
    public void copieMDP() throws IOException {
        try {
            String password = tableView.getSelectionModel().getSelectedItem().getPassword();
            Clipboard clipboardObj = Clipboard.getSystemClipboard();
            ClipboardContent content = new ClipboardContent();
            content.putString(password);
            clipboardObj.setContent(content);

            Alert alert = new Alert(AlertType.INFORMATION);
            alert.setTitle("Mot de passe copié");
            alert.setHeaderText(null);
            alert.setContentText("Le mot de passe a été copié dans le presse-papier");
            alert.showAndWait();

        } catch (Exception e) {

        }
    }

    @FXML
    public void updateTable() throws IOException {
        // Get the selected data
        Data passwordData = tableView.getSelectionModel().getSelectedItem();
        if (passwordData == null) {
            Alert alert = new Alert(AlertType.WARNING);
            alert.setTitle("Attention");
            alert.setHeaderText("Aucun site sélectionné");
            alert.setContentText("Veuillez sélectionner un site à modifier");
            alert.showAndWait();
            return;
        }

        // Get the instance of JsonDataSerialize
        JsonDataSerialize jsonData = JsonDataSerialize.getInstance();

        // Read the data from the file
        jsonData.loadData();

        UpdateController updateController = (UpdateController) createController.changeView("update.fxml", "Modifications");
        updateController.setData(passwordData);
    }

    @FXML
    public void showAdd() throws IOException {
        textFieldAdd.setVisible(true);
        siteLabel.setVisible(true);
        emailLabel.setVisible(true);
        passwordLabel.setVisible(true);
        siteTexteField.setVisible(true);
        emailTextField.setVisible(true);
        passwordTextField.setVisible(true);
        addConfirm.setVisible(true);
    }

    @FXML
    private void add() throws IOException {
        String site = siteTexteField.getText();
        String email = emailTextField.getText();
        String password = passwordTextField.getText();

        // Vérifier que tous les champs sont remplis
        if (site.isEmpty() || email.isEmpty() || password.isEmpty()) {
            Alert alert = new Alert(AlertType.WARNING);
            alert.setTitle("Attention");
            alert.setHeaderText("Champs manquants");
            alert.setContentText("Veuillez remplir tous les champs.");
            alert.showAndWait();
            return;
        }

        // Vérifier que le mot de passe contient au moins une lettre majuscule
        if (!password.matches(".*[A-Z].*")) {
            Alert alert = new Alert(AlertType.WARNING);
            alert.setTitle("Attention");
            alert.setHeaderText("Erreur mot de passe");
            alert.setContentText("Le mot de passe doit contenir au moins une lettre majuscule.");
            alert.showAndWait();
            return;
        }

        // Vérifier que le mot de passe contient au moins une lettre minuscule
        if (!password.matches(".*[a-z].*")) {
            Alert alert = new Alert(AlertType.WARNING);
            alert.setTitle("Attention");
            alert.setHeaderText("Erreur mot de passe");
            alert.setContentText("Le mot de passe doit contenir au moins une lettre minuscule.");
            alert.showAndWait();
            return;
        }

        // Vérifier que le mot de passe contient au moins un chiffre
        if (!password.matches(".*\\d.*")) {
            Alert alert = new Alert(AlertType.WARNING);
            alert.setTitle("Attention");
            alert.setHeaderText("Erreur mot de passe");
            alert.setContentText("Le mot de passe doit contenir au moins un chiffre.");
            alert.showAndWait();
            return;
        }

        // Vérifier que le mot de passe contient au moins un caractère spécial
        if (!password.matches(".*[@#$%^&+=!*].*")) {
            Alert alert = new Alert(AlertType.WARNING);
            alert.setTitle("Attention");
            alert.setHeaderText("Erreur mot de passe");
            alert.setContentText("Le mot de passe doit contenir au moins un caractère spécial (@#$%^&+=!).");
            alert.showAndWait();
            return;
        }

        // Vérifier que le mot de passe a une longueur comprise entre 5 et 40 caractères
        if (password.length() < 5 || password.length() > 40) {
            Alert alert = new Alert(AlertType.WARNING);
            alert.setTitle("Attention");
            alert.setHeaderText("Erreur mot de passe");
            alert.setContentText("Le mot de passe doit avoir une longueur comprise entre 5 et 40 caractères.");
            alert.showAndWait();
            return;
        } else {
            textFieldAdd.setVisible(false);
            siteLabel.setVisible(false);
            emailLabel.setVisible(false);
            passwordLabel.setVisible(false);
            siteTexteField.setVisible(false);
            emailTextField.setVisible(false);
            passwordTextField.setVisible(false);
            addConfirm.setVisible(false);

            // Ajouter les données
            Data newData = new Data(site, email, password);
            JsonDataSerialize jsonData = JsonDataSerialize.getInstance();
            List<Data> dataList = jsonData.loadData();
            dataList.add(newData);
            // Sauvegarder les données mises à jour dans le fichier JSON
            jsonData.saveData(dataList);

            data.clear();
            data = FXCollections.observableArrayList(JsonDataSerialize.getInstance().loadData());
            siteColumn.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getSite()));
            emailColumn.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getEmail()));
            passwordColumn.setCellValueFactory(
                    cellData -> new SimpleStringProperty("*".repeat(cellData.getValue().getPassword().length())));

            tableView.setItems(data);

        }
    }

    @FXML
    private void delete() throws IOException {
        int selectedItem = tableView.getSelectionModel().getSelectedIndex();
        JsonDataSerialize jsonData = JsonDataSerialize.getInstance();
        List<Data> dataList = jsonData.loadData();
        dataList.remove(selectedItem);
        jsonData.saveData(dataList);
        tableView.refresh();
    }

    @FXML
    private String randomPasswords() {
        String upperCaseLetters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        String lowerCaseLetters = "abcdefghijklmnopqrstuvwxyz";
        String digits = "0123456789";
        String specialCharacters = "@#$%^&+=!*";
        String combinedChars = upperCaseLetters + lowerCaseLetters + digits + specialCharacters;
    
        Random random = new Random();
        int passwordLength = random.nextInt(35) + 5; 
        StringBuilder password = new StringBuilder();
    
        for (int i = 0; i < passwordLength; i++) {
            password.append(combinedChars.charAt(random.nextInt(combinedChars.length())));
        }
    
        String passwordStr = password.toString();
    
        if (!passwordStr.matches(".*[A-Z].*")) {
            return randomPasswords();
        }
    
        if (!passwordStr.matches(".*[a-z].*")) {
            return randomPasswords();
        }
    
        if (!passwordStr.matches(".*\\d.*")) {
            return randomPasswords();
        }
    
        if (!passwordStr.matches(".*[@#$%^&+=!*].*")) {
            return randomPasswords();
        }
    
        passwordTextField.setText(passwordStr);
        return passwordStr;
    }


}
