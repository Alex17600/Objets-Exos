package fr.password;

import org.jasypt.util.password.StrongPasswordEncryptor; 

public class PasswordBcrypt {
    
    public static String encryptPassword(String password) {
        StrongPasswordEncryptor passwordEncryptor = new StrongPasswordEncryptor();
        String encryptedPassword = passwordEncryptor.encryptPassword(password);
        return encryptedPassword;
    }
    
    public static boolean checkPassword(String inputPassword, String encryptedPassword) {
        StrongPasswordEncryptor passwordEncryptor = new StrongPasswordEncryptor();
        boolean isPasswordMatched = passwordEncryptor.checkPassword(inputPassword, encryptedPassword);
        return isPasswordMatched;
    }
    
}
