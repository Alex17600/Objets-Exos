module fr.password {
    requires transitive javafx.controls;
    requires javafx.fxml;
    requires com.google.gson;
    requires jasypt;
    requires org.apache.commons.codec;
    opens fr.password to com.google.gson, javafx.fxml;
    exports fr.password;
}
