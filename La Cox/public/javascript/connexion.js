const email = document.querySelector('#email-connect');
const password = document.querySelector('#mdp-connect');

document.querySelector('form').addEventListener('submit', function(event) {
  event.preventDefault(); // Empêche le rechargement de la page lors de la soumission du formulaire

  // Récupérer les valeurs des champs email et mot de passe saisis par l'utilisateur
  const userEmail = email.value;
  const userPassword = password.value;

  // Effectuer la requête Fetch API pour vérifier les informations d'identification de l'utilisateur
  fetch('/connexion', {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({ email: userEmail, password: userPassword })
  })
  .then(response => response.json())
  .then(data => {
    if (data.success) {
      // L'utilisateur est valide, redirigez-le vers la page souhaitée
      window.location.href = '/mon-compte.html';
    } else {
      // Les informations d'identification de l'utilisateur sont incorrectes
      alert('Identifiants invalides. Veuillez réessayer.');
    }
  })
  .catch(error => {
    console.error('Une erreur s\'est produite:', error); 
  });
});


