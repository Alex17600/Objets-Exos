document.addEventListener('DOMContentLoaded', function() {
    const imageModal = document.querySelector('.image-modal');
    const modal = document.querySelector('.modal');
  
    imageModal.addEventListener('click', function() {
      modal.style.display = 'flex';
    });
  
    modal.addEventListener('click', function(event) {
      if (event.target === modal) {
        modal.style.display = 'none';
      }
    });
  });
