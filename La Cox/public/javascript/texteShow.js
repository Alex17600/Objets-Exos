const textClub = document.getElementById('textPresentation');
const textNavette = document.getElementById('texteNavette');
const textAgenda = document.getElementById('textAgenda');

fetch('http://localhost:8000/textes', {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json'
    }
  })
  .then(response => {
    if (!response.ok) {
      throw new Error('Erreur lors de la requête');
    }
    return response.json();
  })
  .then(data => {
    textClub.innerHTML = data[2].contenu;
    if (textNavette !== null && textAgenda !== null) {
      textNavette.innerHTML = data[0].contenu;
      textAgenda.innerHTML = data[1].contenu;
    }
    console.log(data);
  })
  .catch(error => {
    console.error(error);
  });