document.addEventListener('DOMContentLoaded', function () {
    const checkBox = document.querySelector('#navCheck');
    const burger = document.querySelector('.burger');
    const closeButton = burger.querySelector('.close');

    checkBox.addEventListener('click', function () {
        if (checkBox.checked) {
            burger.style.left = 0;
        }
    });

    closeButton.addEventListener('click', function () {
        burger.style.left = '-400px';
        checkBox.checked = false;
    });

    // Code pour détecter la taille de l'écran et rediriger le lien de connexion
    const connexionLinks = document.querySelectorAll('.connexion-link');

    connexionLinks.forEach(function(connexionLink) {
        connexionLink.addEventListener('click', function(event) {
            event.preventDefault();
    
            if (window.innerWidth > 768) {
                document.getElementById('popup').style.display = 'block';
            } else {
                window.location.href = 'connexion.html';
            }
        });
    });
});