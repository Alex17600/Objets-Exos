const toggleProfile = document.getElementById('toggle-profile');
const profileInfo = document.getElementById('profile-info');
const toggleAddress = document.getElementById('toggle-address');
const profileAddress = document.getElementById('profile-adress');

toggleProfile.addEventListener('click', function() {
  if (profileInfo.style.maxHeight) {
    profileInfo.style.maxHeight = null;
    profileInfo.style.opacity = '0';
    toggleProfile.classList.remove('rotate');
  } else {
    profileInfo.style.maxHeight = profileInfo.scrollHeight + 'px';
    profileInfo.style.opacity = '1';
    toggleProfile.classList.add('rotate');
  }
});

toggleAddress.addEventListener('click', function() {
  if (profileAddress.style.maxHeight) {
    profileAddress.style.maxHeight = null;
    profileAddress.style.opacity = '0';
    toggleAddress.classList.remove('rotate');
  } else {
    profileAddress.style.maxHeight = profileAddress.scrollHeight + 'px';
    profileAddress.style.opacity = '1';
    toggleAddress.classList.add('rotate');
  }
});

