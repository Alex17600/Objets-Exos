const tbody = document.querySelector('tbody');
let data;

fetch('http://localhost:8000/utilisateurs', {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json'
    }
  })
  .then(response => {
    if (!response.ok) {
      throw new Error('Erreur lors de la requête');
    }
    return response.json();
  })
  .then(responseData => {
    data = responseData;
    console.log(data);
    data.forEach(utilisateur => {
      const newRow = tbody.insertRow();
      const idPageCell = newRow.insertCell();
      const nomCell = newRow.insertCell();
      const contenuCell = newRow.insertCell();
      const buttonCell = newRow.insertCell();

      idPageCell.textContent = utilisateur.nom;
      nomCell.textContent = utilisateur.prenom;
      contenuCell.textContent = utilisateur.email;

      const button = document.createElement('button');
      button.setAttribute('data-id', utilisateur.id);
      button.textContent = "Supprimer";
      button.classList.add("btn-supprimer");
      buttonCell.appendChild(button);

      button.addEventListener('click', () => {
        const userId = utilisateur.id;
        supprimerUtilisateur(userId);
      });
    });

  })
  .catch(error => {
    console.error(error);
  });

function supprimerUtilisateur(userId) {
  fetch('http://localhost:8000/utilisateurs/' + userId, {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json'
      }
    })
    .then(response => {
      if (!response.ok) {
        throw new Error('Erreur lors de la suppression de l\'utilisateur');
      }
      // Suppression réussie, vous pouvez mettre à jour l'interface utilisateur en conséquence
      const utilisateurSupprimer = document.querySelector('button[data-id="' + userId + '"]');
      const ligneSupprimer = utilisateurSupprimer.parentNode.parentNode;
      tbody.removeChild(ligneSupprimer);
      window.location.reload();
    })
    .catch(error => {
      console.error(error);
    });
}