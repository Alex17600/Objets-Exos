function scrollHaut() {
  window.scrollTo({ top: 0, behavior: "smooth" });
}

window.addEventListener("scroll", function () {

  const btnScroll = document.getElementById("btn-scroll");
  if (window.pageYOffset > 800) {
    btnScroll.classList.add("visible");
  } else {
    btnScroll.classList.remove("visible");
  }
});
