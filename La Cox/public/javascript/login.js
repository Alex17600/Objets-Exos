window.addEventListener('DOMContentLoaded', function() {
  const popup = document.getElementById('popup');
  const closeButton = document.querySelector('.close-btn');

  closeButton.addEventListener('click', function() {
    popup.style.display = 'none';
  }); 
  
});
