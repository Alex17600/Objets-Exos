const tbody = document.querySelector('tbody');
const select = document.getElementById('page');
const titreInput = document.getElementById('titre');
const contenuTextarea = document.getElementById('contenuTexte');
const formUpdate = document.getElementById('modifTexte');

let data;

fetch('http://localhost:8000/textes', {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json'
    }
  })
  .then(response => {
    if (!response.ok) {
      throw new Error('Erreur lors de la requête');
    }
    return response.json();
  })
  .then(responseData => {
    data = responseData; // Affecter les données récupérées à la variable data

    // Remplissage dynamique du select
    const defaultOption = document.createElement('option');
    defaultOption.value = '';
    defaultOption.textContent = '';
    select.appendChild(defaultOption);

    data.forEach(texte => {
      const option = document.createElement('option');
      option.value = texte.page;
      option.textContent = texte.page;
      select.appendChild(option);
    });

    // Remplissage tableau DOM
    data.forEach(texte => {
      const newRow = tbody.insertRow();
      const idPageCell = newRow.insertCell();
      const nomCell = newRow.insertCell();
      const contenuCell = newRow.insertCell();

      idPageCell.textContent = texte.page;
      nomCell.textContent = texte.nom;
      contenuCell.textContent = texte.contenu;
    });

    select.addEventListener('change', (event) => {
      const selectedTexte = data.find(texte => texte.page === event.target.value); // Trouver l'objet correspondant à la valeur sélectionnée
      const selectedValue = selectedTexte.id; // Récupérer l'ID associé

      titreInput.value = selectedTexte.nom;
      contenuTextarea.value = selectedTexte.contenu;
    });

    formUpdate.addEventListener('submit', (event) => {
      event.preventDefault();

      const selectedTexte = data.find(texte => texte.page === select.value); // Trouver l'objet correspondant à la valeur sélectionnée
      const selectedValue = selectedTexte.id; // Récupérer l'ID associé

      const updatedTexte = {
        nom: titreInput.value,
        contenu: contenuTextarea.value,
        page: select.value
      };

      fetch('http://localhost:8000/textes/' + selectedValue, {
          method: 'PUT',
          headers: {
            'Content-Type': 'application/json'
          },
          body: JSON.stringify(updatedTexte)
        })
        .then(response => {
          if (!response.ok) {
            throw new Error('Erreur lors de la requête');
          }
          return response.json();
        })
        .then(data => {
          titreInput.value = data.nom;
          contenuTextarea.value = data.contenu;
        })
        .catch(error => {
          console.error(error);
        });

        alert('Mise à jour effectuer');
        window.location.reload();
    });
  })
  .catch(error => {
    console.error(error);
  });