import React from 'react';
import HeaderAdmin from '../components/admin/HeaderAdmin.js';
import Main from '../components/admin/utilisateurs/Main';


const UtilisateurAdmin = () => {
    return (
        <div>
            <HeaderAdmin />
            <Main />
        </div>
    );
};

export default UtilisateurAdmin;