import React from "react";

const MonCompte = () => {
  return (
    <div>
      <div class="compte-mobile">
        <div class="container-mobile">
          <div class="img-profile">
            <img src="public/assets/images/icon/avatar.svg" alt="profile" />
            <div class="name">John Doe</div>
          </div>
        </div>

        <div class="item">
          <img
            src="public/assets/images/icon/Person.svg"
            alt="icone personne"
          />
          <span>Mon Profil</span>
          <div class="icon">
            <img
              src="public/assets/images/icon/arrow-down.svg"
              alt="plus"
              id="toggle-profile"
            />
          </div>
        </div>
        <div id="profile-info">
          <span>Nom</span>
          <span>Prenom</span>
        </div>
        <div class="item">
          <img
            src="public/assets/images/icon/address.svg"
            alt="icone adresse"
          />
          <span>Mon Adresse</span>
          <div class="icon">
            <img
              src="public/assets/images/icon/arrow-down.svg"
              alt="plus"
              id="toggle-address"
            />
          </div>
        </div>
        <div id="profile-adress">
          <span>Adresse du compte</span>
        </div>
        <div class="item-logout">
          <img
            src="public/assets/images/icon/logout.svg"
            alt="icone personne"
          />
          <span>Déconnexion</span>
        </div>
      </div>
    </div>
  );
};

export default MonCompte;
