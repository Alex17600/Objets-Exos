import React from 'react';
import HeaderAdmin from '../components/admin/HeaderAdmin';
import Telecharge from '../components/admin/photo/Telecharge';

const PhotoAdmin = () => {
    return (
        <div>
            <HeaderAdmin />
            <Telecharge />
        </div>
    );
};

export default PhotoAdmin;