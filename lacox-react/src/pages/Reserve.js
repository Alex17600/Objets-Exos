import React from 'react';
import Header from '../components/Header';
import Footer from '../components/Footer';
import PresentationSoiree from '../components/PresentationSoiree';

const Reserve = () => {
    return (
        <div>
            <Header />
            <PresentationSoiree />
            <Footer />
        </div>
    );
};

export default Reserve;