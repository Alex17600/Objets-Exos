import React from 'react';
import HeaderAdmin from '../components/admin/HeaderAdmin';
import Main from '../components/admin/texte/Main';

const TexteAdmin = () => {
    return (
        <div>
            <HeaderAdmin />
            <Main />
        </div>
    );
};

export default TexteAdmin;