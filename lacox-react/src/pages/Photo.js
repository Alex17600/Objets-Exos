import React from 'react';
import Header from '../components/Header';
import PopupConnexion from '../components/PopupConnexion';
import Presentation from '../components/Presentation';
import Footer from '../components/Footer';
import Album from '../components/Album';
import Scroll from '../components/Scroll';

const Photo = () => {
    return (
        <div>
            <Header />
            <Presentation />
            <Album />
            <Footer />
            <Scroll />
        </div>
    );
};

export default Photo;