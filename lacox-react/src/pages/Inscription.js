import React from 'react';
import Header from '../components/Header';
import FormInscription from '../components/FormInscription';
import Footer from '../components/Footer';


const Inscription = () => {
    return (
        <div className="inscription">
            <Header />
            <h1 className='h1Insctiption'>Inscrivez-vous</h1>
            <FormInscription />
            <Footer />
        </div>
    );
};

export default Inscription;