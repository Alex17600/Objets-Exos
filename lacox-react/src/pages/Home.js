import React from 'react';
import '../styles/style.scss';
import Header from '../components/Header';
import Presentation from '../components/Presentation';
import Agenda from '../components/Agenda';
import Media from '../components/Media';
import WeekEnd from '../components/WeekEnd';
import Footer from '../components/Footer';
import Scroll from '../components/Scroll';
import Navette from '../components/Navette';


const Home = () => {
    return (
        <div>
            <Header />
            <Presentation />
            <Agenda />
            <Media />
            <WeekEnd />
            <Navette />
            <Footer />
            <Scroll />
        </div>
    );
};

export default Home;