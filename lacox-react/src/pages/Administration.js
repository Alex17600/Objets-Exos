import React from 'react';
import HeaderAdmin from '../components/admin/HeaderAdmin';
import TexteAccueilAdmin from '../components/admin/TexteAccueilAdmin';


const Administration = () => {


    return (
        <div>
            <HeaderAdmin />
            <TexteAccueilAdmin />  
        </div>
    );
};

export default Administration;