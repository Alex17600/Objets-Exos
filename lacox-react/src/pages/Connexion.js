import { useState, useEffect } from "react";
import Header from "../components/Header";
import PopupConnexion from "../components/PopupConnexion";
import { NavLink, useNavigate } from "react-router-dom";
import jwtDecode from "jwt-decode";



const Connexion = () => {
  const [email, setEmail] = useState("");
  const [mdp, setMDP] = useState("");
  const navigate = useNavigate();
  const [display, setDisplay] = useState();

  const handleSetMdp = (mdp) => {
    setMDP(mdp);
  }
  
  const handleSetEmail = (email) => {
    setEmail(email);
  }

  const handleLogin = async (e) => {
    e.preventDefault();

    try {
      // Effectue la requête de connexion
      const response = await fetch("http://localhost:8000/connexion", {
        method: "POST",
        body: JSON.stringify({ email, mdp }),
      });

      // Vérifie la réponse de la requête
      if (response.ok && response.status === 200) {
        localStorage.setItem("token", response.headers.get("Access_token"));
        const token = localStorage.getItem("token");
        const tokenDecode = jwtDecode(token);
        const role = tokenDecode.roles;
        console.log(role);
        if (role[0] === "ROLE_ADMIN") {
          navigate("/administration");
        }
      } else {
        console.log("Échec de la connexion !");
      }
    } catch (error) {
      console.error(error);
    }
  };
  
  //affiche la popup au dessus de 768px
  const handleResize = () => {
    const screenWidth = window.innerWidth;
    setDisplay(screenWidth > 768); 
  };

  useEffect(() => {
    handleResize(); 
    window.addEventListener("resize", handleResize); 

    return () => {
      window.removeEventListener("resize", handleResize);
    };

  }, []);
  
  return (
    <div>
      <Header />
      <div className="connexion">

        <h1 className="h1Connect-mobile">Connectez-vous</h1>
        <div className="form-connexion">
          <form className="form-connect-mobile" onSubmit={handleLogin}>
            <label htmlFor="email-connect">Email</label>
            <input
              id="email-connect"
              type="email"
              placeholder="Veuillez entrer votre email"
              autoComplete="email"
              value={email}
              onChange={(e) => handleSetEmail(e.target.value)}
            />
            <label htmlFor="mdp-connect">Mot de passe</label>
            <input
              id="mdp-connect"
              type="password"
              placeholder="Mot de passe"
              autoComplete="current-password"
              value={mdp}
              onChange={(e) => handleSetMdp(e.target.value)}
            />
            <button type="submit">Valider</button>
          </form>
        </div>
  
        <div className="inscript">
          <NavLink to="/inscription">
            <input
              className="no-compte"
              type="submit"
              value="Pas encore de compte?"
            />
          </NavLink>
        </div>
        
        {display && (
        <PopupConnexion handleChangeMDP={handleSetMdp} handleChangeEmail={handleSetEmail} handleSubmit={handleLogin}/>
        )};
      </div>
    </div>
  );
};

export default Connexion;
