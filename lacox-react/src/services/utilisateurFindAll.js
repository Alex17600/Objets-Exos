export const fetchAllUsers = async () => {
  const token = localStorage.getItem("token");
  const headers = {
    "Content-Type": "application/json",
    Authorization: `Bearer ${token}`,
  };

  try {

    const response = await fetch('http://localhost:8000/utilisateurs', {
      method: 'GET',
      headers: headers
    });

    if (!response.ok) {
      throw new Error('Erreur lors de la requête');
    }

    const responseData = await response.json();
    return responseData;
  } catch (error) {
    throw new Error('Erreur lors de la récupération des utilisateurs');
  }
};

export const deleteUser = async (userId) => {
  try {
    const response = await fetch(`http://localhost:8000/utilisateurs/${userId}`, {
      method: 'DELETE',
      headers
    });

    if (!response.ok) {
      throw new Error('Erreur lors de la suppression de l\'utilisateur');
    }

    // Suppression réussie, pas besoin de retourner de données
  } catch (error) {
    throw new Error('Erreur lors de la suppression de l\'utilisateur');
  }
};
