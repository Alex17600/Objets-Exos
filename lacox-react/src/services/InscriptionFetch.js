const InscriptionFetch = async (url, data) => {
    try {
    const response = await fetch(url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    });
    const responseData = await response.json();
    return responseData;
  } catch (error) {
    console.error('Erreur:', error);
    throw error;
  }
  };
  
  export default InscriptionFetch;