const fetchTexte = (id) => {
  return fetch(`http://localhost:8000/textes/${id}`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json'
    }
  })
    .then(response => {
      if (!response.ok) {
        throw new Error('Erreur lors de la requête');
      }
      return response.json();
    })
    .then(data => {
      return data.contenu;
    })
    .catch(error => {
      console.error(error);
      throw new Error('Erreur lors de la récupération du texte');
    });
};

export default fetchTexte;
  