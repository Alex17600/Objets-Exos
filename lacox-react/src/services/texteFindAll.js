let data;

const fetchAll = async () => {
  return await fetch("http://localhost:8000/textes", {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
    },
  })
    .then((response) => {
      if (!response.ok) {
        throw new Error("Erreur lors de la requête");
      }
      return response.json();
    })
    .then((responseData) => {
      return (data = responseData);
    });
};

export default fetchAll;
