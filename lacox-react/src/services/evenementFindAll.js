
const fetchAllEvents = async () => {
    try {
        const response = await fetch('http://localhost:8000/evenements', {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        });

        if (!response.ok) {
            throw new Error('Erreur lors de la requête');
        }

        const responseData = await response.json();
        return responseData;
    } catch (error) {
        throw new Error('Erreur lors de la récupération des utilisateurs');
    }
};

export default fetchAllEvents;