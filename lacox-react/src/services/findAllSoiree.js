const findAllSoiree = async () => {

    return await fetch("http://localhost:8000/evenements/type/soiree", {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json'
      }
    })
      .then((response) => {
        if (!response.ok) {
          throw new Error("Erreur lors de la requête");
        }
        return response.json();
      })

      .catch(error => {
        console.error(error);
        throw new Error('Erreur lors de la récupération des soirées');
      });
  };
  
  export default findAllSoiree;