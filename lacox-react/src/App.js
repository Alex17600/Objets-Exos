import React from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import Home from './pages/Home';
import Photo from './pages/Photo';
import Connexion from './pages/Connexion';
import Contact from './pages/Contact';
import Inscription from './pages/Inscription';
import FilterAuth from './components/FilterAuth';
import AdminRouter from './components/AdminRouter';
import Reserve from './pages/Reserve';

const App = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/photos" element={<Photo />} />
        <Route path="/connexion" element={<Connexion />} />
        <Route path="/administration/*" element={<FilterAuth>
          <AdminRouter />
        </FilterAuth>} >
        </Route>
             
        <Route path="/reserve" element={<Reserve />} />
        <Route path="/contact" element={<Contact />} />
        <Route path="/inscription" element={<Inscription/>} />
      </Routes>
    </BrowserRouter>
  );
};

export default App;
