import React, { useEffect, useState } from "react";
import AgendaImage from "../assets/images/planning-soiree.png";
import fetchTexte from "../services/texteService";

const Agenda = () => {
  const [texteAgenda, setTexteNavette] = useState("");

  useEffect(() => {
    // Fonction asynchrone pour récupérer le texte
    const getTexteNavette = async () => {
      try {
        const texte = await fetchTexte(3); // Récupère le texte avec l'ID 2
        setTexteNavette(texte);
      } catch (error) {
        console.error(error);
        // Gérer l'erreur de récupération du texte
      }
    };

    getTexteNavette();
  }, []); // Le tableau de dépendances vide assure que l'effet s'exécute une seule fois


  return (
    <div>
      <h3>Agenda</h3>
      <div className="agenda">
        <img
          src={AgendaImage}
          alt="agenda des soirées"
          className="image-modal"
        />
        <div className="modal">
          <img
            src="public/assets/images/planning-soiree.png"
            alt="agenda des soirées"
            className="modal-content"
          />
        </div>
        <span className="vertical-line">
          <h3>Agenda Texte</h3>
          <p id="textAgenda">
            {texteAgenda}
            </p>
        </span>
      </div>
      <hr className="hrBase" />
    </div>
  );
};

export default Agenda;
