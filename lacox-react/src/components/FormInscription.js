import React from "react";
import InscriptionFetch from "../services/InscriptionFetch";

const FormInscription = () => {
    const handleSubmit = (event) => {
        event.preventDefault();
    
        const formData = new FormData(event.target);
        const formValues = Object.fromEntries(formData.entries());
    
        InscriptionFetch('http://localhost:8000/utilisateurs', formValues)
          .then((data) => {
            
            console.log(data);
          })
          .catch((error) => {
            
            console.error('Erreur:', error);
          });
      };
      
  return (
    <div>
      <form className="formInscription" onSubmit={handleSubmit}>
        <label htmlFor="nom">Nom</label>
        <input type="text" name="nom" />
        <label htmlFor="prenom">Prenom</label>
        <input type="text" name="prenom" />
        <label htmlFor="email">Email</label>
        <input type="email" name="email" />
        <label htmlFor="adresse">Adresse</label>
        <input type="text" name="adresse" />
        <label htmlFor="tel">Téléphone</label>
        <input type="tel" name="tel" />
        <label htmlFor="mdp">Mot de passe</label>
        <input type="password" name="mdp" />
        <button type="submit">Envoyer</button>
      </form>
    </div>
  );
};

export default FormInscription;
