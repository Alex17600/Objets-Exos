import React from "react";
import { useState, useEffect } from "react";
import { NavLink, useNavigate } from "react-router-dom";

const PopupConnexion = ({handleChangeMDP, handleChangeEmail, handleSubmit}) => {
  const navigate = useNavigate();

  const handleClick = () => {
    navigate("/");
  }
  
  return (
    <div id="popup" className="popup">
      <div className="popup-content">
      <span className="close-btn" onClick={handleClick}>&times;</span>
        <form className="popup-form" onSubmit={handleSubmit}>
        <h2>Connexion</h2>
          <input type="email" name="email" placeholder="Entrez votre Email" onChange={(e)=> handleChangeEmail(e.target.value)}/>
          <input type="password" placeholder="Mot de passe" onChange={(e)=> handleChangeMDP(e.target.value)}/>
          <button type="submit" className="login-button" >
            Se connecter
          </button>
          <p>
            <NavLink to="/inscription">
              Pas encore de compte? Rejoins-nous ici
            </NavLink>
          </p>
        </form>
      </div>
    </div>
  );
};

export default PopupConnexion;
