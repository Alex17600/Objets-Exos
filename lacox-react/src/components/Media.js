import React from 'react';
import TestPhoto from '../assets/images/test_photo.jpg';

const Media = () => {
    return (
        <div className="medias">
            <div className="photos">
                <h3>Les Photos</h3>
                <img src={TestPhoto} alt="photo test" />
                <p className="dateAjout">Date de l'ajout</p>
            </div>
            <div className="videos">
                <h3>Les vidéos</h3>
                <img src={TestPhoto} alt="video test" />
                <p className="dateAjout">Date de l'ajout</p>
            </div>
            <div className="actus">
                <h3>L'Actu</h3>
                <img src={TestPhoto} alt="actu test" />
                <p className="dateAjout">Date de l'ajout</p>
            </div>
            <hr className="hrBase" />
        </div>
    );
};

export default Media;