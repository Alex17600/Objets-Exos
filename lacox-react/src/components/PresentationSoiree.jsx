import React, { useEffect, useState } from "react";
import findAllSoiree from "../services/findAllSoiree";
import dayjs from "dayjs";
import "dayjs/locale/fr";
import jwtDecode from "jwt-decode";

const PresentationSoiree = () => {
  const token = localStorage.getItem("token");
  console.log(token);
  dayjs.locale("fr");
  const [events, setEvents] = useState([]);
  const [texteButton, setButton] = useState("");

  useEffect(() => {
    const loadPresentation = async () => {
      try {
        const data = await findAllSoiree();
        setEvents(data);

        console.log(data);
        if (!token) {
          setButton("Inscrivez-vous");
        } else {
          setButton("Reserver");
        }
      } catch (error) {
        console.error(error);
      }
    };

    loadPresentation();
  }, []);

  const handleClickReservation = async (eventId) => {
  //recup du token pour inscription si pas loggué alors button iscription
  const headers = {
    'Content-Type': 'application/json',
    Authorization: `Bearer ${token}`,
  };

    try {
        const response = await fetch(`http://localhost:8000/resevents/${eventId}/evenement`, {
            method: "POST",
            headers
        });
        if (response.ok) {
          const responseData = await response.json();
          console.log(responseData);
        }

    } catch (error) {
        console.error(error);
    }
};

  return (
    <div className="blockSoiree">
      <h1>Reserve ta soirée!</h1>
      {events.map((event) => (
        <React.Fragment key={event.id}>
          <div className="soiree">
            <p>{event.nomEvent}</p>
            <p>le {dayjs(event.date_event).format("DD MMMM YYYY")}</p>
            {event.medias.map((media) => (
              <img
                key={media.id}
                src={`http://localhost:8000/medias/${media.id}/image`}
                alt="photoSoiree"
              />
            ))}
            <div className="textSoiree" key={event.id}>
              <p>{event.presentation}</p>
            </div>
            <button  onClick={() => handleClickReservation(event.id)}>
              {texteButton}
              </button>
          </div>
        </React.Fragment>
      ))}
    </div>
  );
};

export default PresentationSoiree;
