import React, { useEffect, useState } from "react";
import NavetteImage from "../assets/images/navette-la-cox.jpg";
import fetchTexte from "../services/texteService";

const Navette = () => {
  const [texteNavette, setTexteNavette] = useState("");

  useEffect(() => {
    // Fonction asynchrone pour récupérer le texte
    const getTexteNavette = async () => {
      try {
        const texte = await fetchTexte(2); // Récupère le texte avec l'ID 2
        setTexteNavette(texte);
      } catch (error) {
        console.error(error);
        // Gérer l'erreur de récupération du texte
      }
    };

    getTexteNavette();
  }, []); // Le tableau de dépendances vide assure que l'effet s'exécute une seule fois

  return (
    <div className="navette-container">
      <h3>Laissez-vous conduire</h3>
      <div className="text-navette">
        <p id="texteNavette">
          {texteNavette}
        </p>
      </div>
      <div className="image-navette">
        <img src={NavetteImage} alt="Image" />
      </div>
    </div>
  );
};

export default Navette;

