import React from "react";
import LogoNoir from "../assets/images/logo-noir-lacox.png";
import LogoFacebook from "../assets/images/facebook.png";
import LogoInstagram from "../assets/images/instagram.png";
import LogoTikTok from "../assets/icons/icons8-tic-tac-96.png";
import { NavLink } from "react-router-dom";

const Footer = () => {
  return (
    <div className="footer">
      <img src={LogoNoir} alt="logo la cox" />
      <p>Mentions légales</p>
      <p>Nos soirées</p>
      <p>Où nous trouver?</p>
      <div className="logoReseaux">
        <NavLink to="https://www.tiktok.com/discover/discoth%C3%A8que-la-cox" rel="noreferrer"><img src={LogoTikTok} alt="icon tiktok" /></NavLink>
        <NavLink to="https://www.facebook.com/discothequelacox/?locale=fr_FR" rel="noreferrer"><img src={LogoFacebook} alt="icon facebook" /></NavLink>
        <NavLink to="https://www.instagram.com/discotheque_lacox/?hl=fr" rel="noreferrer"><img src={LogoInstagram} alt="icon instagram" /></NavLink>
      </div>
    </div>
  );
};

export default Footer;
