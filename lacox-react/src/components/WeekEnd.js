import { useState, useEffect } from "react";
import React from "react";

const WeekEnd = () => {
  const [afficheLeft, setAfficheLeft] = useState();
  const [afficheRight, setAfficheRight] = useState();

  useEffect(() => {
    const weekStart = async () => {
      try {
        const response = await fetch(
          "http://localhost:8000/medias/types/4", {});

        if (response.ok) {
          const imageJson = await response.json();

          const idImage1 = imageJson[0].id;
          const idImage2 = imageJson[1].id;

          setAfficheLeft(`http://localhost:8000/medias/${idImage1}/image`);
          setAfficheRight(`http://localhost:8000/medias/${idImage2}/image`);

        }
      } catch (error) {
        throw new Error("Erreur lors du chargment medias week-ends", error);
      }
    };
    weekStart();
  }, []);

  return (
    <div>
      <h3>Ce week-end à la Cox</h3>
      <div className="blocWeekEnd">
        <div className="firstSoiree">
          <img src={afficheLeft} alt="soiree blackout" />
          <button type="submit">Reserver</button>
        </div>

        <div className="secondSoiree">
          <img src={afficheRight} alt="soiree blackout" />
          <button type="submit">Reserver</button>
        </div>
      </div>
      <hr className="hrBase" />
    </div>
  );
};

export default WeekEnd;
