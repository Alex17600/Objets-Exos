import React, { useEffect, useState } from "react";
import Logo from "../assets/icons/cox.svg";
import DiscoBall from "../assets/icons/icon-disco-ball.svg";
import fetchTexte from "../services/texteService";

const Presentation = () => {
  const [textePresentation, setTexteNavette] = useState("");

  useEffect(() => {
    // Fonction asynchrone pour récupérer le texte
    const getTexteNavette = async () => {
      try {
        const texte = await fetchTexte(1); // Récupère le texte avec l'ID 2
        setTexteNavette(texte);
      } catch (error) {
        console.error(error);
      }
    };

    getTexteNavette();
  }, []); // Le tableau de dépendances vide assure que l'effet s'exécute une seule fois

  return (
    <div className="blocPresentation">
      <div className="title-container">
        <h1>
          Discothèque la C
          <img
            src={DiscoBall}
            alt="icon disco ball"
          />
          x
        </h1>
        <img
          className="logoTop"
          src={Logo}
          alt="logo cox"
        />
      </div>
      <p className="adress">
        5 route des carrières, Saint-Sulpice-d'Arnoult, France, 17250
      </p>
      <hr className="hrBase" />
      <h2>Le club</h2>
      <p id="textPresentation">
        {textePresentation}
        </p>
      <hr className="hrBase" />
    </div>
  );
};

export default Presentation;
