import React from 'react';
import { Route, Routes } from 'react-router';
import Administration from '../pages/Administration';
import TexteAdmin from '../pages/TexteAdmin';
import UtilisateurAdmin from '../pages/UtilisateurAdmin';
import PhotoAdmin from '../pages/PhotoAdmin';

const AdminRouter = () => {
    return (
        <Routes>
          <Route index path="" element={<Administration />}/>
          <Route path="texteAdmin" element={<TexteAdmin />} />
          <Route path="utilisateurAdmin" element={<UtilisateurAdmin />} />
          <Route path="photoAdmin" element={<PhotoAdmin/>} />
        </Routes>
    );
};

export default AdminRouter;