import React, { useEffect, useRef } from "react";
import ArrowUp from "../assets/icons/arrow-up.png";

const Scroll = () => {
    const btnScrollRef = useRef(null);

    function scrollHaut() {
      window.scrollTo({ top: 0, behavior: "smooth" });
    }
  
    useEffect(() => {
      const handleScroll = () => {
        const btnScroll = btnScrollRef.current;
        if (window.pageYOffset > 600) {
          btnScroll.classList.add("visible");
        } else {
          btnScroll.classList.remove("visible");
        }
      };
  
      window.addEventListener("scroll", handleScroll);
  
      return () => {
        window.removeEventListener("scroll", handleScroll);
      };
    }, []);

  return (
    <div ref={btnScrollRef} id="btn-scroll" className="scroll">
      <img src={ArrowUp} alt="arrow up" onClick={scrollHaut}/>
    </div>
  );
};

export default Scroll;
