import React, { useState, useEffect, useRef } from "react";
import albumFindAll from "../services/albumFindAll.js";

const Album = () => {
  const [albums, setAlbums] = useState([]);

  useEffect(() => {
    const fetchAllAlbum = async () => {
      try {
        const response = await albumFindAll();
        setAlbums(response);
        console.log(response);
      } catch (error) {
        console.error("Erreur lors de la récupération des événements", error);
      }
    };
    fetchAllAlbum();
  }, []);

  return (
    <div className="blockPhotos">
      {albums.map((album) => {
        if (album.medias && album.medias.length > 0) {
          const hasNonNullCover = album.medias.some(
            (media) => media.cover !== null
          );
          if (hasNonNullCover) {
            return (
              <div className="album" key={album.id}>
                <p className="textAlbum">{album.nom}</p>
                <img
                  src={`http://localhost:8000/albums/${album.id}/media`}
                  alt="photo album"
                />
              </div>
            );
          }
        }
        return null;
      })}
    </div>
  );
};

export default Album;
