import React from "react";
import { Navigate } from "react-router";
import jwtDecode from "jwt-decode";

const FilterAuth = ({ children }) => {

  const token = localStorage.getItem("token");
  const tokenDecode = jwtDecode(token);
  const role = tokenDecode.roles;

  if (role[0] !== "ROLE_ADMIN") {
    return <Navigate to={"/"}/> ;
  }
  return children;
};

export default FilterAuth;
