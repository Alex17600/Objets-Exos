import React from "react";
import Navbar from "./Navbar";
import NabarResponsive from "./NabarResponsive";

const Header = () => {
  return (
    <div>
      <header id="header-principale">
        <Navbar />
        <NabarResponsive />
      </header>
    </div>
  );
};

export default Header;
