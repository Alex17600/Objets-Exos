import React from 'react';
import { NavLink } from 'react-router-dom';
import ReturnIcon from "../../assets/icons/return-icon.svg";

const TitreAdmin = () => {
    return (
        <div className="header">
            <h1>Administration</h1>
            <NavLink to="/">
                <img id="returnIcon" src={ReturnIcon} alt="retour site" />
            </NavLink>
        </div>
    );
};

export default TitreAdmin;