import React, { useEffect, useState } from "react";
import fetchAll from "../../../services/texteFindAll";

const Main = () => {
  const token = localStorage.getItem("token");

  const headers = {
    "Content-Type": "application/json",
    Authorization: `Bearer ${token}`,
  };

  const [data, setData] = useState([]);
  const [selectedTexte, setSelectedTexte] = useState(null);
  const [titreValue, setTitreValue] = useState("");
  const [contenuValue, setContenuValue] = useState("");

  useEffect(() => {
    fetchAll()
      .then((responseData) => {
        setData(responseData);
      })
      .catch((error) => {
        console.error("Error during fetch:", error);
      });
  }, []);

  const handleSelectChange = (event) => {
    const selectedPage = event.target.value;
    const selectedTexte = data.find((texte) => texte.page === selectedPage);

    setSelectedTexte(selectedTexte);
    setTitreValue(selectedTexte?.nom || "");
    setContenuValue(selectedTexte?.contenu || "");
  };

  const handleSubmit = (event) => {
    event.preventDefault(); 

    const updatedTexte = {
      nom: titreValue,
      contenu: contenuValue,
      page: selectedTexte.page,
    };

    fetch(`http://localhost:8000/textes/${selectedTexte.id}`, {
      method: "PUT",
      headers,
      body: JSON.stringify(updatedTexte),
    })
      .then((response) => {
        if (!response.ok) {
          throw new Error("Erreur lors de la requête");
        }
        return response.json();
      })
      .then((data) => {
        setTitreValue(data.nom);
        setContenuValue(data.contenu);
      })
      .catch((error) => {
        console.error(error);
      });

    alert("Mise à jour effectuée");
    window.location.reload();
  };


  return (
    <div>
      <h2>Vos Textes</h2>
      <div id="tableau">
        <table>
          <thead>
            <tr>
              <th>Page</th>
              <th>Nom</th>
              <th>Contenu</th>
            </tr>
          </thead>
          <tbody>
            {data.map((texte) => (
              <tr key={texte.id}>
                <td>{texte.page}</td>
                <td>{texte.nom}</td>
                <td>{texte.contenu}</td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
      <h2 className="h2AdminTexte">Modifier un texte</h2>
      <div className="blockFormAddTexte">
        <form id="modifTexte" onSubmit={handleSubmit}>
          <label htmlFor="page">Page</label>
          <select id="page" onChange={handleSelectChange}>
            <option value="">Selectionne une option</option>
            {data.map((texte) => (
              <option key={texte.id} value={texte.page}>
                {texte.page}
              </option>
            ))}
          </select>
          <label htmlFor="titre">Titre</label>
          <input
            id="titre"
            type="text"
            required
            value={titreValue}
            onChange={(event) => setTitreValue(event.target.value)}
          />
          <label htmlFor="contenuTexte">Votre texte</label>
          <textarea
            name="contenuTexte"
            id="contenuTexte"
            cols="30"
            rows="10"
            required
            value={contenuValue}
            onChange={(event) => setContenuValue(event.target.value)}
          ></textarea>

          <button type="submit">Modifier</button>
        </form>
      </div>
    </div>
  );
};

export default Main;
