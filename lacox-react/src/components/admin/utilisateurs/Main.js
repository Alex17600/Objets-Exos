import React, { useEffect, useState } from "react";
import { fetchAllUsers, deleteUser } from "../../../services/utilisateurFindAll.js";

const UtilisateurTable = () => {
  const [data, setData] = useState([]);

  useEffect(() => {
    fetchData();
  }, []);

  const fetchData = async () => {
    try {
      const responseData = await fetchAllUsers();
      setData(responseData);
    } catch (error) {
      console.error("Error during fetch:", error);
    }
  };

  const handleDeleteUser = async (userId) => {
    try {
      await deleteUser(userId);
      const updatedData = data.filter((user) => user.id !== userId);
      setData(updatedData);
    } catch (error) {
      console.error("Error deleting user:", error);
    }
  };

  return (
    <div id="tableauUtilisateur">
      <table>
        <thead>
          <tr>
            <th>Nom</th>
            <th>Prenom</th>
            <th>Email</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {data.map((user) => (
            <tr key={user.id}>
              <td>{user.nom}</td>
              <td>{user.prenom}</td>
              <td>{user.email}</td>
              <td>
                <button
                  data-id={user.id}
                  onClick={() => handleDeleteUser(user.id)}
                  className="btn-supprimer"
                >
                  Supprimer
                </button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default UtilisateurTable;
