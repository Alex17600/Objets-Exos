import React from "react";

const TexteAccueilAdmin = () => {
  return (
    <div className="texte-accueil-admin">
      <p className="bienvenue">Bienvenue sur le back-office</p>
      <p>Veuillez choisir une élément à gérer</p>
    </div>
  );
};

export default TexteAccueilAdmin;
