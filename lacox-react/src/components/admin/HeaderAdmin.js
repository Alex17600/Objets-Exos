import React from 'react';
import TitreAdmin from '../admin/TitreAdmin';
import NavbarAdmin from '../admin/NavbarAdmin';

const HeaderAdmin = () => {
    return (
        <div className="header-container">
            <TitreAdmin />
            <NavbarAdmin />
        </div>
    );
};

export default HeaderAdmin;