import React from "react";
import { NavLink} from "react-router-dom";

const NavbarAdmin = () => {
  return (
    <div>
      <ul>
        <li>Accueil Admin</li>
        <NavLink to="/administration/photoAdmin">
        <li>Photos</li>
        </NavLink>
        <li>Agenda</li>
        <NavLink to="/administration/utilisateurAdmin">
          <li>Utilisateurs</li>
        </NavLink>
        <li>Actualités</li>
        <NavLink to="/administration/texteAdmin">
          <li>Textes</li>
        </NavLink>

      </ul>
    </div>
  );
};

export default NavbarAdmin;
