import React, { useState, useEffect, useRef } from "react";
import fetchAllEvents from "../../../services/evenementFindAll";
import albumFindAll from "../../../services/albumFindAll";

const Telecharge = () => {
  const filesInputs = useRef();
  const [nom, setNom] = useState("");
  const [nomCover, setNomCover] = useState("");
  const [image, setImage] = useState(null);
  const [imageCover, setImageCover] = useState(null);
  const [evenement, setEvenement] = useState("");
  const [evenementCover, setEvenementCover] = useState("");
  const [events, setEvents] = useState([]);
  const [albums, setAlbums] = useState([]);
  const [albumsCover, setAlbumsCover] = useState("");
  const [albumSelect, setAlbumSelect] = useState("");
  const [nomAlbum, setNomAlbum] = useState("");
  const [message, setMessage] = useState("");
  const [messageAlbum, setMessageAlbum] = useState("");
  const [imageData, setImageData] = useState(null);

  //charge les evenements existants dans le select associé
  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await fetchAllEvents();
        setEvents(response);
      } catch (error) {
        console.error("Erreur lors de la récupération des événements", error);
      }
    };

    fetchData();
  }, []);

  //charge les albums existants dans le select associé
  useEffect(() => {
    const albumFetch = async () => {
      try {
        const response = await albumFindAll();
        setAlbums(response);
      } catch (error) {
        console.error("Erreur lors de la récupération des événements", error);
      }
    };

    albumFetch();
  }, []);

  //recup du token pour autorisation
  const token = localStorage.getItem("token");
  const headers = {
    Authorization: `Bearer ${token}`,
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    const formData = new FormData();

    const jsonData = {
      nom: nom,
      evenement: { id: evenement },
      album: { id: albumSelect },
    };

    const blob = new Blob([JSON.stringify(jsonData)], {
      type: "application/json",
    });

    formData.append("media", blob);
    formData.append("image", image);

    try {
      const response = await fetch("http://localhost:8000/medias/download", {
        method: "POST",
        headers,
        body: formData,
      });

      if (response.ok) {
        setMessage(`Votre fichier ${nom} a été ajouté avec succès.`);

        fetchImageData(); // Mettre à jour l'image affichée
      } else {
        setMessage("Erreur lors de l'envoi du fichier.");
      }
    } catch (error) {
      setMessage("Erreur lors de l'envoi de la requête.");
    }
  };

  //affiche la photo télécharger
  const fetchImageData = async () => {
    try {
      const response = await fetch("http://localhost:8000/medias/image", {
        method: "GET",
      });

      if (response.ok) {
        const imageBlob = await response.blob();
        const imageUrl = URL.createObjectURL(imageBlob);
        setImageData(imageUrl);
      } else {
        setMessage("Erreur lors de la récupération de l'image.");
      }
    } catch (error) {
      setMessage("Erreur lors de l'envoi de la requête.");
    }
  };

  // apelle la fonction albumCreate
  const handleCreateAlbum = async (e) => {
    e.preventDefault();
    try {
      const response = await fetch("http://localhost:8000/albums/create", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({ nom: nomAlbum }),
      });

      if (!response.ok) {
        throw new Error("Erreur lors de la requête");
      }

      setMessageAlbum("Album " + nomAlbum + " créer avec succès");
      setNomAlbum("");

      // Traitez la réponse si nécessaire
    } catch (error) {
      throw new Error("Erreur lors de la création de l'album");
    }
  };

  const createCovered = async (e) => {
    e.preventDefault();

    const formData = new FormData();

    const jsonData = {
      nom: nomCover,
      evenement: { id: evenementCover },
      album: { id: albumsCover },
    };

    const blob = new Blob([JSON.stringify(jsonData)], {
      type: "application/json",
    });

    formData.append("media", blob);
    formData.append("image", imageCover);

    try {
      const response = await fetch("http://localhost:8000/medias/cover/download", {
        method: "POST",
        headers,
        body: formData,
      });
      if (response.ok) {
        alert("it's ok");
      }
    } catch (error) {
      throw new Error("Erreur lors de la création du cover");
    }
  };

  return (
    <div>
      <h1 className="h1Photo">Gestion des Photos</h1>
      {/* block ajout image couverture */}
      <div className="addCover">
        <h2>Ajout d'un photo de couverture</h2>
        <form onSubmit={createCovered} className="addCoverInput">
          <label htmlFor="nomFichier">Titre photo</label>
          <input
            name="nomFichier"
            type="text"
            value={nomCover}
            onChange={(e) => setNomCover(e.target.value)}
          />
          <label>Choisir son fichier</label>
          <input
            type="file"
            name="imageCover"
            onChange={(e) => setImageCover(e.target.files[0])}
            multiple
            accept="image/png"
          />
          <label>Événement</label>
          <select
            name="evenementCover"
            value={evenementCover}
            onChange={(e) => setEvenementCover(e.target.value)}
          >
            <option value="">Sélectionnez un événement</option>
            {events.map((event) => (
              <option key={event.id} value={event.id}>
                {event.nomEvent}
              </option>
            ))}
          </select>
          <label>Choisir un album</label>
          <select
            name="albumSelectCover"
            value={albumsCover}
            onChange={(e) => setAlbumsCover(e.target.value)}
          >
            <option value="">Sélectionnez un album</option>
            {albums.map((album) => (
              <option key={album.id} value={album.id}>
                {album.nom}
              </option>
            ))}
          </select>
          <button type="submit">Envoyer</button>
        </form>
      </div>

      <div className="download">
        <h2 className="h2PhotoAdmin">Téléchargement</h2>
        <div className="top">
          <p>{message}</p>
          {imageData && <img src={imageData} alt="Image" />}
        </div>

        <div className="left">
          <h2>Selection multiple</h2>
          <form onSubmit={handleSubmit}>
            <label htmlFor="titrePhoto">Titre photo</label>
            <input
              name="titrePhoto"
              type="text"
              value={nom}
              onChange={(e) => setNom(e.target.value)}
              multiple
            />
            <label htmlFor="image">Choisir ses fichiers</label>
            <input
              ref={filesInputs}
              type="file"
              name="image"
              onChange={(e) => setImage(e.target.files[0])}
              multiple
              accept="image/png"
            />
            <label htmlFor="evenement">Événement</label>
            <select
              name="evenement"
              value={evenement}
              onChange={(e) => setEvenement(e.target.value)}
            >
              <option value="">Sélectionnez un événement</option>
              {events.map((event) => (
                <option key={event.id} value={event.id}>
                  {event.nomEvent}
                </option>
              ))}
            </select>
            <label htmlFor="albumSelect">Choisir un album</label>
            <select
              name="albumSelect"
              value={albumSelect}
              onChange={(e) => setAlbumSelect(e.target.value)}
            >
              <option value="">Sélectionnez un album</option>
              {albums.map((album) => (
                <option key={album.id} value={album.id}>
                  {album.nom}
                </option>
              ))}
            </select>
            <button type="submit">Envoyer</button>
          </form>
        </div>
        <div className="right">
          <h2 className="h2PhotoAdmin">Fichier envoyé</h2>
          {message}
          {imageData && <img src={imageData} alt="Image" />}
        </div>
      </div>

      {/* form gestion des albums */}
      <h2 className="h2CreaAlbum">Creation de l'album</h2>
      <div className="ajoutAlbum">
        <form className="formAlbum" onSubmit={handleCreateAlbum}>
          <div>{messageAlbum}</div>
          <label htmlFor="nomAlbum">Nom de l'album</label>
          <input
            name="nomAlbum"
            type="text"
            value={nomAlbum}
            onChange={(e) => setNomAlbum(e.target.value)}
          />
          <button type="submit">Envoyer</button>
        </form>
      </div>
    </div>
  );
};

export default Telecharge;
