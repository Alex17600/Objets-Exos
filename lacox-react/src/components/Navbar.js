import React, { useEffect, useRef } from "react";
import { NavLink } from "react-router-dom";

const Navbar = () => {

  return (
    <div className="navbar">
      <div className="menu">
        <NavLink to="/">
          <p>Accueil</p>
        </NavLink>
        <NavLink to="/photos">
          <p>Les photos</p>
        </NavLink>
        <NavLink to='/contact'>
        <p>Nous contacter</p>
        </NavLink>
        <NavLink to="/reserve">
        <p>Reserver</p>
        </ NavLink>
        <NavLink to="/connexion">
          <p className="connexion-link">
            Se connecter
          </p>
        </NavLink>
      </div>
      <p>Discothèque</p>
      <p>-La Cox-</p>
    </div>
  );
};

export default Navbar;
