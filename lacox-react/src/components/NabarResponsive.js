import { NavLink } from "react-router-dom";
import Close from "../assets/icons/close.svg";
import IconBurger from "../assets/icons/icon-burger.svg";
import { useRef } from "react";

const NabarResponsive = () => {
  const burger = useRef();

  function openNavBar() {
    burger.current.style.left = 0;
  }

  function closeNavBar() {
    burger.current.style.left = "-400px";
  }

  return (
    <div className="navbarResponsive">
      <div className="burger" ref={burger}>
        <div className="close">
          <img src={Close} alt="fermeture navbar" onClick={closeNavBar} />
        </div>
        <NavLink to="/">Accueil</NavLink>
        <NavLink to="/photos">Photos</NavLink>
        <NavLink to='/contact'>Contact</NavLink>
        <NavLink to='/reserve'>Reserver</NavLink>
        <NavLink to="/connexion">Connexion</NavLink>
      </div>
      <label htmlFor="navCheck" className="burgerImage">
        <img src={IconBurger} alt="icon burger" onClick={openNavBar} />
      </label>
      -La Cox-
    </div>
  );
};

export default NabarResponsive;
