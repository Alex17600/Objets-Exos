import React from "react";
import TikTok from "../../assets/icons/tiktok.png";
import Facebook from "../../assets/images/facebook.png";
import Instagram from "../../assets/images/instagram.png";
import Telephone from "./Telephone";
import { NavLink } from "react-router-dom";

const contactForm = () => {
  return (
    <div className="blockFormContact">
      <form>
        <h1 className="h1Contact">Contactez-nous</h1>
        <hr />
        <label htmlFor="nom">Nom</label>
        <input name="nom" type="text" />
        <label htmlFor="Prenom">Prenom</label>
        <input name="prenom" type="text" />
        <label htmlFor="email">Email</label>
        <input name="email" type="text" />
        <label htmlFor="message">Votre message</label>
        <textarea name="message" cols="26" rows="10"></textarea>
        <button type="submit">Envoyer</button>
      </form>
      <section className="blockInfosContact">
        <h2 className="h2Contact">Nos réseaux</h2>
        <hr />
        <div className="adresseContact">
          <p>Retrouvez-nous à cette adresse:</p>
          <p>5 route des carrières, Saint-Sulpice-d'Arnoult, France, 17250</p>
        </div>
        <div className="images">
          <NavLink to="https://www.tiktok.com/discover/discoth%C3%A8que-la-cox" rel="noreferrer">
            <img src={TikTok} alt="icon TikTok" />
          </NavLink>
          <NavLink to="https://www.facebook.com/discothequelacox/?locale=fr_FR" rel="noreferrer">
            <img src={Facebook} alt="icon Facebook" />
          </NavLink>
          <NavLink to="https://www.instagram.com/discotheque_lacox/?hl=fr" rel="noreferrer">
            <img src={Instagram} alt="icon Instagram" />
          </NavLink>
        </div>
        <hr />
        <h2>Téléphone</h2>
        <Telephone />
      </section>
    </div>
  );
};

export default contactForm;
