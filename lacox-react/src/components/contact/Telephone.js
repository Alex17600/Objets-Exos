import React, { useState } from 'react';
import Tel from "../../assets/icons/telephone.png";

const Telephone = () => {
    const [showNumber, setShowNumber] = useState(false);

    const handleClick = () => {
        setShowNumber(!showNumber);
    };

    return (
        <div className="contactAutre">
            <img src={Tel} alt="icône téléphone" onClick={handleClick} />
            {showNumber && (
                <p>06 51 49 92 92</p>
            )}
        </div>
    );
};

export default Telephone;