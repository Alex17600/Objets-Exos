package com.bibliotheque;

public class BluRay extends Document {
    private String auteur;
    private double caution;

    public BluRay(String id, String titre, String auteur, double caution) {
        super(id, titre);
        this.auteur = auteur;
        this.caution = caution;
    }

    public String getAuteur() {
        return auteur;
    }

    public void setAuteur(String auteur) {
        this.auteur = auteur;
    }

    public double getCaution() {
        return caution;
    }

    public void setCaution(double caution) {
        this.caution = caution;
    }

}
