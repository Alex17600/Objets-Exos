package com.bibliotheque;

import java.util.Date;

public class Journaux extends Document {
    private Date datePublication;

    public Journaux(String id, String titre, Date datePublication) {
        super(id, titre);
        this.datePublication = datePublication;
    }

    public Date getDatePublication() {
        return datePublication;
    }

    public void setDatePublication(Date datePublication) {
        this.datePublication = datePublication;
    }

    
}
