package com.bibliotheque;

import java.util.Date;

public class Benevole extends Personne implements ValideEmprunt {
    private Date limiteDate;

    public Benevole(String nom, String prenom, Date limiteDate) {
        super(nom, prenom);
        this.limiteDate = limiteDate;
    }

    public Date getLimiteDate() {
        return limiteDate;
    }

    public void setLimiteDate(Date limiteDate) {
        this.limiteDate = limiteDate;
    }

    @Override
    public boolean valideEmprunt(Emprunt emprunt) {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Unimplemented method 'valideEmprunt'");
    }
}
