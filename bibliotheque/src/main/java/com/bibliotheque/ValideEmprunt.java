package com.bibliotheque;

public interface ValideEmprunt {

    public boolean valideEmprunt(Emprunt emprunt);
    
}
