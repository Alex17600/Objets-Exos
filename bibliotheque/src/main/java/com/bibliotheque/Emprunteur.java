package com.bibliotheque;

public class Emprunteur extends Personne {
    private String carteLecteur;
    private String adresse;
    private Cotisation cotisation;
    private Document[] liste = new Document[5];

   

    public Emprunteur(String nom, String prenom, String carteLecteur, String adresse, Cotisation cotisation,
            Document[] liste) {
        super(nom, prenom);
        this.carteLecteur = carteLecteur;
        this.adresse = adresse;
        this.cotisation = cotisation;
        this.liste = liste;
    }

    public Cotisation getCotisation() {
        return cotisation;
    }

    public void setCotisation(Cotisation cotisation) {
        this.cotisation = cotisation;
    }

    public String getCarteLecteur() {
        return carteLecteur;
    }

    public void setCarteLecteur(String carteLecteur) {
        this.carteLecteur = carteLecteur;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public Document[] getListe() {
        return liste;
    }

    public void use (Document document) {
        
    }

}
