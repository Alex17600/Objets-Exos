package com.bibliotheque;

import java.util.ArrayList;
import java.util.List;

public final class Catalogue {
    private static Catalogue instance;
    public List<Document> documents = new ArrayList<Document>();

    public static Catalogue getInstance(List<Document> value) {
        if (instance == null) {
            instance = new Catalogue();
        }
        return instance;
    }
}
