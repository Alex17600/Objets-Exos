package fr.additionneur;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class App extends Application {

    private TextArea textArea;

    @Override
    public void start(Stage stage) {
        BorderPane root = new BorderPane();
        stage.setTitle("Additionneur");

        root.setPadding(new Insets(10));

        // Création de la VBox pour l'affichage du résultat
        VBox resultBox = new VBox();
        resultBox.setAlignment(Pos.CENTER);
        textArea = new TextArea();
        textArea.setEditable(false);
        textArea.setWrapText(true);
        ScrollPane scroll = new ScrollPane();
        scroll.setContent(textArea);
        scroll.setPrefWidth(250);
        resultBox.getChildren().add(scroll);

        // Création de la GridPane dynamique pour les boutons de 0 à 9
        GridPane numGrid = new GridPane();
        numGrid.setAlignment(Pos.CENTER);
        numGrid.setHgap(5);
        numGrid.setVgap(5);
        for (int i = 0; i < 10; i++) {
            Button button = new Button(Integer.toString(i));
            int row = i / 5;
            int col = i % 5;
            numGrid.add(button, col, row);
            button.setOnAction(event -> appendText(button.getText() + " + "));
        }

        // Création des boutons Calculer et Vider
        Button calculateButton = new Button("Calculer");
        calculateButton.setOnAction(event -> calculate());
        Button clearButton = new Button("Vider");
        clearButton.setOnAction(event -> clear());

        // Création de la HBox pour les boutons Calculer et Vider
        HBox buttonBox = new HBox();
        buttonBox.setAlignment(Pos.CENTER);
        buttonBox.setSpacing(10);
        buttonBox.getChildren().addAll(calculateButton, clearButton);

        // Ajout des VBoxes, de la GridPane et de la HBox au BorderPane
        root.setTop(resultBox);
        root.setCenter(numGrid);
        root.setBottom(buttonBox);

        Scene scene = new Scene(root, 300, 300);
        stage.setScene(scene);
        stage.show();
    }

    // Fonction pour ajouter du texte dans la TextArea
    private void appendText(String text) {
        textArea.appendText(text);
    }

    // Fonction pour calculer la somme des nombres dans la TextArea
    private void calculate() {
        String[] nums = textArea.getText().split("\\s*\\+\\s*");
        int sum = 0;
        for (String num : nums) {
            if (!num.isEmpty()) {
                sum += Integer.parseInt(num);
            }
        }
        textArea.setText(Integer.toString(sum));
    }

    // Fonction pour vider la TextArea
    private void clear() {
        textArea.clear();
    }

    public static void main(String[] args) {
        launch(args);
    }

}
