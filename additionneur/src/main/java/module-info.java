module fr.additionneur {
    requires transitive javafx.controls;
    requires javafx.fxml;

    opens fr.additionneur to javafx.fxml;
    exports fr.additionneur;
}