package fr.formulaire;


import javafx.application.Application;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.stage.Stage;
import javafx.event.*;

/**
 * Application de démonstration
 */
public class App extends Application {

    @Override
    public void start(Stage stage) {       

        // Instanciation des composants graphique
        Label entreeLabel = new Label("Entrée utilisateur: ");
        Label copieLabel = new Label("Copie de l'entrée: ");

        TextField entreeLabelTextField = new TextField("Saississez un texte");
        TextField copieField = new TextField("Saississez un texte");
        Label messageLabel = new Label();
        messageLabel.setPadding(new Insets(5, 10, 5, 10));
        Button copieButton = new Button("Copie");
        Button effacerButton = new Button("Effacer");
        Button quitterButton = new Button("Quitter");
        
        // Création d'une classe locale pour la gestion de l'évènement "click"
        EventHandler<MouseEvent> eventHandler = new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                String entree = entreeLabelTextField.getText();
                ;
                if (entree.isEmpty()) {
                    messageLabel.setText("Veuillez entrer un texte.");
                    messageLabel.setStyle("-fx-text-fill: WHITE; -fx-background-color: RED");   
                } else {
                    copieField.setText(entree);
                    copieField.setDisable(true);
  
                }
            }
        };

        EventHandler<MouseEvent> efface = new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                String entree = entreeLabelTextField.getText();

                if (entree.isEmpty()) {
                    Label messageLabel = new Label();                 
                } else {
                    copieField.setDisable(false);
                    copieField.setText("Copie de l'entrée: "); 
                    entreeLabelTextField.setText("Entrée utilisateur: ");
                }
            }
        };

        EventHandler<MouseEvent> quitter = new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                String entree = entreeLabelTextField.getText();

                if (entree.isEmpty()) {
                    messageLabel.setText("Veuillez entrer un texte.");                 
                } 
            }
        };quitterButton.setOnAction(ActionEvent -> Platform.exit());

        copieButton.addEventHandler(MouseEvent.MOUSE_CLICKED, eventHandler);
        effacerButton.addEventHandler(MouseEvent.MOUSE_CLICKED, efface);
        quitterButton.addEventHandler(MouseEvent.MOUSE_CLICKED, quitter);

        // Instanciation d'un pane grille pour organisation des composants graphiques 
        GridPane gridPane = new GridPane();
        gridPane.setHgap(10);
        gridPane.setVgap(10);

        // ajout de "padding" pour l'écarter du bord
        gridPane.setPadding(new Insets(10, 10, 10, 10));

        gridPane.add(entreeLabel, 0, 0);
        GridPane.setVgrow(entreeLabel, Priority.ALWAYS);
        gridPane.add(entreeLabelTextField, 1, 0);
        GridPane.setHgrow(entreeLabelTextField, Priority.ALWAYS);

        gridPane.add(copieLabel, 0, 1);
        GridPane.setVgrow(copieLabel, Priority.ALWAYS);

        gridPane.add(copieField, 1, 1);
        GridPane.setHgrow(copieField, Priority.ALWAYS);
        // gridPane.setGridLinesVisible(true);

        // Ajout du message d'information
        messageLabel.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        gridPane.add(messageLabel, 0, 2);
        GridPane.setColumnSpan(messageLabel, 2);

        // Ajout du bouton
        // avec gestion de la taille du button et du "span" sur plusieurs colonnes
        // On autorise le bouton à prendre toute la place disponible
        effacerButton.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        gridPane.add(effacerButton, 0, 1);
        quitterButton.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        gridPane.add(quitterButton, 0, 2);
        copieButton.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        gridPane.add(copieButton, 0, 3);
        GridPane.setColumnSpan(copieButton, 3);
        
        // Intanciation d'un "pane" racine
        // permet de faire "coller" au bord ce qu'il contient
        AnchorPane root = new AnchorPane();
        // Ajout du "gridPane" à la liste des enfants de la racine
        root.getChildren().add(gridPane);
        // paramétrage du "pane" racine afin de faire coller la grille
        AnchorPane.setLeftAnchor(gridPane, 0.0);
        AnchorPane.setRightAnchor(gridPane,  0.0);
        AnchorPane.setTopAnchor(gridPane, 0.0);
        AnchorPane.setBottomAnchor(gridPane,  0.0);

        // Création de la scène à afficher (le contenu de la fenêtre)
        Scene scene = new Scene(root);
        // Ajout de la scène à la fenêtre
        stage.setScene(scene);
        // Affichage de la fenêtre
        stage.show();
    }

    public static void main(String[] args) {
        launch();
    }

    // Classe INTERNE de gestion d'évènement 
    // -> à titre d'exemple
    // public class ConnexionHandler implements EventHandler<MouseEvent> {
    //     @Override
    //     public void handle(MouseEvent event) {
    //         System.out.println("Bonjour !");
    //     }
    // }
}