module fr.formulaire {
    requires transitive javafx.controls;
    requires javafx.fxml;

    opens fr.formulaire to javafx.fxml;
    exports fr.formulaire;
}
